import junit.framework.*;

public class Report extends TestCase {
  // throws NullPointerException but should be NoSuchElementException
  public void test2() throws Throwable {
    java.lang.Object[] var1 = new java.lang.Object[] { new Object() };
    java.util.Iterator var2 = uk.co.jezuk.mango.Iterators.ChainIterator(var1);
    var2.next();
    var2.hasNext();
    // disable call to hasNext() and NoSuchElementException is thrown
    var2.next();
  }
}
