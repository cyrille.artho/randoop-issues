import junit.framework.*;

public class FalsePositives extends TestCase {

  public void test1() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test1"); }


    com.google.common.collect.ImmutableRangeMap var0 = com.google.common.collect.ImmutableRangeMap.<java.lang.Comparable,java.lang.Object>of();
    java.lang.Class var1 = var0.getClass();
    com.google.common.collect.EnumHashBiMap var2 = com.google.common.collect.EnumHashBiMap.<java.lang.Enum,java.lang.Object>create(var1);

  }

  public void test2() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test2"); }

    com.google.common.collect.ImmutableRangeMap var0 = com.google.common.collect.ImmutableRangeMap.<java.lang.Comparable,java.lang.Object>of();

    java.lang.Class var1 = var0.getClass();
    com.google.common.collect.EnumBiMap var2 = com.google.common.collect.EnumBiMap.<java.lang.Enum,java.lang.Enum>create(var1, var1);

  }

  // null element in array
  public void test6() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test6"); }


    java.util.Comparator var0 = com.google.common.primitives.UnsignedLongs.lexicographicalComparator();
    com.google.common.collect.ImmutableSortedSet.Builder var1 = new com.google.common.collect.ImmutableSortedSet.Builder((java.util.Comparator)var0);
    java.util.Set var2 = com.google.common.collect.Sets.newConcurrentHashSet();
    java.lang.Object[] var3 = new java.lang.Object[] { var2};
    java.lang.Object[] var5 = com.google.common.collect.ObjectArrays.newArray(var3, 241);
    com.google.common.collect.ImmutableSet.Builder var6 = var1.add((java.lang.Object[])var5);

  }

  // null element in array
  public void test9() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test9"); }


    com.google.common.collect.ImmutableSetMultimap.Builder var0 = com.google.common.collect.ImmutableSetMultimap.builder();
    java.util.Set var2 = com.google.common.collect.Sets.newConcurrentHashSet();
    java.lang.Object[] var3 = new java.lang.Object[] { var2};
    java.lang.Object[] var5 = com.google.common.collect.ObjectArrays.newArray(var3, 241);
    com.google.common.collect.ImmutableSetMultimap.Builder var6 = var0.putAll((java.lang.Object)"should have thrown an IAE above", (java.lang.Object[])var5);

  }

}
