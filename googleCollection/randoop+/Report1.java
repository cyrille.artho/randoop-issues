import junit.framework.*;

public class Report1 extends TestCase {
  public void test10() throws Throwable {
    com.google.common.collect.ImmutableSetMultimap var1 = com.google.common.collect.ImmutableSetMultimap.of((java.lang.Object)"a", (java.lang.Object)"a");
    com.google.common.collect.Range var2 = com.google.common.collect.Range.<java.lang.Comparable>lessThan((java.lang.Comparable)"a");
    com.google.common.base.Function var3 = com.google.common.base.Functions.forPredicate((com.google.common.base.Predicate)var2);
    com.google.common.collect.Multimap var4 = com.google.common.collect.Multimaps.transformValues((com.google.common.collect.Multimap)var1, (com.google.common.base.Function)var3);
    
    // Checks the contract:  equals-hashcode on var4 and var4
    assertTrue("Contract failed: equals-hashcode on var4 and var4", var4.equals(var4) ? var4.hashCode() == var4.hashCode() : true);

  }
}
