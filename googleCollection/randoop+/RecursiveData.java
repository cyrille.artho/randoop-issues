import junit.framework.*;

public class RecursiveData extends TestCase {

  public void test5() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test5"); }


    com.google.common.collect.TreeRangeMap var0 = com.google.common.collect.TreeRangeMap.<java.lang.Comparable,java.lang.Object>create();
    com.google.common.collect.Range var1 = com.google.common.collect.Range.<java.lang.Comparable>all();
    com.google.common.collect.RangeMap var2 = var0.subRangeMap(var1);
    var0.put(var1, (java.lang.Object)var0);
    boolean var6 = var0.equals((java.lang.Object)"Suppliers.o");
    
    // Checks the contract:  var0.equals(var0)
    assertTrue("Contract failed: var0.equals(var0)", var0.equals(var0));
    
    // Checks the contract:  var2.equals(var2)
    assertTrue("Contract failed: var2.equals(var2)", var2.equals(var2));
    
  }

  public void test7() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test7"); }


    com.google.common.collect.HashBiMap var0 = com.google.common.collect.HashBiMap.create();
    java.util.Set var1 = var0.entrySet();
    java.lang.Object var2 = var0.put((java.lang.Object)var1, (java.lang.Object)var1);
    java.util.HashMap var3 = com.google.common.collect.Maps.newHashMap((java.util.Map)var0);

  }

  public void test8() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test8"); }


    com.google.common.collect.EvictingQueue var1 = com.google.common.collect.EvictingQueue.create(83);
    boolean var2 = var1.offer((java.lang.Object)var1);
    java.lang.String var3 = var1.toString();

  }

  public void test11() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test11"); }


    com.google.common.collect.HashBiMap var0 = com.google.common.collect.HashBiMap.create();
    com.google.common.collect.MapConstraint var1 = com.google.common.collect.MapConstraints.notNull();
    java.util.Map var2 = com.google.common.collect.MapConstraints.constrainedMap((java.util.Map)var0, (com.google.common.collect.MapConstraint)var1);
    java.lang.Object var3 = var2.put((java.lang.Object)var2, (java.lang.Object)var2);
    com.google.common.collect.HashBiMap var4 = com.google.common.collect.HashBiMap.create((java.util.Map)var2);

  }

  public void test13() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test13"); }


    com.google.common.base.Joiner var1 = com.google.common.base.Joiner.on("Must be at least 1 hash code to combine.");
    java.lang.StringBuilder var2 = new java.lang.StringBuilder();
    char[] var4 = new char[] { 'a', 'a'};
    java.util.List var5 = com.google.common.primitives.Chars.asList(var4);
    com.google.common.collect.ConcurrentHashMultiset var6 = com.google.common.collect.ConcurrentHashMultiset.create((java.lang.Iterable)var5);
    int var8 = var6.add((java.lang.Object)var6, 8199);
    java.lang.StringBuilder var9 = var1.appendTo(var2, (java.lang.Iterable)var6);

  }

}
