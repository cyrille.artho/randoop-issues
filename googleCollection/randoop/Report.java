package randoop;


import junit.framework.*;

public class Report extends TestCase {

  public void test1() throws Throwable {

    java.util.Comparator var0 = com.google.common.primitives.Chars.lexicographicalComparator();
    java.util.TreeSet var1 = com.google.common.collect.Sets.newTreeSet(var0);
    com.google.common.collect.ImmutableSetMultimap.Builder var3 = com.google.common.collect.ImmutableSetMultimap.builder();
    com.google.common.collect.ImmutableSetMultimap var7 = com.google.common.collect.ImmutableSetMultimap.of((java.lang.Object)var0, (java.lang.Object)var1, (java.lang.Object)"hi!", (java.lang.Object)var3, (java.lang.Object)100.0d, (java.lang.Object)var3, (java.lang.Object)(short)0, (java.lang.Object)100);
    com.google.common.base.Converter var8 = com.google.common.primitives.Floats.stringConverter();
    com.google.common.collect.Multimap var9 = com.google.common.collect.Multimaps.transformValues((com.google.common.collect.Multimap)var7, (com.google.common.base.Function)var8);
    
    // Checks the contract:  equals-hashcode on var9 and var9
    assertTrue("Contract failed: equals-hashcode on var9 and var9", var9.equals(var9) ? var9.hashCode() == var9.hashCode() : true);

  }

}
