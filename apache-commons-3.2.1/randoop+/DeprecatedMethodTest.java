package enhancement_3000_simplified;


import java.util.Map;

import org.apache.commons.collections.iterators.TransformIterator;
import org.apache.commons.collections.map.CompositeMap;

import junit.framework.*;

public class DeprecatedMethodTest extends TestCase {

  public static boolean debug = false;

  
  
  public void test76() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test76");
		}

		org.apache.commons.collections.StaticBucketMap var0 = new org.apache.commons.collections.StaticBucketMap();
		java.util.Set var1 = org.apache.commons.collections.set.MapBackedSet
				.decorate((java.util.Map) var0);
		boolean var2 = var1.add((java.lang.Object) var1);

		// Checks the contract: var1.equals(var1)
		assertTrue("Contract failed: var1.equals(var1)", var1.equals(var1));

	}
  
  
  public void test32() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test32");
		}

		org.apache.commons.collections.BoundedFifoBuffer var0 = new org.apache.commons.collections.BoundedFifoBuffer();
		org.apache.commons.collections.BoundedCollection var1 = org.apache.commons.collections.collection.UnmodifiableBoundedCollection
				.decorate((org.apache.commons.collections.BoundedCollection) var0);
		org.apache.commons.collections.BoundedCollection var2 = org.apache.commons.collections.collection.UnmodifiableBoundedCollection
				.decorate((org.apache.commons.collections.BoundedCollection) var1);
		java.util.Iterator var3 = var1.iterator();

		// This assertion (symmetry of equals) fails
		assertTrue("Contract failed: equals-symmetric on var2 and var1.",
				var2.equals(var1) == var1.equals(var2));

	}

	public void test39() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test39");
		}

		org.apache.commons.collections.BeanMap var0 = new org.apache.commons.collections.BeanMap();
		java.util.Set var1 = var0.entrySet();
		java.util.Set var2 = var0.entrySet();
		var0.setBean((java.lang.Object) "Index ");

		// Checks the contract: equals-hashcode on var0 and var0
		assertTrue("Contract failed: equals-hashcode on var0 and var0",
				var0.equals(var0) ? var0.hashCode() == var0.hashCode() : true);

		// Checks the contract: equals-hashcode on var1 and var1
		assertTrue("Contract failed: equals-hashcode on var1 and var1",
				var1.equals(var1) ? var1.hashCode() == var1.hashCode() : true);

		// Checks the contract: equals-hashcode on var2 and var2
		assertTrue("Contract failed: equals-hashcode on var2 and var2",
				var2.equals(var2) ? var2.hashCode() == var2.hashCode() : true);

		// Checks the contract: equals-hashcode on var3 and var3
		assertTrue("Contract failed: equals-hashcode on var3 and var3",
				var0.equals(var0) ? var0.hashCode() == var0.hashCode() : true);

	}


	// ///////////////////////////contract violation end///

  
  
	// ///////////////////////////contract violation start///

	public void test14() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test14");
		}

		org.apache.commons.collections.BeanMap var1 = new org.apache.commons.collections.BeanMap(
				(java.lang.Object) "Entry.next=null, data[removeIndex]=");

		// Checks the contract: equals-hashcode on var1 and var1
		assertTrue("Contract failed: equals-hashcode on var1 and var1",
				var1.equals(var1) ? var1.hashCode() == var1.hashCode() : true);

	}
  
  
  
  
/////////////////////////////deprecated method usage start///
	public void test13() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test13");
		}

		org.apache.commons.collections.map.CompositeMap var0 = new org.apache.commons.collections.map.CompositeMap();
		java.util.Set var1 = var0.entrySet();
		((CompositeMap) var1).addComposited((Map) var1);
		org.apache.commons.collections.BoundedFifoBuffer var3 = new org.apache.commons.collections.BoundedFifoBuffer(
				(java.util.Collection) var1);

	}

	public void test15() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test15");
		}

		org.apache.commons.collections.DefaultMapEntry var2 = new org.apache.commons.collections.DefaultMapEntry(
				(java.lang.Object) 0,
				(java.lang.Object) "Cannot put new key/value pair - Map is fixed size");
		var2.setKey((java.lang.Object) var2);
		java.lang.String var4 = var2.toString();

	}

	public void test18() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test18");
		}

		org.apache.commons.collections.iterators.ProxyListIterator var0 = new org.apache.commons.collections.iterators.ProxyListIterator();
		var0.add((java.lang.Object) "]");

	}

	public void test20() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test20");
		}

		org.apache.commons.collections.iterators.SingletonIterator var1 = new org.apache.commons.collections.iterators.SingletonIterator(
				(java.lang.Object) "The transformer array must ");
		org.apache.commons.collections.iterators.ProxyIterator var2 = new org.apache.commons.collections.iterators.ProxyIterator(
				(java.util.Iterator) var1);
		var2.setIterator((java.util.Iterator) var2);
		boolean var4 = var2.hasNext();

	}

	public void test21() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test21");
		}

		org.apache.commons.collections.iterators.ProxyListIterator var0 = new org.apache.commons.collections.iterators.ProxyListIterator();
		boolean var1 = var0.hasNext();

	}

	public void test26() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test26");
		}

		org.apache.commons.collections.iterators.ProxyListIterator var0 = new org.apache.commons.collections.iterators.ProxyListIterator();
		int var1 = var0.nextIndex();

	}

	public void test27() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test27");
		}

		org.apache.commons.collections.iterators.ProxyListIterator var0 = new org.apache.commons.collections.iterators.ProxyListIterator();
		java.util.ListIterator var1 = org.apache.commons.collections.IteratorUtils
				.unmodifiableListIterator((java.util.ListIterator) var0);
		boolean var2 = var1.hasNext();

	}
	
	public void test30() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test30");
		}

		org.apache.commons.collections.iterators.ProxyListIterator var0 = new org.apache.commons.collections.iterators.ProxyListIterator();
		boolean var1 = var0.hasPrevious();

	}

	public void test31() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test31");
		}

		org.apache.commons.collections.iterators.ProxyListIterator var0 = new org.apache.commons.collections.iterators.ProxyListIterator();
		java.util.ListIterator var1 = org.apache.commons.collections.IteratorUtils
				.unmodifiableListIterator((java.util.ListIterator) var0);
		java.lang.Object var2 = var1.next();

	}
	
	public void test36() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test36");
		}

		org.apache.commons.collections.DefaultMapEntry var0 = new org.apache.commons.collections.DefaultMapEntry();
		org.apache.commons.collections.map.SingletonMap var1 = new org.apache.commons.collections.map.SingletonMap(
				(org.apache.commons.collections.KeyValue) var0);
		org.apache.commons.collections.map.LinkedMap var2 = new org.apache.commons.collections.map.LinkedMap(
				(java.util.Map) var1);
		org.apache.commons.collections.Factory var3 = org.apache.commons.collections.FactoryUtils
				.exceptionFactory();
		org.apache.commons.collections.map.MultiValueMap var4 = org.apache.commons.collections.map.MultiValueMap
				.decorate((java.util.Map) var2,
						(org.apache.commons.collections.Factory) var3);
		boolean var6 = var4
				.containsValue((java.lang.Object) "The list must not be null");

	}
	
	public void test40() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test40");
		}

		org.apache.commons.collections.comparators.ReverseComparator var0 = new org.apache.commons.collections.comparators.ReverseComparator();
		org.apache.commons.collections.TreeBag var1 = new org.apache.commons.collections.TreeBag(
				(java.util.Comparator) var0);
		org.apache.commons.collections.Factory var2 = org.apache.commons.collections.FactoryUtils
				.nullFactory();
		org.apache.commons.collections.Transformer var3 = org.apache.commons.collections.functors.FactoryTransformer
				.getInstance((org.apache.commons.collections.Factory) var2);
		org.apache.commons.collections.SortedBag var4 = org.apache.commons.collections.BagUtils
				.transformedSortedBag(
						(org.apache.commons.collections.SortedBag) var1,
						(org.apache.commons.collections.Transformer) var3);
		org.apache.commons.collections.buffer.BoundedFifoBuffer var5 = new org.apache.commons.collections.buffer.BoundedFifoBuffer();
		org.apache.commons.collections.Predicate var6 = org.apache.commons.collections.PredicateUtils
				.allPredicate((java.util.Collection) var5);
		org.apache.commons.collections.SortedBag var7 = org.apache.commons.collections.BagUtils
				.predicatedSortedBag(
						(org.apache.commons.collections.SortedBag) var4,
						(org.apache.commons.collections.Predicate) var6);
		boolean var10 = var7.add((java.lang.Object) " not known to ", 39);

	}

	
	public void test37() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test37");
		}

		org.apache.commons.collections.comparators.ReverseComparator var0 = new org.apache.commons.collections.comparators.ReverseComparator();
		org.apache.commons.collections.TreeBag var1 = new org.apache.commons.collections.TreeBag(
				(java.util.Comparator) var0);
		org.apache.commons.collections.Factory var2 = org.apache.commons.collections.FactoryUtils
				.nullFactory();
		org.apache.commons.collections.Transformer var3 = org.apache.commons.collections.functors.FactoryTransformer
				.getInstance((org.apache.commons.collections.Factory) var2);
		org.apache.commons.collections.SortedBag var4 = org.apache.commons.collections.BagUtils
				.transformedSortedBag(
						(org.apache.commons.collections.SortedBag) var1,
						(org.apache.commons.collections.Transformer) var3);
		boolean var7 = var4.add((java.lang.Object) "MapIterator[]", 94);

	}

	public void test38() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test38");
		}

		org.apache.commons.collections.BoundedFifoBuffer var0 = new org.apache.commons.collections.BoundedFifoBuffer();
		org.apache.commons.collections.iterators.IteratorEnumeration var1 = new org.apache.commons.collections.iterators.IteratorEnumeration();
		org.apache.commons.collections.CollectionUtils.addAll(
				(java.util.Collection) var0, (java.util.Enumeration) var1);

	}
	
	// ///////////////////////////deprecated method usage end///

	public void test43() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test43");
		}

		org.apache.commons.collections.iterators.ProxyListIterator var0 = new org.apache.commons.collections.iterators.ProxyListIterator();
		var0.remove();

	}

	public void test44() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test44");
		}

		org.apache.commons.collections.iterators.ProxyIterator var0 = new org.apache.commons.collections.iterators.ProxyIterator();
		java.util.ListIterator var1 = org.apache.commons.collections.IteratorUtils
				.toListIterator((java.util.Iterator) var0);
		java.lang.Object var2 = var1.next();

	}

	public void test45() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test45");
		}

		org.apache.commons.collections.HashBag var0 = new org.apache.commons.collections.HashBag();
		org.apache.commons.collections.Bag var1 = org.apache.commons.collections.BagUtils
				.synchronizedBag((org.apache.commons.collections.Bag) var0);
		boolean var3 = var1.add((java.lang.Object) var1, 31);
		java.lang.Object[] var4 = var1.toArray();

	}

	public void test46() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test46");
		}

		org.apache.commons.collections.iterators.ProxyListIterator var0 = new org.apache.commons.collections.iterators.ProxyListIterator();
		org.apache.commons.collections.iterators.SingletonIterator var3 = new org.apache.commons.collections.iterators.SingletonIterator(
				(java.lang.Object) "off", false);
		java.lang.Object[] var4 = org.apache.commons.collections.IteratorUtils
				.toArray((java.util.Iterator) var3);
		var0.set((java.lang.Object) var4);

	}
	
	public void test52() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test52");
		}

		org.apache.commons.collections.iterators.ProxyIterator var0 = new org.apache.commons.collections.iterators.ProxyIterator();
		var0.remove();

	}
	

	public void test54() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test54");
		}

		org.apache.commons.collections.iterators.ProxyListIterator var0 = new org.apache.commons.collections.iterators.ProxyListIterator();
		java.util.ListIterator var1 = org.apache.commons.collections.IteratorUtils
				.unmodifiableListIterator((java.util.ListIterator) var0);
		int var2 = var1.nextIndex();

	}

	public void test55() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test55");
		}

		org.apache.commons.collections.bidimap.DualTreeBidiMap var0 = new org.apache.commons.collections.bidimap.DualTreeBidiMap();
		org.apache.commons.collections.DefaultMapEntry var1 = new org.apache.commons.collections.DefaultMapEntry();
		org.apache.commons.collections.map.SingletonMap var2 = new org.apache.commons.collections.map.SingletonMap(
				(org.apache.commons.collections.KeyValue) var1);
		org.apache.commons.collections.map.LinkedMap var3 = new org.apache.commons.collections.map.LinkedMap(
				(java.util.Map) var2);
		var0.putAll((java.util.Map) var3);

	}

	public void test56() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test56");
		}

		org.apache.commons.collections.HashBag var0 = new org.apache.commons.collections.HashBag();
		org.apache.commons.collections.Bag var1 = org.apache.commons.collections.BagUtils
				.synchronizedBag((org.apache.commons.collections.Bag) var0);
		boolean var3 = var1.add((java.lang.Object) var1, 100);
		org.apache.commons.collections.Predicate var4 = org.apache.commons.collections.functors.UniquePredicate
				.getInstance();
		int var5 = org.apache.commons.collections.CollectionUtils.countMatches(
				(java.util.Collection) var1,
				(org.apache.commons.collections.Predicate) var4);

	}

	public void test57() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test57");
		}

		org.apache.commons.collections.MultiHashMap var2 = new org.apache.commons.collections.MultiHashMap(
				61, 0.18129212f);
		java.util.Collection var3 = var2.values();
		org.apache.commons.collections.OrderedIterator var4 = org.apache.commons.collections.IteratorUtils
				.emptyOrderedIterator();
		org.apache.commons.collections.Transformer var6 = org.apache.commons.collections.TransformerUtils
				.constantTransformer((java.lang.Object) "off");
		java.util.Iterator var7 = org.apache.commons.collections.IteratorUtils
				.transformedIterator((java.util.Iterator) var4,
						(org.apache.commons.collections.Transformer) var6);
		((TransformIterator) var7).setIterator((java.util.Iterator) var7);
		org.apache.commons.collections.CollectionUtils.addAll(
				(java.util.Collection) var3, (java.util.Iterator) var7);

	}

	public void test58() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test58");
		}

		org.apache.commons.collections.iterators.ProxyListIterator var0 = new org.apache.commons.collections.iterators.ProxyListIterator();
		java.util.ListIterator var1 = org.apache.commons.collections.IteratorUtils
				.unmodifiableListIterator((java.util.ListIterator) var0);
		java.lang.Object[] var2 = org.apache.commons.collections.IteratorUtils
				.toArray((java.util.Iterator) var1);

	}
	

	public void test61() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test61");
		}

		org.apache.commons.collections.comparators.FixedOrderComparator var0 = new org.apache.commons.collections.comparators.FixedOrderComparator();
		org.apache.commons.collections.comparators.ComparatorChain var1 = new org.apache.commons.collections.comparators.ComparatorChain(
				(java.util.Comparator) var0);
		org.apache.commons.collections.iterators.ProxyIterator var2 = new org.apache.commons.collections.iterators.ProxyIterator();
		java.util.ListIterator var3 = org.apache.commons.collections.IteratorUtils
				.toListIterator((java.util.Iterator) var2);
		java.util.Iterator[] var4 = new java.util.Iterator[] { var3 };
		java.util.Iterator var5 = org.apache.commons.collections.IteratorUtils
				.collatedIterator((java.util.Comparator) var1, var4);
		java.lang.Object var6 = var5.next();

	}

	public void test63() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test63");
		}

		org.apache.commons.collections.DefaultMapEntry var0 = new org.apache.commons.collections.DefaultMapEntry();
		org.apache.commons.collections.map.SingletonMap var1 = new org.apache.commons.collections.map.SingletonMap(
				(org.apache.commons.collections.KeyValue) var0);
		org.apache.commons.collections.DoubleOrderedMap var2 = new org.apache.commons.collections.DoubleOrderedMap(
				(java.util.Map) var1);

	}


	public void test65() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test65");
		}

		org.apache.commons.collections.BeanMap var0 = new org.apache.commons.collections.BeanMap();
		java.util.Iterator var1 = var0.valueIterator();
		java.util.List var2 = org.apache.commons.collections.IteratorUtils
				.toList((java.util.Iterator) var1);
		org.apache.commons.collections.Factory var5 = org.apache.commons.collections.functors.PrototypeFactory
				.getInstance((java.lang.Object) "1g3");
		java.util.List var6 = org.apache.commons.collections.list.LazyList
				.decorate((java.util.List) var2,
						(org.apache.commons.collections.Factory) var5);
		java.lang.Object var7 = var6.get(93);
		org.apache.commons.collections.bag.HashBag var8 = new org.apache.commons.collections.bag.HashBag(
				(java.util.Collection) var6);
		org.apache.commons.collections.Transformer var9 = org.apache.commons.collections.functors.ConstantTransformer
				.getInstance((java.lang.Object) "1g3");
		org.apache.commons.collections.Bag var10 = org.apache.commons.collections.bag.TransformedBag
				.decorate((org.apache.commons.collections.Bag) var8,
						(org.apache.commons.collections.Transformer) var9);
		org.apache.commons.collections.buffer.BoundedFifoBuffer var11 = new org.apache.commons.collections.buffer.BoundedFifoBuffer(
				(java.util.Collection) var10);

	}
	
	public void test68() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test68");
		}

		org.apache.commons.collections.iterators.ProxyListIterator var0 = new org.apache.commons.collections.iterators.ProxyListIterator();
		java.util.ListIterator var1 = org.apache.commons.collections.IteratorUtils
				.unmodifiableListIterator((java.util.ListIterator) var0);
		int var2 = var1.previousIndex();

	}

	public void test69() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test69");
		}

		org.apache.commons.collections.HashBag var0 = new org.apache.commons.collections.HashBag();
		org.apache.commons.collections.list.TreeList var1 = new org.apache.commons.collections.list.TreeList(
				(java.util.Collection) var0);
		java.util.Collection[] var2 = new java.util.Collection[] { var1 };
		org.apache.commons.collections.collection.CompositeCollection var3 = new org.apache.commons.collections.collection.CompositeCollection(
				var2);
		var3.addComposited((java.util.Collection) var3);
		boolean var5 = var3.isEmpty();

	}

	public void test77() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test77");
		}

		org.apache.commons.collections.iterators.ProxyListIterator var0 = new org.apache.commons.collections.iterators.ProxyListIterator();
		int var1 = var0.previousIndex();

	}
	
	public void test2() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test2");
		}

		org.apache.commons.collections.iterators.ProxyListIterator var0 = new org.apache.commons.collections.iterators.ProxyListIterator();
		java.lang.Object var1 = var0.previous();

	}

	public void test3() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test3");
		}

		org.apache.commons.collections.iterators.ProxyListIterator var0 = new org.apache.commons.collections.iterators.ProxyListIterator();
		java.lang.Object var1 = var0.next();

	}

	public void test4() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test4");
		}

		org.apache.commons.collections.iterators.ProxyIterator var0 = new org.apache.commons.collections.iterators.ProxyIterator();
		java.lang.Object var1 = var0.next();

	}
	
	public void test6() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test6");
		}

		org.apache.commons.collections.iterators.ProxyIterator var0 = new org.apache.commons.collections.iterators.ProxyIterator();
		org.apache.commons.collections.iterators.ProxyListIterator var1 = new org.apache.commons.collections.iterators.ProxyListIterator();
		java.util.ListIterator var2 = org.apache.commons.collections.IteratorUtils
				.unmodifiableListIterator((java.util.ListIterator) var1);
		java.util.Iterator var3 = org.apache.commons.collections.IteratorUtils
				.chainedIterator((java.util.Iterator) var0,
						(java.util.Iterator) var2);
		java.lang.Object var4 = var3.next();

	}
	
	public void test49() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test49");
		}

		org.apache.commons.collections.iterators.ProxyIterator var0 = new org.apache.commons.collections.iterators.ProxyIterator();
		java.util.ListIterator var1 = org.apache.commons.collections.IteratorUtils
				.toListIterator((java.util.Iterator) var0);
		boolean var2 = var1.hasNext();

	}
	
}
