package enhancement_3000_simplified;

import junit.framework.TestCase;

public class FalsePositive extends TestCase {

	
	public static boolean debug = false;

	
	public void test62() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test62");
		}

		org.apache.commons.collections.FastArrayList var0 = new org.apache.commons.collections.FastArrayList();
		var0.setFast(true);
		java.util.List var4 = var0.subList(20, 20);

		// Checks the contract: !var4.equals(null)
		assertTrue("Contract failed: !var4.equals(null)", !var4.equals(null));

	}
	
	
	public void test53() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test53");
		}

//		org.apache.commons.collections.comparators.BooleanComparator var0 = org.apache.commons.collections.comparators.BooleanComparator
//				.getFalseFirstComparator();
		org.apache.commons.collections.list.GrowthList var2 = new org.apache.commons.collections.list.GrowthList(
				1000);
		java.lang.Object var4 = var2.set(1000,
				(java.lang.Object) "Invalid array element: ");
		java.util.Iterator var5 = org.apache.commons.collections.IteratorUtils
				.collatedIterator(null,
						(java.util.Collection) var2);
	}
	
	public void test35() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test35");
		}

		org.apache.commons.collections.comparators.NullComparator var1 = new org.apache.commons.collections.comparators.NullComparator(
				true);
		org.apache.commons.collections.list.GrowthList var3 = new org.apache.commons.collections.list.GrowthList(
				1000);
		boolean var4 = var3.addAll(1000, (java.util.Collection) var3);
		org.apache.commons.collections.iterators.CollatingIterator var5 = new org.apache.commons.collections.iterators.CollatingIterator(
				(java.util.Comparator) var1, (java.util.Collection) var3);
	}
	
	// A similar test in 4.0 was confirmed as a false positive
	public void test51() throws Throwable {
		org.apache.commons.collections.map.ReferenceIdentityMap var0 = new org.apache.commons.collections.map.ReferenceIdentityMap();
		java.util.Set var1 = org.apache.commons.collections.set.MapBackedSet
				.decorate((java.util.Map) var0);
		boolean var3 = var1
				.add((java.lang.Object) "");
	}

}
