package enhancement_3000_simplified;

import java.util.Map;

import org.apache.commons.collections.iterators.TransformIterator;
import org.apache.commons.collections.map.CompositeMap;

import junit.framework.*;

public class ContractNotEnforcedTest extends TestCase {

	public static boolean debug = false;




	// documented contract not enforced at runtime by throwing an exception.
	public void test17() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test17");
		}

		org.apache.commons.collections.buffer.BoundedFifoBuffer var0 = new org.apache.commons.collections.buffer.BoundedFifoBuffer();
//		org.apache.commons.collections.Buffer var1 = org.apache.commons.collections.BufferUtils
//				.unmodifiableBuffer((org.apache.commons.collections.Buffer) var0);
		org.apache.commons.collections.buffer.BoundedBuffer var5 = org.apache.commons.collections.buffer.BoundedBuffer
				.decorate((org.apache.commons.collections.Buffer) var0, 255,
						100L);

		// This assertion (symmetry of equals) fails
		assertTrue("Contract failed: equals-symmetric on var7 and var2.",
				var5.equals(var0) == var0.equals(var5));
	}
	
	
	public void test5() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test5");
		}

		org.apache.commons.collections.iterators.EnumerationIterator var0 = new org.apache.commons.collections.iterators.EnumerationIterator();
		org.apache.commons.collections.iterators.IteratorEnumeration var1 = new org.apache.commons.collections.iterators.IteratorEnumeration(
				(java.util.Iterator) var0);
		java.lang.Object var2 = var1.nextElement();

	}
	
	public void test7() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test7");
		}

		org.apache.commons.collections.iterators.FilterListIterator var0 = new org.apache.commons.collections.iterators.FilterListIterator();
		java.lang.Object var1 = var0.next();

	}

	public void test8() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test8");
		}

		org.apache.commons.collections.iterators.FilterListIterator var0 = new org.apache.commons.collections.iterators.FilterListIterator();
		org.apache.commons.collections.iterators.UniqueFilterIterator var1 = new org.apache.commons.collections.iterators.UniqueFilterIterator(
				(java.util.Iterator) var0);
		java.lang.Object var2 = var1.next();

	}
	
	public void test9() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test9");
		}

		org.apache.commons.collections.iterators.IteratorEnumeration var0 = new org.apache.commons.collections.iterators.IteratorEnumeration();
		org.apache.commons.collections.list.GrowthList var2 = new org.apache.commons.collections.list.GrowthList(
				1000);
		java.util.Iterator var3 = org.apache.commons.collections.IteratorUtils
				.asIterator((java.util.Enumeration) var0,
						(java.util.Collection) var2);
		java.lang.Object var4 = var3.next();

	}


	public void test16() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test16");
		}
		org.apache.commons.collections.iterators.FilterIterator var0 = new org.apache.commons.collections.iterators.FilterIterator();
		boolean var1 = var0.hasNext();
	}

	
	public void test23() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test23");
		}

		org.apache.commons.collections.iterators.TransformIterator var0 = new org.apache.commons.collections.iterators.TransformIterator();
		boolean var1 = var0.hasNext();

	}
	
	public void test24() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test24");
		}

		org.apache.commons.collections.iterators.EnumerationIterator var0 = new org.apache.commons.collections.iterators.EnumerationIterator();
		org.apache.commons.collections.iterators.IteratorEnumeration var1 = new org.apache.commons.collections.iterators.IteratorEnumeration(
				(java.util.Iterator) var0);
		boolean var2 = var1.hasMoreElements();
	}
	
	public void test25() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test25");
		}

		org.apache.commons.collections.iterators.EnumerationIterator var0 = new org.apache.commons.collections.iterators.EnumerationIterator();
		boolean var1 = var0.hasNext();

	}
	
	
	public void test29() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test29");
		}

		org.apache.commons.collections.iterators.TransformIterator var0 = new org.apache.commons.collections.iterators.TransformIterator();
		var0.remove();

	}
	
	public void test34() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test34");
		}

		org.apache.commons.collections.iterators.FilterIterator var0 = new org.apache.commons.collections.iterators.FilterIterator();
		java.util.List var2 = org.apache.commons.collections.IteratorUtils
				.toList((java.util.Iterator) var0, 39);

	}
	
	public void test47() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test47");
		}

		org.apache.commons.collections.iterators.FilterListIterator var0 = new org.apache.commons.collections.iterators.FilterListIterator();
		boolean var1 = var0.hasPrevious();

	}



	public void test50() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test50");
		}

		org.apache.commons.collections.iterators.FilterIterator var0 = new org.apache.commons.collections.iterators.FilterIterator();
		var0.remove();

	}

	
	//requires setIterrator method to be invoked to make it functional, bugs
	public void test1() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test1");
		}

		org.apache.commons.collections.iterators.TransformIterator var0 = new org.apache.commons.collections.iterators.TransformIterator();
		java.lang.Object var1 = var0.next();

	}
	
	// ///////////////////////////not sure suspious start///
	// ////////////////////////////////
	// Parameters:
	// enumeration - the enumeration to traverse, which should not be null.
	// Throws:
	// java.lang.NullPointerException - if the enumeration parameter is null.

	public void test10() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test10");
		}

		org.apache.commons.collections.iterators.IteratorEnumeration var0 = new org.apache.commons.collections.iterators.IteratorEnumeration();
		java.util.List var1 = org.apache.commons.collections.EnumerationUtils
				.toList((java.util.Enumeration) var0);

	}
	
	public void test28() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test28");
		}

		java.util.ListIterator var1 = org.apache.commons.collections.IteratorUtils
				.singletonListIterator((java.lang.Object) 4189952);
		org.apache.commons.collections.iterators.FilterListIterator var2 = new org.apache.commons.collections.iterators.FilterListIterator(
				(java.util.ListIterator) var1);
		boolean var3 = var2.hasNext();

	}
	
	public void test67() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test67");
		}

		org.apache.commons.collections.Predicate var0 = org.apache.commons.collections.functors.UniquePredicate
				.getInstance();
		org.apache.commons.collections.Predicate var1 = org.apache.commons.collections.functors.ExceptionPredicate
				.getInstance();
		org.apache.commons.collections.Predicate var2 = org.apache.commons.collections.PredicateUtils
				.neitherPredicate(
						(org.apache.commons.collections.Predicate) var0,
						(org.apache.commons.collections.Predicate) var1);
		org.apache.commons.collections.iterators.FilterListIterator var3 = new org.apache.commons.collections.iterators.FilterListIterator(
				(org.apache.commons.collections.Predicate) var2);
		java.lang.Object var4 = var3.previous();

	}
	
	
	// ///////////////////////////contract violation end///

}
