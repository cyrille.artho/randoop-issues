package enhancement_3000_simplified;


import java.util.Map;

import org.apache.commons.collections.iterators.TransformIterator;
import org.apache.commons.collections.map.CompositeMap;

import junit.framework.*;

public class RecursiveDataStructureTest extends TestCase {

  public static boolean debug = false;


	// ///////////////////////////recursive data structure bugs start///

  public void test22() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test22");
		}

		org.apache.commons.collections.set.ListOrderedSet var0 = new org.apache.commons.collections.set.ListOrderedSet();
		java.util.Set var1 = org.apache.commons.collections.SetUtils
				.unmodifiableSet((java.util.Set) var0);
		java.util.Set[] var2 = new java.util.Set[] { var1 };
		org.apache.commons.collections.set.CompositeSet var3 = new org.apache.commons.collections.set.CompositeSet(
				var2);
		org.apache.commons.collections.set.CompositeSet var4 = new org.apache.commons.collections.set.CompositeSet(
				var2);
		var4.addComposited((java.util.Collection) var4);
		boolean var6 = var3.removeAll((java.util.Collection) var3);

		// Checks the contract: var4.equals(var4)
		assertTrue("Contract failed: var4.equals(var4)", var4.equals(var4));

	}

	public void test42() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test42");
		}

		org.apache.commons.collections.set.CompositeSet var0 = new org.apache.commons.collections.set.CompositeSet();
		var0.addComposited((java.util.Collection) var0);

		// Checks the contract: var0.equals(var0)
		assertTrue("Contract failed: var0.equals(var0)", var0.equals(var0));

	}
  
	public void test12() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test12");
		}

		org.apache.commons.collections.buffer.CircularFifoBuffer var1 = new org.apache.commons.collections.buffer.CircularFifoBuffer(
				128);
		java.util.Collection var2 = org.apache.commons.collections.CollectionUtils
				.synchronizedCollection((java.util.Collection) var1);
		Boolean var3 = var1.add((java.lang.Object) var2);
		java.lang.Class var4 = var3.getClass();
		org.apache.commons.collections.Buffer var5 = org.apache.commons.collections.BufferUtils
				.typedBuffer((org.apache.commons.collections.Buffer) var1, var4);
	}
	
	public void test19() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test19");
		}

		org.apache.commons.collections.comparators.NullComparator var1 = new org.apache.commons.collections.comparators.NullComparator(
				false);
		org.apache.commons.collections.buffer.PriorityBuffer var2 = new org.apache.commons.collections.buffer.PriorityBuffer(
				false, (java.util.Comparator) var1);
		boolean var3 = var2.add((java.lang.Object) var2);
		java.lang.String var4 = var2.toString();

	}

	public void test72() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test72");
		}

		org.apache.commons.collections.MultiHashMap var2 = new org.apache.commons.collections.MultiHashMap(
				20, 30.937235f);
		java.lang.Object var3 = var2.clone();
		java.lang.Object var4 = ((org.apache.commons.collections.MultiHashMap)var3).put(
				(java.lang.Object) var3, (java.lang.Object) var2);
		 java.lang.Object var5 = ((org.apache.commons.collections.MultiHashMap)var3).clone();
	}
	

	public void test74() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test74");
		}

		org.apache.commons.collections.FastHashMap var2 = new org.apache.commons.collections.FastHashMap(
				23, 1.0f);
		org.apache.commons.collections.StaticBucketMap var3 = new org.apache.commons.collections.StaticBucketMap(
				23);
		java.lang.Object var4 = var3.put((java.lang.Object) var3,
				(java.lang.Object) var3);
		var2.putAll((java.util.Map) var3);

	}
	
	
	public void test75() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test75");
		}

		org.apache.commons.collections.functors.ConstantTransformer var1 = new org.apache.commons.collections.functors.ConstantTransformer(
				(java.lang.Object) "} ");
		org.apache.commons.collections.comparators.NullComparator var2 = new org.apache.commons.collections.comparators.NullComparator();
		org.apache.commons.collections.comparators.TransformingComparator var3 = new org.apache.commons.collections.comparators.TransformingComparator(
				(org.apache.commons.collections.Transformer) var1,
				(java.util.Comparator) var2);
		org.apache.commons.collections.BinaryHeap var4 = new org.apache.commons.collections.BinaryHeap(
				(java.util.Comparator) var3);
		boolean var5 = var4.add((java.lang.Object) var4);
		java.lang.String var6 = var4.toString();

	}
	
	
	 public void test73() throws Throwable {

		    if (debug) { System.out.println(); System.out.print("RandoopTest0.test73"); }


		    org.apache.commons.collections.map.CompositeMap var0 = new org.apache.commons.collections.map.CompositeMap();
		    java.util.Set var1 = var0.entrySet();
		    java.util.Set var2 = var0.keySet();
		    ((org.apache.commons.collections.set.CompositeSet)var2).addComposited((java.util.Collection)var2);
//		    org.apache.commons.collections.BeanMap var4 = new org.apache.commons.collections.BeanMap();
//		    java.util.Set var5 = var4.keySet();
//		    java.util.Collection[] var6 = new java.util.Collection[] { var5};
//		    var1.addComposited(var6);
		    
		    // Checks the contract:  var2.equals(var2)
		    assertTrue("Contract failed: var2.equals(var2)", var2.equals(var2));

		  }
	
	// ///////////////////////////recursive data structure bugs end///
  
	public void test78() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test78");
		}

		org.apache.commons.collections.set.CompositeSet var0 = new org.apache.commons.collections.set.CompositeSet();
		java.util.Set var1 = org.apache.commons.collections.SetUtils
				.synchronizedSet((java.util.Set) var0);
		var0.addComposited((java.util.Collection) var1);
		org.apache.commons.collections.BinaryHeap var5 = new org.apache.commons.collections.BinaryHeap(
				18, false);
		boolean var6 = var0.retainAll((java.util.Collection) var5);

	}
  
	public void test33() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test33");
		}

		org.apache.commons.collections.FastHashMap var2 = new org.apache.commons.collections.FastHashMap(
				0, 0.75f);
		java.lang.Object var3 = var2.put((java.lang.Object) var2,
				(java.lang.Object) var2);
		java.util.Properties var4 = org.apache.commons.collections.MapUtils
				.toProperties((java.util.Map) var2);

	}
	
	
	public void test48() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test48");
		}

		org.apache.commons.collections.set.CompositeSet var0 = new org.apache.commons.collections.set.CompositeSet();
		java.util.Set var1 = org.apache.commons.collections.SetUtils
				.synchronizedSet((java.util.Set) var0);
		var0.addComposited((java.util.Collection) var1);
		java.util.Collection var3 = var0.toCollection();

	}
	
	public void test59() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test59");
		}

		org.apache.commons.collections.set.CompositeSet var0 = new org.apache.commons.collections.set.CompositeSet();
		var0.addComposited((java.util.Collection) var0);
		boolean var3 = var0
				.remove((java.lang.Object) "Th^ pr^dicat^ and transform^r arrays must b^ th^ sam^ siz^");

	}
	
	public void test64() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test64");
		}

		org.apache.commons.collections.set.ListOrderedSet var0 = new org.apache.commons.collections.set.ListOrderedSet();
		java.util.Set var1 = org.apache.commons.collections.SetUtils
				.unmodifiableSet((java.util.Set) var0);
		java.util.Set[] var2 = new java.util.Set[] { var1 };
		org.apache.commons.collections.set.CompositeSet var3 = new org.apache.commons.collections.set.CompositeSet(
				var2);
		var3.addComposited((java.util.Collection) var3);
		boolean var6 = var3
				.contains((java.lang.Object) "The factory must not be null");

	}
	
	public void test66() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test66");
		}

		org.apache.commons.collections.map.HashedMap var2 = new org.apache.commons.collections.map.HashedMap(
				93, 1.4f);
		org.apache.commons.collections.map.DefaultedMap var4 = new org.apache.commons.collections.map.DefaultedMap(
				(java.lang.Object) "java.lang.Throwable");
		org.apache.commons.collections.Factory var5 = org.apache.commons.collections.FactoryUtils
				.nullFactory();
		java.util.Map var6 = org.apache.commons.collections.map.LazyMap
				.decorate((java.util.Map) var4,
						(org.apache.commons.collections.Factory) var5);
		java.lang.Object var7 = var6.get((java.lang.Object) var6);
		boolean var8 = org.apache.commons.collections.MapUtils.getBooleanValue(
				(java.util.Map) var2, (java.lang.Object) var6);
	}
	

	public void test70() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test70");
		}

		org.apache.commons.collections.map.ListOrderedMap var0 = new org.apache.commons.collections.map.ListOrderedMap();
		java.util.Set var1 = var0.keySet();
		java.lang.Object var2 = var0.put((java.lang.Object) var1,
				(java.lang.Object) var1);
		java.lang.String var3 = var0.toString();

	}

	public void test71() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test71");
		}

		org.apache.commons.collections.set.ListOrderedSet var0 = new org.apache.commons.collections.set.ListOrderedSet();
		boolean var1 = var0.add((java.lang.Object) var0);
		org.apache.commons.collections.FastTreeMap var2 = new org.apache.commons.collections.FastTreeMap();
		java.util.Collection var3 = var2.values();
		boolean var4 = var0.retainAll((java.util.Collection) var3);

	}
	
}
