package enhancement_3000_simplified;

public class ContractExceptionMismatch {
	
	public static boolean debug = false;

	// Parameters:
	// predicatesAndClosures - a map of predicates to closures
	// Returns:
	// the switch closure
	// Throws:
	// java.lang.IllegalArgumentException - if the map is null
	// java.lang.IllegalArgumentException - if the map is empty
	// java.lang.IllegalArgumentException - if any closure in the map is null
	// java.lang.ClassCastException - if the map elements are of the wrong type

	
	//contract  : java.lang.IllegalArgumentException is not thrown
	public void test11() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test11");
		}

		org.apache.commons.collections.bidimap.DualTreeBidiMap var0 = new org.apache.commons.collections.bidimap.DualTreeBidiMap();
		org.apache.commons.collections.Closure var1 = org.apache.commons.collections.ClosureUtils
				.switchMapClosure((java.util.Map) var0);

	}
	
	
	// java.lang.IllegalArgumentException is not thrown
	public void test41() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test41");
		}

		org.apache.commons.collections.FastTreeMap var0 = new org.apache.commons.collections.FastTreeMap();
		org.apache.commons.collections.Transformer var1 = org.apache.commons.collections.TransformerUtils
				.switchMapTransformer((java.util.Map) var0);

	}
	
	
	
	public void test60() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test60");
		}

		org.apache.commons.collections.iterators.IteratorEnumeration var0 = new org.apache.commons.collections.iterators.IteratorEnumeration();
		org.apache.commons.collections.map.ListOrderedMap var1 = new org.apache.commons.collections.map.ListOrderedMap();
		java.util.Set var2 = var1.entrySet();
		org.apache.commons.collections.iterators.EnumerationIterator var3 = new org.apache.commons.collections.iterators.EnumerationIterator(
				(java.util.Enumeration) var0, (java.util.Collection) var2);
		org.apache.commons.collections.Predicate var4 = org.apache.commons.collections.functors.NotNullPredicate
				.getInstance();
		org.apache.commons.collections.functors.PredicateTransformer var5 = new org.apache.commons.collections.functors.PredicateTransformer(
				(org.apache.commons.collections.Predicate) var4);
		org.apache.commons.collections.map.StaticBucketMap var7 = new org.apache.commons.collections.map.StaticBucketMap(
				106);
		java.util.Set var8 = var7.keySet();
		org.apache.commons.collections.Predicate var10 = org.apache.commons.collections.PredicateUtils
				.equalPredicate((java.lang.Object) "' doesn't map to an existing object");
		org.apache.commons.collections.Predicate[] var11 = new org.apache.commons.collections.Predicate[] { var10 };
		org.apache.commons.collections.functors.NonePredicate var12 = new org.apache.commons.collections.functors.NonePredicate(
				var11);
		java.util.Set var13 = org.apache.commons.collections.SetUtils
				.predicatedSet((java.util.Set) var8,
						(org.apache.commons.collections.Predicate) var12);
		java.util.Collection var14 = org.apache.commons.collections.CollectionUtils
				.collect((java.util.Iterator) var3,
						(org.apache.commons.collections.Transformer) var5,
						(java.util.Collection) var13);

	}
	
	
	
	
}
