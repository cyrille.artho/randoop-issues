import junit.framework.*;

public class InternetAddressTest extends TestCase {
  // NullPointerException
  public void test9() throws Throwable {
    new javax.mail.internet.InternetAddress().validate();
  }

  // NullPointerException
  public void test12() throws Throwable {
    new javax.mail.internet.InternetAddress().getGroup(false);
  }

  // NullPointerException
  public void test11() throws Throwable {
    javax.mail.internet.InternetAddress var0 = new javax.mail.internet.InternetAddress();
    javax.mail.Address[] var1 = new javax.mail.Address[] { var0};
    java.lang.String var3 = javax.mail.internet.InternetAddress.toString(var1);
  }

  // NullPointerException, fails as test above, but in more complex setting
  public void test17() throws Throwable {
    com.sun.mail.util.logging.MailHandler var2 = new com.sun.mail.util.logging.MailHandler();
    java.util.Properties var6 = var2.getMailProperties();
    javax.mail.Session var11 = javax.mail.Session.getInstance(var6);
    javax.mail.internet.MimeMessage var12 = new javax.mail.internet.MimeMessage(var11);

    javax.mail.internet.InternetAddress var30 = new javax.mail.internet.InternetAddress();
    javax.mail.Address[] var31 = new javax.mail.Address[] { var30};
    var12.addFrom(var31);
    // also fails in the same way when using var12.setReplyTo(var31);
  }
}
