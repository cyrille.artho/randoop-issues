import junit.framework.*;

public class ContentTypeMatch extends TestCase {

  // same as second issue reported by plain Randoop
  public void test18() throws Throwable {

    javax.mail.internet.ContentType var0 = new javax.mail.internet.ContentType();
    java.lang.String var2 = var0.getParameter("STLS required but not supported");
    var0.setSubType("Could not convert socket to TLST%");
    javax.mail.internet.ContentType var5 = new javax.mail.internet.ContentType();
    var5.setParameter(":", " is not equal to ");
    var5.setParameter("begin", "QUOTA not supported");
    var5.setParameter("QUOTA not supported", "QUOTA not supported");
    java.lang.Object var13 = component.model.IdentityMethod.IdentityForward((java.lang.Object)var5);
    var5.setParameter("UNSEEN", "Could not connect to SMTP host: ");
    boolean var17 = var0.match(var5);

  }
}
