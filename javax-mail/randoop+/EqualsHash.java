import junit.framework.*;

import com.sun.mail.imap.ModifiedSinceTerm;
import com.sun.mail.imap.YoungerTerm;

public class EqualsHash extends TestCase {
  public void test3() throws Throwable {
    ModifiedSinceTerm var1 = new ModifiedSinceTerm(0L);
    ModifiedSinceTerm var3 = new ModifiedSinceTerm(0L);
    
    // Checks the contract:  equals-hashcode on var1 and var3
    assertTrue("Contract failed: equals-hashcode on var1 and var3", var1.equals(var3) ? var1.hashCode() == var3.hashCode() : true);
  }

  public void test5() throws Throwable {
    YoungerTerm var2 = new YoungerTerm(0);
    YoungerTerm var11 = new YoungerTerm(0);
    // Checks the contract:  equals-hashcode on var2 and var11
    assertTrue("Contract failed: equals-hashcode on var2 and var11", var2.equals(var11) ? var2.hashCode() == var11.hashCode() : true);
  }
}
