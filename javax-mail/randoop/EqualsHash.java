import junit.framework.*;

public class EqualsHash extends TestCase {
  public void test3() throws Throwable {
    com.sun.mail.imap.YoungerTerm var1 = new com.sun.mail.imap.YoungerTerm(100);
    com.sun.mail.imap.YoungerTerm var5 = new com.sun.mail.imap.YoungerTerm(100);
    
    // Checks the contract:  equals-hashcode on var1 and var5
    assertTrue("Contract failed: equals-hashcode on var1 and var5", var1.equals(var5) ? var1.hashCode() == var5.hashCode() : true);
  }

  // public void test17() throws Throwable is a duplicate
}
