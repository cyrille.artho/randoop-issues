import junit.framework.*;

public class Equals extends TestCase {
  // x != x
  public void test1() throws Throwable {
    javax.mail.URLName var1 = new javax.mail.URLName("hi!");
    
    // Checks the contract:  var1.equals(var1)
    assertTrue("Contract failed: var1.equals(var1)", var1.equals(var1));

  }

  // equals throws NullPointerException
  public void test5() throws Throwable {
    javax.mail.internet.NewsAddress var0 = new javax.mail.internet.NewsAddress();
    
    // Checks the contract:  var0.equals(var0)
    assertTrue("Contract failed: var0.equals(var0)", var0.equals(var0));
  }
}
