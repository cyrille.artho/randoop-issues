import junit.framework.*;
  import org.apache.commons.math3.complex.Complex;

public class Report5 extends TestCase {
  public void test8() throws Throwable {
    Complex c1 = new Complex(0.0, 0.0);
    Complex c2 = new Complex(0.0, -0.0);
    // Checks the contract:  equals-hashcode on c1 and c2
    assertTrue("Contract failed: equals-hashcode on c1 and c2", c1.equals(c2) ? c1.hashCode() == c2.hashCode() : true);
  }
}
