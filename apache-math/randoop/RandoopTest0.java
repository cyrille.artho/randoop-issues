package randoop;


import junit.framework.*;

public class RandoopTest0 extends TestCase {
  public void test21() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test21"); }


    org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression();
    boolean var1 = var0.isNoIntercept();
    double var2 = var0.estimateRegressionStandardError();

  }

  public void test22() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test22"); }


    java.lang.Double[] var1 = new java.lang.Double[] { (-0.09966865249116202d)};
    org.apache.commons.math3.linear.OpenMapRealVector var2 = new org.apache.commons.math3.linear.OpenMapRealVector(var1);
    org.apache.commons.math3.linear.OpenMapRealVector var3 = new org.apache.commons.math3.linear.OpenMapRealVector((org.apache.commons.math3.linear.RealVector)var2);
    java.lang.Double[] var5 = new java.lang.Double[] { (-0.09966865249116202d)};
    org.apache.commons.math3.linear.OpenMapRealVector var6 = new org.apache.commons.math3.linear.OpenMapRealVector(var5);
    org.apache.commons.math3.linear.RealVector var7 = var3.projection((org.apache.commons.math3.linear.RealVector)var6);
    org.apache.commons.math3.analysis.function.Exp var8 = new org.apache.commons.math3.analysis.function.Exp();
    org.apache.commons.math3.linear.RealVector var9 = var7.map((org.apache.commons.math3.analysis.UnivariateFunction)var8);
    org.apache.commons.math3.linear.ArrayRealVector var10 = new org.apache.commons.math3.linear.ArrayRealVector(var7);
    org.apache.commons.math3.linear.RealVector var12 = var10.append(10.0d);
    java.lang.Double[] var14 = new java.lang.Double[] { (-0.09966865249116202d)};
    org.apache.commons.math3.linear.OpenMapRealVector var15 = new org.apache.commons.math3.linear.OpenMapRealVector(var14);
    org.apache.commons.math3.linear.OpenMapRealVector var16 = new org.apache.commons.math3.linear.OpenMapRealVector((org.apache.commons.math3.linear.RealVector)var15);
    java.lang.Double[] var18 = new java.lang.Double[] { (-0.09966865249116202d)};
    org.apache.commons.math3.linear.OpenMapRealVector var19 = new org.apache.commons.math3.linear.OpenMapRealVector(var18);
    org.apache.commons.math3.linear.RealVector var20 = var16.projection((org.apache.commons.math3.linear.RealVector)var19);
    org.apache.commons.math3.analysis.function.Exp var21 = new org.apache.commons.math3.analysis.function.Exp();
    org.apache.commons.math3.linear.RealVector var22 = var20.map((org.apache.commons.math3.analysis.UnivariateFunction)var21);
    org.apache.commons.math3.linear.ArrayRealVector var23 = new org.apache.commons.math3.linear.ArrayRealVector(var20);
    org.apache.commons.math3.linear.RealVector var24 = var10.append(var20);
    java.lang.Double[] var26 = new java.lang.Double[] { (-0.09966865249116202d)};
    org.apache.commons.math3.linear.OpenMapRealVector var27 = new org.apache.commons.math3.linear.OpenMapRealVector(var26);
    org.apache.commons.math3.linear.OpenMapRealVector var28 = new org.apache.commons.math3.linear.OpenMapRealVector(var27);
    double var29 = var20.cosine((org.apache.commons.math3.linear.RealVector)var28);
    org.apache.commons.math3.optimization.linear.LinearObjectiveFunction var31 = new org.apache.commons.math3.optimization.linear.LinearObjectiveFunction(var20, 11013.232874703393d);
    java.lang.Double[] var33 = new java.lang.Double[] { (-0.09966865249116202d)};
    org.apache.commons.math3.linear.OpenMapRealVector var34 = new org.apache.commons.math3.linear.OpenMapRealVector(var33);
    org.apache.commons.math3.linear.OpenMapRealVector var35 = new org.apache.commons.math3.linear.OpenMapRealVector((org.apache.commons.math3.linear.RealVector)var34);
    java.lang.Double[] var37 = new java.lang.Double[] { (-0.09966865249116202d)};
    org.apache.commons.math3.linear.OpenMapRealVector var38 = new org.apache.commons.math3.linear.OpenMapRealVector(var37);
    org.apache.commons.math3.linear.RealVector var39 = var35.projection((org.apache.commons.math3.linear.RealVector)var38);
    org.apache.commons.math3.analysis.function.Exp var40 = new org.apache.commons.math3.analysis.function.Exp();
    org.apache.commons.math3.linear.RealVector var41 = var39.map((org.apache.commons.math3.analysis.UnivariateFunction)var40);
    org.apache.commons.math3.linear.ArrayRealVector var42 = new org.apache.commons.math3.linear.ArrayRealVector(var39);
    org.apache.commons.math3.linear.RealVector var44 = var42.append(10.0d);
    org.apache.commons.math3.linear.RealVector var45 = var20.projection((org.apache.commons.math3.linear.RealVector)var42);
    
    // Checks the contract:  equals-hashcode on var45 and var7
    assertTrue("Contract failed: equals-hashcode on var45 and var7", var45.equals(var7) ? var45.hashCode() == var7.hashCode() : true);
    
    // Checks the contract:  equals-hashcode on var45 and var20
    assertTrue("Contract failed: equals-hashcode on var45 and var20", var45.equals(var20) ? var45.hashCode() == var20.hashCode() : true);
    
    // Checks the contract:  equals-hashcode on var45 and var39
    assertTrue("Contract failed: equals-hashcode on var45 and var39", var45.equals(var39) ? var45.hashCode() == var39.hashCode() : true);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var45 and var7.", var45.equals(var7) == var7.equals(var45));
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var45 and var20.", var45.equals(var20) == var20.equals(var45));
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var45 and var39.", var45.equals(var39) == var39.equals(var45));

  }

  public void test23() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test23"); }


    org.apache.commons.math3.optimization.general.GaussNewtonOptimizer var0 = new org.apache.commons.math3.optimization.general.GaussNewtonOptimizer();
    org.apache.commons.math3.optimization.PointVectorValuePair var1 = var0.doOptimize();

  }

  public void test24() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test24"); }


    org.apache.commons.math3.distribution.PoissonDistribution var3 = new org.apache.commons.math3.distribution.PoissonDistribution(Double.NaN, 0.500002947286864d, 5);
    int[] var5 = var3.sample(5);

  }

  public void test25() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test25"); }


    org.apache.commons.math3.stat.descriptive.summary.SumOfSquares var2 = new org.apache.commons.math3.stat.descriptive.summary.SumOfSquares();
    double[] var6 = new double[] { 10.0d};
    double[] var8 = new double[] { 0.0d};
    org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator var9 = new org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator(1.0d, (-0.09966865249116202d), var6, var8);
    var2.incrementAll(var6);
    double[] var13 = new double[] { 10.0d, 1.0d};
    org.apache.commons.math3.optim.PointVectorValuePair var14 = new org.apache.commons.math3.optim.PointVectorValuePair(var6, var13);
    double var15 = org.apache.commons.math3.stat.StatUtils.sumSq(var6);
    org.apache.commons.math3.random.Well19937c var26 = new org.apache.commons.math3.random.Well19937c();
    org.apache.commons.math3.optimization.SimpleVectorValueChecker var28 = new org.apache.commons.math3.optimization.SimpleVectorValueChecker();
    org.apache.commons.math3.optimization.direct.CMAESOptimizer var29 = new org.apache.commons.math3.optimization.direct.CMAESOptimizer(0, 0.0d, true, (-1), 1, (org.apache.commons.math3.random.RandomGenerator)var26, true, (org.apache.commons.math3.optimization.ConvergenceChecker)var28);
    org.apache.commons.math3.distribution.HypergeometricDistribution var33 = new org.apache.commons.math3.distribution.HypergeometricDistribution((org.apache.commons.math3.random.RandomGenerator)var26, 100, 1, 10);
    org.apache.commons.math3.distribution.ChiSquaredDistribution var36 = new org.apache.commons.math3.distribution.ChiSquaredDistribution((org.apache.commons.math3.random.RandomGenerator)var26, 0.009933840289404017d, 10000.0d);
    org.apache.commons.math3.optimization.SimpleVectorValueChecker var38 = new org.apache.commons.math3.optimization.SimpleVectorValueChecker();
    org.apache.commons.math3.optimization.direct.CMAESOptimizer var39 = new org.apache.commons.math3.optimization.direct.CMAESOptimizer(10000, var6, (-1), (-0.06394368557988182d), true, 10053, 10000, (org.apache.commons.math3.random.RandomGenerator)var26, true, (org.apache.commons.math3.optimization.ConvergenceChecker)var38);
    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var44 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer((-1.0d), (org.apache.commons.math3.optimization.ConvergenceChecker)var38, 0.15729920705028488d, (-0.09983374988500592d), (-0.06394368557988182d), (-0.4999975593213537d));
    double var45 = var44.getChiSquare();
    double[] var46 = var44.getStartPoint();

  }

  public void test26() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test26"); }


    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
    var0.setSoftPreviousTime(Double.POSITIVE_INFINITY);
    double[] var4 = var0.getInterpolatedSecondaryState((-1));

  }

  public void test27() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test27"); }


    org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression();
    boolean var1 = var0.isNoIntercept();
    boolean var2 = var0.isNoIntercept();
    double var3 = var0.estimateRegressandVariance();

  }

  public void test28() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test28"); }


    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var5 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(0.15729920705028488d, (-1.0d), (-0.4999975593213537d), (-0.09966865249116202d), (-0.09966865249116202d));
    double var6 = var5.getChiSquare();
    double var7 = var5.getChiSquare();
    double var8 = var5.getRMS();
    org.apache.commons.math3.random.Well19937c var15 = new org.apache.commons.math3.random.Well19937c();
    org.apache.commons.math3.optimization.SimpleVectorValueChecker var17 = new org.apache.commons.math3.optimization.SimpleVectorValueChecker();
    org.apache.commons.math3.optimization.direct.CMAESOptimizer var18 = new org.apache.commons.math3.optimization.direct.CMAESOptimizer(0, 0.0d, true, (-1), 1, (org.apache.commons.math3.random.RandomGenerator)var15, true, (org.apache.commons.math3.optimization.ConvergenceChecker)var17);
    org.apache.commons.math3.random.UnitSphereRandomVectorGenerator var21 = new org.apache.commons.math3.random.UnitSphereRandomVectorGenerator(100);
    org.apache.commons.math3.optimization.MultivariateMultiStartOptimizer var22 = new org.apache.commons.math3.optimization.MultivariateMultiStartOptimizer((org.apache.commons.math3.optimization.MultivariateOptimizer)var18, 100, (org.apache.commons.math3.random.RandomVectorGenerator)var21);
    org.apache.commons.math3.optimization.DifferentiableMultivariateVectorMultiStartOptimizer var23 = new org.apache.commons.math3.optimization.DifferentiableMultivariateVectorMultiStartOptimizer((org.apache.commons.math3.optimization.DifferentiableMultivariateVectorOptimizer)var5, 3, (org.apache.commons.math3.random.RandomVectorGenerator)var21);
    double[] var24 = var5.getTarget();

  }

  public void test29() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test29"); }


    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var5 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer((-0.0639872699254868d), (-0.4999975593213537d), (-1.0d), 1.0d, (-0.09966865249116202d));
    int var6 = var5.getMaxIterations();
    org.apache.commons.math3.stat.descriptive.DescriptiveStatistics var8 = new org.apache.commons.math3.stat.descriptive.DescriptiveStatistics(2147483647);
    org.apache.commons.math3.stat.descriptive.UnivariateStatistic var9 = var8.getVarianceImpl();
    double[] var10 = var8.getValues();
    double[] var12 = var5.computeSigma(var10, (-639.4600907622915d));

  }

  public void test30() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test30"); }


    org.apache.commons.math3.stat.inference.MannWhitneyUTest var0 = new org.apache.commons.math3.stat.inference.MannWhitneyUTest();
    org.apache.commons.math3.stat.descriptive.rank.Max var1 = new org.apache.commons.math3.stat.descriptive.rank.Max();
    long var2 = var1.getN();
    org.apache.commons.math3.stat.descriptive.rank.Max var3 = var1.copy();
    org.apache.commons.math3.stat.descriptive.rank.Max var4 = var3.copy();
    org.apache.commons.math3.stat.descriptive.summary.SumOfSquares var5 = new org.apache.commons.math3.stat.descriptive.summary.SumOfSquares();
    double[] var9 = new double[] { 10.0d};
    double[] var11 = new double[] { 0.0d};
    org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator var12 = new org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator(1.0d, (-0.09966865249116202d), var9, var11);
    var5.incrementAll(var9);
    double[] var16 = new double[] { 10.0d, 1.0d};
    org.apache.commons.math3.optim.PointVectorValuePair var17 = new org.apache.commons.math3.optim.PointVectorValuePair(var9, var16);
    double var18 = var4.evaluate(var16);
    double[] var22 = new double[] { 10.0d};
    double[] var24 = new double[] { 0.0d};
    org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator var25 = new org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator(1.0d, (-0.09966865249116202d), var22, var24);
    org.apache.commons.math3.stat.descriptive.summary.SumOfSquares var28 = new org.apache.commons.math3.stat.descriptive.summary.SumOfSquares();
    double[] var32 = new double[] { 10.0d};
    double[] var34 = new double[] { 0.0d};
    org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator var35 = new org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator(1.0d, (-0.09966865249116202d), var32, var34);
    var28.incrementAll(var32);
    double[] var39 = new double[] { 10.0d, 1.0d};
    org.apache.commons.math3.optim.PointVectorValuePair var40 = new org.apache.commons.math3.optim.PointVectorValuePair(var32, var39);
    org.apache.commons.math3.linear.DiagonalMatrix var42 = new org.apache.commons.math3.linear.DiagonalMatrix(var39, false);
    org.apache.commons.math3.stat.descriptive.summary.SumOfSquares var43 = new org.apache.commons.math3.stat.descriptive.summary.SumOfSquares();
    double[] var47 = new double[] { 10.0d};
    double[] var49 = new double[] { 0.0d};
    org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator var50 = new org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator(1.0d, (-0.09966865249116202d), var47, var49);
    var43.incrementAll(var47);
    double[] var55 = new double[] { 10.0d};
    double[] var57 = new double[] { 0.0d};
    org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator var58 = new org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator(1.0d, (-0.09966865249116202d), var55, var57);
    double var59 = var43.evaluate(var55);
    var25.setStepSizeControl((-1.0d), 0.009933840289404017d, var39, var55);
    double var61 = var0.mannWhitneyU(var16, var39);
    org.apache.commons.math3.linear.RealMatrix var62 = org.apache.commons.math3.linear.MatrixUtils.createRealDiagonalMatrix(var16);
    org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex var63 = new org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex(var16);
    org.apache.commons.math3.optimization.PointValuePair[] var64 = var63.getPoints();

  }

  public void test31() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test31"); }


    org.apache.commons.math3.fraction.Fraction var1 = new org.apache.commons.math3.fraction.Fraction(0);
    org.apache.commons.math3.linear.DefaultFieldMatrixPreservingVisitor var2 = new org.apache.commons.math3.linear.DefaultFieldMatrixPreservingVisitor((org.apache.commons.math3.FieldElement)var1);
    org.apache.commons.math3.fraction.FractionField var3 = var1.getField();
    org.apache.commons.math3.linear.SparseFieldVector var6 = new org.apache.commons.math3.linear.SparseFieldVector((org.apache.commons.math3.Field)var3, 10, 11);
    org.apache.commons.math3.fraction.Fraction var8 = new org.apache.commons.math3.fraction.Fraction(0);
    org.apache.commons.math3.linear.DefaultFieldMatrixPreservingVisitor var9 = new org.apache.commons.math3.linear.DefaultFieldMatrixPreservingVisitor((org.apache.commons.math3.FieldElement)var8);
    org.apache.commons.math3.fraction.FractionField var10 = var8.getField();
    org.apache.commons.math3.linear.SparseFieldVector var13 = new org.apache.commons.math3.linear.SparseFieldVector((org.apache.commons.math3.Field)var10, 10, 11);
    org.apache.commons.math3.fraction.Fraction var15 = new org.apache.commons.math3.fraction.Fraction(0);
    org.apache.commons.math3.linear.DefaultFieldMatrixPreservingVisitor var16 = new org.apache.commons.math3.linear.DefaultFieldMatrixPreservingVisitor((org.apache.commons.math3.FieldElement)var15);
    org.apache.commons.math3.fraction.FractionField var17 = var15.getField();
    org.apache.commons.math3.linear.SparseFieldVector var20 = new org.apache.commons.math3.linear.SparseFieldVector((org.apache.commons.math3.Field)var17, 10, 11);
    org.apache.commons.math3.linear.FieldVector var21 = var13.append(var20);
    org.apache.commons.math3.fraction.Fraction var23 = new org.apache.commons.math3.fraction.Fraction(0);
    org.apache.commons.math3.linear.DefaultFieldMatrixPreservingVisitor var24 = new org.apache.commons.math3.linear.DefaultFieldMatrixPreservingVisitor((org.apache.commons.math3.FieldElement)var23);
    org.apache.commons.math3.fraction.FractionField var25 = var23.getField();
    org.apache.commons.math3.linear.SparseFieldVector var28 = new org.apache.commons.math3.linear.SparseFieldVector((org.apache.commons.math3.Field)var25, 10, 11);
    org.apache.commons.math3.fraction.Fraction var30 = new org.apache.commons.math3.fraction.Fraction(0);
    org.apache.commons.math3.linear.DefaultFieldMatrixPreservingVisitor var31 = new org.apache.commons.math3.linear.DefaultFieldMatrixPreservingVisitor((org.apache.commons.math3.FieldElement)var30);
    org.apache.commons.math3.fraction.FractionField var32 = var30.getField();
    org.apache.commons.math3.linear.SparseFieldVector var35 = new org.apache.commons.math3.linear.SparseFieldVector((org.apache.commons.math3.Field)var32, 10, 11);
    org.apache.commons.math3.linear.FieldVector var36 = var28.append(var35);
    org.apache.commons.math3.linear.FieldVector var37 = var20.append((org.apache.commons.math3.linear.FieldVector)var35);
    org.apache.commons.math3.linear.SparseFieldVector var38 = var6.subtract(var35);
    org.apache.commons.math3.fraction.Fraction var40 = new org.apache.commons.math3.fraction.Fraction(0);
    org.apache.commons.math3.linear.DefaultFieldMatrixPreservingVisitor var41 = new org.apache.commons.math3.linear.DefaultFieldMatrixPreservingVisitor((org.apache.commons.math3.FieldElement)var40);
    org.apache.commons.math3.fraction.FractionField var42 = var40.getField();
    org.apache.commons.math3.linear.SparseFieldVector var45 = new org.apache.commons.math3.linear.SparseFieldVector((org.apache.commons.math3.Field)var42, 10, 11);
    org.apache.commons.math3.fraction.Fraction var47 = new org.apache.commons.math3.fraction.Fraction(0);
    org.apache.commons.math3.linear.DefaultFieldMatrixPreservingVisitor var48 = new org.apache.commons.math3.linear.DefaultFieldMatrixPreservingVisitor((org.apache.commons.math3.FieldElement)var47);
    org.apache.commons.math3.fraction.FractionField var49 = var47.getField();
    org.apache.commons.math3.linear.SparseFieldVector var52 = new org.apache.commons.math3.linear.SparseFieldVector((org.apache.commons.math3.Field)var49, 10, 11);
    org.apache.commons.math3.linear.FieldVector var53 = var45.append(var52);
    org.apache.commons.math3.linear.FieldVector var54 = var38.append(var52);
    org.apache.commons.math3.linear.ArrayFieldVector var55 = new org.apache.commons.math3.linear.ArrayFieldVector(var54);
    org.apache.commons.math3.linear.FieldVector var56 = var55.copy();
    
    // Checks the contract:  equals-hashcode on var56 and var21
    assertTrue("Contract failed: equals-hashcode on var56 and var21", var56.equals(var21) ? var56.hashCode() == var21.hashCode() : true);
    
    // Checks the contract:  equals-hashcode on var56 and var36
    assertTrue("Contract failed: equals-hashcode on var56 and var36", var56.equals(var36) ? var56.hashCode() == var36.hashCode() : true);
    
    // Checks the contract:  equals-hashcode on var56 and var37
    assertTrue("Contract failed: equals-hashcode on var56 and var37", var56.equals(var37) ? var56.hashCode() == var37.hashCode() : true);
    
    // Checks the contract:  equals-hashcode on var56 and var53
    assertTrue("Contract failed: equals-hashcode on var56 and var53", var56.equals(var53) ? var56.hashCode() == var53.hashCode() : true);
    
    // Checks the contract:  equals-hashcode on var56 and var54
    assertTrue("Contract failed: equals-hashcode on var56 and var54", var56.equals(var54) ? var56.hashCode() == var54.hashCode() : true);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var56 and var21.", var56.equals(var21) == var21.equals(var56));
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var56 and var36.", var56.equals(var36) == var36.equals(var56));
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var56 and var37.", var56.equals(var37) == var37.equals(var56));
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var56 and var53.", var56.equals(var53) == var53.equals(var56));
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var56 and var54.", var56.equals(var54) == var54.equals(var56));

  }

  public void test32() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test32"); }


    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
    var0.setSoftPreviousTime(Double.POSITIVE_INFINITY);
    double[] var3 = var0.getInterpolatedStateVariation();

  }

  public void test33() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test33"); }


    org.apache.commons.math3.geometry.partitioning.BSPTree var0 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
    org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet var1 = new org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet(var0);
    org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet var2 = new org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet(var0);
    org.apache.commons.math3.geometry.euclidean.threed.Vector3D var5 = new org.apache.commons.math3.geometry.euclidean.threed.Vector3D((-0.4636456564559829d), Double.POSITIVE_INFINITY);
    org.apache.commons.math3.geometry.euclidean.threed.Rotation var11 = new org.apache.commons.math3.geometry.euclidean.threed.Rotation(Double.NaN, Double.NaN, 1.0d, Double.NaN, true);
    double[][] var12 = var11.getMatrix();
    double var13 = var11.getQ1();
    org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet var14 = var2.rotate(var5, var11);
    org.apache.commons.math3.geometry.partitioning.AbstractRegion var15 = var2.copySelf();
    org.apache.commons.math3.geometry.partitioning.AbstractRegion var16 = var2.copySelf();
    org.apache.commons.math3.geometry.euclidean.threed.Vector3D var19 = new org.apache.commons.math3.geometry.euclidean.threed.Vector3D((-0.4636456564559829d), Double.POSITIVE_INFINITY);
    org.apache.commons.math3.geometry.euclidean.threed.Vector3D var20 = var19.orthogonal();
    org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet var21 = var2.translate(var20);
    org.apache.commons.math3.geometry.euclidean.threed.Rotation var27 = new org.apache.commons.math3.geometry.euclidean.threed.Rotation(Double.NaN, Double.NaN, 1.0d, Double.NaN, true);
    double[][] var28 = var27.getMatrix();
    org.apache.commons.math3.geometry.euclidean.threed.Vector3D var29 = var27.getAxis();
    double var30 = var29.getAlpha();
    org.apache.commons.math3.geometry.euclidean.threed.Vector3D var33 = new org.apache.commons.math3.geometry.euclidean.threed.Vector3D((-0.4636456564559829d), Double.POSITIVE_INFINITY);
    double var34 = org.apache.commons.math3.geometry.euclidean.threed.Vector3D.distance(var29, var33);
    org.apache.commons.math3.geometry.euclidean.threed.SubLine var35 = new org.apache.commons.math3.geometry.euclidean.threed.SubLine(var20, var29);
    org.apache.commons.math3.geometry.partitioning.BSPTree var36 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
    org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet var37 = new org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet(var36);
    org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet var38 = new org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet(var36);
    org.apache.commons.math3.geometry.euclidean.threed.Vector3D var41 = new org.apache.commons.math3.geometry.euclidean.threed.Vector3D((-0.4636456564559829d), Double.POSITIVE_INFINITY);
    org.apache.commons.math3.geometry.euclidean.threed.Rotation var47 = new org.apache.commons.math3.geometry.euclidean.threed.Rotation(Double.NaN, Double.NaN, 1.0d, Double.NaN, true);
    double[][] var48 = var47.getMatrix();
    double var49 = var47.getQ1();
    org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet var50 = var38.rotate(var41, var47);
    org.apache.commons.math3.geometry.partitioning.AbstractRegion var51 = var38.copySelf();
    org.apache.commons.math3.geometry.partitioning.AbstractRegion var52 = var38.copySelf();
    org.apache.commons.math3.geometry.euclidean.threed.Vector3D var55 = new org.apache.commons.math3.geometry.euclidean.threed.Vector3D((-0.4636456564559829d), Double.POSITIVE_INFINITY);
    org.apache.commons.math3.geometry.euclidean.threed.Vector3D var56 = var55.orthogonal();
    org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet var57 = var38.translate(var56);
    org.apache.commons.math3.geometry.euclidean.threed.Rotation var63 = new org.apache.commons.math3.geometry.euclidean.threed.Rotation(Double.NaN, Double.NaN, 1.0d, Double.NaN, true);
    double[][] var64 = var63.getMatrix();
    org.apache.commons.math3.geometry.euclidean.threed.Vector3D var65 = var63.getAxis();
    double var66 = var65.getAlpha();
    org.apache.commons.math3.geometry.euclidean.threed.Vector3D var69 = new org.apache.commons.math3.geometry.euclidean.threed.Vector3D((-0.4636456564559829d), Double.POSITIVE_INFINITY);
    double var70 = org.apache.commons.math3.geometry.euclidean.threed.Vector3D.distance(var65, var69);
    org.apache.commons.math3.geometry.euclidean.threed.SubLine var71 = new org.apache.commons.math3.geometry.euclidean.threed.SubLine(var56, var65);
    org.apache.commons.math3.geometry.euclidean.threed.Vector3D var73 = var35.intersection(var71, true);

  }

  public void test34() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test34"); }


    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var0 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer();
    org.apache.commons.math3.linear.RealMatrix var1 = var0.getWeightSquareRoot();

  }

  public void test35() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test35"); }


    org.apache.commons.math3.optim.linear.SimplexSolver var3 = new org.apache.commons.math3.optim.linear.SimplexSolver(0.011031616446428008d, 1016070145, (-639.4600907622915d));
    org.apache.commons.math3.optim.PointValuePair var4 = var3.doOptimize();

  }

  public void test36() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test36"); }


    org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression();
    boolean var1 = var0.isNoIntercept();
    double[][] var2 = var0.estimateRegressionParametersVariance();

  }

  public void test37() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test37"); }


    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
    double[] var1 = var0.estimateRegressionParametersStandardErrors();

  }

  public void test38() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test38"); }


    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var5 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(0.15729920705028488d, (-1.0d), (-0.4999975593213537d), (-0.09966865249116202d), (-0.09966865249116202d));
    double var6 = var5.getChiSquare();
    double var7 = var5.getChiSquare();
    double var8 = var5.getRMS();
    org.apache.commons.math3.linear.RealMatrix var9 = var5.getWeight();

  }

  public void test39() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test39"); }


    org.apache.commons.math3.stat.correlation.PearsonsCorrelation var0 = new org.apache.commons.math3.stat.correlation.PearsonsCorrelation();
    org.apache.commons.math3.linear.RealMatrix var1 = var0.getCorrelationStandardErrors();

  }

  public void test40() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test40"); }


    org.apache.commons.math3.optim.SimpleVectorValueChecker var3 = new org.apache.commons.math3.optim.SimpleVectorValueChecker((-0.4636456564559829d), (-0.4636456564559829d), 6);
    double var4 = var3.getRelativeThreshold();
    org.apache.commons.math3.optim.nonlinear.vector.jacobian.GaussNewtonOptimizer var5 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.GaussNewtonOptimizer((org.apache.commons.math3.optim.ConvergenceChecker)var3);
    org.apache.commons.math3.optim.PointVectorValuePair var6 = var5.doOptimize();

  }

  public void test41() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test41"); }


    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
    var0.setNoIntercept(false);
    double var3 = var0.calculateAdjustedRSquared();

  }

  public void test42() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test42"); }


    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
    var0.setNoIntercept(false);
    org.apache.commons.math3.linear.RealMatrix var3 = var0.calculateHat();

  }

  public void test43() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test43"); }


    org.apache.commons.math3.stat.descriptive.moment.StandardDeviation var0 = new org.apache.commons.math3.stat.descriptive.moment.StandardDeviation();
    org.apache.commons.math3.stat.descriptive.summary.SumOfSquares var1 = new org.apache.commons.math3.stat.descriptive.summary.SumOfSquares();
    double[] var5 = new double[] { 10.0d};
    double[] var7 = new double[] { 0.0d};
    org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator var8 = new org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator(1.0d, (-0.09966865249116202d), var5, var7);
    var1.incrementAll(var5);
    double[] var13 = new double[] { 10.0d};
    double[] var15 = new double[] { 0.0d};
    org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator var16 = new org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator(1.0d, (-0.09966865249116202d), var13, var15);
    org.apache.commons.math3.optimization.direct.CMAESOptimizer.Sigma var17 = new org.apache.commons.math3.optimization.direct.CMAESOptimizer.Sigma(var15);
    org.apache.commons.math3.analysis.function.StepFunction var18 = new org.apache.commons.math3.analysis.function.StepFunction(var5, var15);
    java.lang.Double[] var20 = new java.lang.Double[] { (-0.09966865249116202d)};
    org.apache.commons.math3.linear.OpenMapRealVector var21 = new org.apache.commons.math3.linear.OpenMapRealVector(var20);
    org.apache.commons.math3.linear.OpenMapRealVector var22 = new org.apache.commons.math3.linear.OpenMapRealVector((org.apache.commons.math3.linear.RealVector)var21);
    java.lang.Double[] var24 = new java.lang.Double[] { (-0.09966865249116202d)};
    org.apache.commons.math3.linear.OpenMapRealVector var25 = new org.apache.commons.math3.linear.OpenMapRealVector(var24);
    org.apache.commons.math3.linear.RealVector var26 = var22.projection((org.apache.commons.math3.linear.RealVector)var25);
    org.apache.commons.math3.analysis.function.Exp var27 = new org.apache.commons.math3.analysis.function.Exp();
    org.apache.commons.math3.linear.RealVector var28 = var26.map((org.apache.commons.math3.analysis.UnivariateFunction)var27);
    org.apache.commons.math3.linear.ArrayRealVector var29 = new org.apache.commons.math3.linear.ArrayRealVector(var26);
    org.apache.commons.math3.linear.RealVector var31 = var29.append(10.0d);
    boolean var33 = var29.equals((java.lang.Object)0.0d);
    double[] var34 = var29.toArray();
    org.apache.commons.math3.optim.PointVectorValuePair var36 = new org.apache.commons.math3.optim.PointVectorValuePair(var5, var34, true);
    double var40 = var0.evaluate(var34, (-0.4999975593213537d), 1, 0);
    org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex var43 = new org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex(var34, (-639.4600907622915d), 13.827624257174595d);
    int var44 = var43.getSize();

  }

  public void test44() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test44"); }


    org.apache.commons.math3.optimization.direct.NelderMeadSimplex var1 = new org.apache.commons.math3.optimization.direct.NelderMeadSimplex(3);
    org.apache.commons.math3.optimization.PointValuePair var3 = var1.getPoint(1);

  }

  public void test45() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test45"); }


    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
    double var1 = var0.calculateRSquared();

  }

  public void test46() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test46"); }


    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
    var0.setSoftPreviousTime(Double.POSITIVE_INFINITY);
    var0.setSoftPreviousTime(7.38905609893065d);
    var0.rescale(47.53224177962315d);

  }

  public void test47() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test47"); }


    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
    var0.setNoIntercept(false);
    double var3 = var0.calculateResidualSumOfSquares();

  }

  public void test48() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test48"); }


    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
    double var1 = var0.getInterpolatedTime();
    double var2 = var0.getGlobalPreviousTime();
    double[] var3 = var0.getInterpolatedState();

  }

  public void test49() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test49"); }


    org.apache.commons.math3.analysis.integration.gauss.GaussIntegratorFactory var0 = new org.apache.commons.math3.analysis.integration.gauss.GaussIntegratorFactory();
    org.apache.commons.math3.analysis.integration.gauss.GaussIntegrator var4 = var0.legendreHighPrecision(111330504, (-0.0639872699254868d), 4.493971116214846d);

  }

  public void test50() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test50"); }


    org.apache.commons.math3.optim.nonlinear.scalar.noderiv.MultiDirectionalSimplex var2 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.MultiDirectionalSimplex(10, 1.0E8d);
    org.apache.commons.math3.optim.PointValuePair[] var3 = var2.getPoints();

  }

  public void test51() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test51"); }


    org.apache.commons.math3.geometry.partitioning.RegionFactory var0 = new org.apache.commons.math3.geometry.partitioning.RegionFactory();
    org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet var1 = new org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet();
    org.apache.commons.math3.geometry.partitioning.Region var2 = var0.getComplement((org.apache.commons.math3.geometry.partitioning.Region)var1);
    org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet var5 = new org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet(9.999945104464418d, (-0.09501214013410174d));
    boolean var6 = var5.isEmpty();
    org.apache.commons.math3.geometry.partitioning.Region var7 = var0.getComplement((org.apache.commons.math3.geometry.partitioning.Region)var5);
    org.apache.commons.math3.geometry.euclidean.twod.Vector2D var10 = new org.apache.commons.math3.geometry.euclidean.twod.Vector2D(0.9979562990357942d, (-31.47273727713465d));
    org.apache.commons.math3.geometry.euclidean.twod.Line var12 = new org.apache.commons.math3.geometry.euclidean.twod.Line(var10, 10000.0d);
    org.apache.commons.math3.geometry.euclidean.oned.Vector1D var14 = new org.apache.commons.math3.geometry.euclidean.oned.Vector1D(100.0d);
    org.apache.commons.math3.geometry.euclidean.oned.Vector1D var16 = new org.apache.commons.math3.geometry.euclidean.oned.Vector1D(100.0d);
    double var17 = var14.dotProduct((org.apache.commons.math3.geometry.Vector)var16);
    org.apache.commons.math3.geometry.euclidean.twod.Vector2D var19 = var12.getPointAt(var14, (-0.09983374988500592d));
    org.apache.commons.math3.geometry.euclidean.twod.Vector2D var22 = new org.apache.commons.math3.geometry.euclidean.twod.Vector2D(0.9979562990357942d, (-31.47273727713465d));
    org.apache.commons.math3.fraction.FractionFormat var23 = new org.apache.commons.math3.fraction.FractionFormat();
    int var24 = var23.getMinimumIntegerDigits();
    org.apache.commons.math3.fraction.ProperBigFractionFormat var25 = new org.apache.commons.math3.fraction.ProperBigFractionFormat((java.text.NumberFormat)var23);
    java.lang.String var26 = var22.toString((java.text.NumberFormat)var23);
    org.apache.commons.math3.geometry.euclidean.twod.Vector2D var29 = new org.apache.commons.math3.geometry.euclidean.twod.Vector2D(0.9979562990357942d, (-31.47273727713465d));
    org.apache.commons.math3.fraction.FractionFormat var30 = new org.apache.commons.math3.fraction.FractionFormat();
    int var31 = var30.getMinimumIntegerDigits();
    org.apache.commons.math3.fraction.ProperBigFractionFormat var32 = new org.apache.commons.math3.fraction.ProperBigFractionFormat((java.text.NumberFormat)var30);
    java.lang.String var33 = var29.toString((java.text.NumberFormat)var30);
    var12.reset(var22, var29);
    org.apache.commons.math3.geometry.euclidean.oned.Vector1D var36 = new org.apache.commons.math3.geometry.euclidean.oned.Vector1D(10.0d);
    org.apache.commons.math3.geometry.euclidean.oned.Vector1D var37 = var36.negate();
    org.apache.commons.math3.geometry.euclidean.oned.Vector1D var38 = var36.getZero();
    org.apache.commons.math3.geometry.euclidean.twod.Vector2D var40 = var12.getPointAt(var36, 6.0d);
    org.apache.commons.math3.geometry.partitioning.BSPTree var41 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
    org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet var42 = new org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet(var41);
    org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet var43 = new org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet(var41);
    org.apache.commons.math3.geometry.euclidean.threed.Vector3D var46 = new org.apache.commons.math3.geometry.euclidean.threed.Vector3D((-0.4636456564559829d), Double.POSITIVE_INFINITY);
    org.apache.commons.math3.geometry.euclidean.threed.Rotation var52 = new org.apache.commons.math3.geometry.euclidean.threed.Rotation(Double.NaN, Double.NaN, 1.0d, Double.NaN, true);
    double[][] var53 = var52.getMatrix();
    double var54 = var52.getQ1();
    org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet var55 = var43.rotate(var46, var52);
    org.apache.commons.math3.geometry.partitioning.AbstractRegion var56 = var43.copySelf();
    org.apache.commons.math3.geometry.partitioning.AbstractRegion var57 = var43.copySelf();
    org.apache.commons.math3.geometry.euclidean.oned.SubOrientedPoint var58 = new org.apache.commons.math3.geometry.euclidean.oned.SubOrientedPoint((org.apache.commons.math3.geometry.partitioning.Hyperplane)var12, (org.apache.commons.math3.geometry.partitioning.Region)var43);
    org.apache.commons.math3.geometry.partitioning.RegionFactory var59 = new org.apache.commons.math3.geometry.partitioning.RegionFactory();
    org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet var60 = new org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet();
    org.apache.commons.math3.geometry.partitioning.Region var61 = var59.getComplement((org.apache.commons.math3.geometry.partitioning.Region)var60);
    org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet var64 = new org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet(9.999945104464418d, (-0.09501214013410174d));
    boolean var65 = var64.isEmpty();
    org.apache.commons.math3.geometry.partitioning.Region var66 = var59.getComplement((org.apache.commons.math3.geometry.partitioning.Region)var64);
    org.apache.commons.math3.geometry.partitioning.Region var67 = var0.xor((org.apache.commons.math3.geometry.partitioning.Region)var43, var66);

  }

  public void test52() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test52"); }


    org.apache.commons.math3.analysis.solvers.LaguerreSolver var2 = new org.apache.commons.math3.analysis.solvers.LaguerreSolver(50.0d, 0.9972124504480494d);
    double var7 = var2.laguerre(9.841840200904633E-10d, 1.034696191075925E-9d, 5.551115123125784E-17d, 0.019867680578808035d);

  }

  public void test53() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test53"); }


    int[] var1 = new int[] { 10};
    org.apache.commons.math3.ml.clustering.DoublePoint var2 = new org.apache.commons.math3.ml.clustering.DoublePoint(var1);
    int[] var4 = new int[] { 10};
    org.apache.commons.math3.ml.clustering.DoublePoint var5 = new org.apache.commons.math3.ml.clustering.DoublePoint(var4);
    double var6 = org.apache.commons.math3.util.MathArrays.distance(var1, var4);
    org.apache.commons.math3.random.Well19937c var7 = new org.apache.commons.math3.random.Well19937c(var4);
    org.apache.commons.math3.distribution.LogNormalDistribution var11 = new org.apache.commons.math3.distribution.LogNormalDistribution((org.apache.commons.math3.random.RandomGenerator)var7, 0.011031616446428008d, 0.09966865249116202d, 1.0d);
    double var12 = var7.nextDouble();
    double var13 = var7.nextGaussian();
    org.apache.commons.math3.random.EmpiricalDistribution var14 = new org.apache.commons.math3.random.EmpiricalDistribution((org.apache.commons.math3.random.RandomGenerator)var7);
    double var15 = var14.getSupportUpperBound();
    double var16 = var14.getNumericalVariance();

  }

  public void test54() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test54"); }


    org.apache.commons.math3.geometry.partitioning.BSPTree var0 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
    org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet var1 = new org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet(var0);
    org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet var2 = new org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet(var0);
    org.apache.commons.math3.geometry.euclidean.threed.Vector3D var5 = new org.apache.commons.math3.geometry.euclidean.threed.Vector3D((-0.4636456564559829d), Double.POSITIVE_INFINITY);
    org.apache.commons.math3.geometry.euclidean.threed.Rotation var11 = new org.apache.commons.math3.geometry.euclidean.threed.Rotation(Double.NaN, Double.NaN, 1.0d, Double.NaN, true);
    double[][] var12 = var11.getMatrix();
    double var13 = var11.getQ1();
    org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet var14 = var2.rotate(var5, var11);
    org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet var15 = new org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet();
    org.apache.commons.math3.geometry.partitioning.BSPTree var17 = var15.getTree(true);
    org.apache.commons.math3.geometry.partitioning.BSPTree var18 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
    org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet var19 = new org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet(var18);
    org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet var20 = new org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet(var18);
    java.lang.Object var21 = var18.getAttribute();
    org.apache.commons.math3.geometry.partitioning.BSPTree var22 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
    org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet var23 = new org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet(var22);
    var18.insertInTree(var22, false);
    org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet var26 = var15.buildNew(var22);
    boolean var27 = var2.contains((org.apache.commons.math3.geometry.partitioning.Region)var15);

  }

}
