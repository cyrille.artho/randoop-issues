import junit.framework.*;

public class Deprecated extends TestCase {
  public void test2() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test2"); }


    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var0 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer();
    org.apache.commons.math3.fitting.PolynomialFitter var1 = new org.apache.commons.math3.fitting.PolynomialFitter((org.apache.commons.math3.optim.nonlinear.vector.MultivariateVectorOptimizer)var0);
    org.apache.commons.math3.linear.RealMatrix var2 = var0.getWeight();

  }

  public void test4() throws Throwable {
    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var5 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(0.15729920705028488d, (-1.0d), (-0.4999975593213537d), (-0.09966865249116202d), (-0.09966865249116202d));
    double var6 = var5.getChiSquare();
    double[][] var8 = var5.getCovariances(110.3206397147574d);
  }

  public void test5() throws Throwable {
    double[] var3 = new double[] { 10.0d};
    double[] var5 = new double[] { 0.0d};
    org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator var6 = new org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator(1.0d, (-0.09966865249116202d), var3, var5);
    org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex var9 = new org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex(var3, (-0.09966865249116202d), (-0.0639872699254868d));
    org.apache.commons.math3.optim.nonlinear.scalar.noderiv.MultiDirectionalSimplex var12 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.MultiDirectionalSimplex(var3, 0.0d, (-1.0d));
    org.apache.commons.math3.optim.PointValuePair var14 = var12.getPoint(10000);
  }

  public void test7() throws Throwable {
    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var5 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(0.15729920705028488d, (-1.0d), (-0.4999975593213537d), (-0.09966865249116202d), (-0.09966865249116202d));
    double var6 = var5.getChiSquare();
    double[][] var7 = var5.getCovariances();

  }

  public void test10() throws Throwable {
    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var5 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(0.15729920705028488d, (-1.0d), (-0.4999975593213537d), (-0.09966865249116202d), (-0.09966865249116202d));
    double var6 = var5.getChiSquare();
    org.apache.commons.math3.stat.descriptive.moment.StandardDeviation var7 = new org.apache.commons.math3.stat.descriptive.moment.StandardDeviation();
    org.apache.commons.math3.stat.descriptive.summary.SumOfSquares var8 = new org.apache.commons.math3.stat.descriptive.summary.SumOfSquares();
    double[] var12 = new double[] { 10.0d};
    double[] var14 = new double[] { 0.0d};
    org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator var15 = new org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator(1.0d, (-0.09966865249116202d), var12, var14);
    var8.incrementAll(var12);
    double[] var20 = new double[] { 10.0d};
    double[] var22 = new double[] { 0.0d};
    org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator var23 = new org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator(1.0d, (-0.09966865249116202d), var20, var22);
    org.apache.commons.math3.optimization.direct.CMAESOptimizer.Sigma var24 = new org.apache.commons.math3.optimization.direct.CMAESOptimizer.Sigma(var22);
    org.apache.commons.math3.analysis.function.StepFunction var25 = new org.apache.commons.math3.analysis.function.StepFunction(var12, var22);
    java.lang.Double[] var27 = new java.lang.Double[] { (-0.09966865249116202d)};
    org.apache.commons.math3.linear.OpenMapRealVector var28 = new org.apache.commons.math3.linear.OpenMapRealVector(var27);
    org.apache.commons.math3.linear.OpenMapRealVector var29 = new org.apache.commons.math3.linear.OpenMapRealVector((org.apache.commons.math3.linear.RealVector)var28);
    java.lang.Double[] var31 = new java.lang.Double[] { (-0.09966865249116202d)};
    org.apache.commons.math3.linear.OpenMapRealVector var32 = new org.apache.commons.math3.linear.OpenMapRealVector(var31);
    org.apache.commons.math3.linear.RealVector var33 = var29.projection((org.apache.commons.math3.linear.RealVector)var32);
    org.apache.commons.math3.analysis.function.Exp var34 = new org.apache.commons.math3.analysis.function.Exp();
    org.apache.commons.math3.linear.RealVector var35 = var33.map((org.apache.commons.math3.analysis.UnivariateFunction)var34);
    org.apache.commons.math3.linear.ArrayRealVector var36 = new org.apache.commons.math3.linear.ArrayRealVector(var33);
    org.apache.commons.math3.linear.RealVector var38 = var36.append(10.0d);
    boolean var40 = var36.equals((java.lang.Object)0.0d);
    double[] var41 = var36.toArray();
    org.apache.commons.math3.optim.PointVectorValuePair var43 = new org.apache.commons.math3.optim.PointVectorValuePair(var12, var41, true);
    double var47 = var7.evaluate(var41, (-0.4999975593213537d), 1, 0);
    double[] var49 = var5.computeSigma(var41, 47.53224177962315d);

  }

  public void test14() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test14"); }


    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var0 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer();
    org.apache.commons.math3.fitting.GaussianFitter var1 = new org.apache.commons.math3.fitting.GaussianFitter((org.apache.commons.math3.optim.nonlinear.vector.MultivariateVectorOptimizer)var0);
    double[] var2 = var0.getTarget();

  }

  public void test15() throws Throwable {
    double[] var3 = new double[] { 10.0d};
    double[] var5 = new double[] { 0.0d};
    org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator var6 = new org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator(1.0d, (-0.09966865249116202d), var3, var5);
    org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex var9 = new org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex(var3, (-0.09966865249116202d), (-0.0639872699254868d));
    org.apache.commons.math3.optim.nonlinear.scalar.noderiv.MultiDirectionalSimplex var12 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.MultiDirectionalSimplex(var3, 0.0d, (-1.0d));
    int var13 = var12.getDimension();
    int var14 = var12.getSize();
  }

  public void test16() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test16"); }


    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var0 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer();
    org.apache.commons.math3.optim.ConvergenceChecker var1 = var0.getConvergenceChecker();
    org.apache.commons.math3.optim.nonlinear.scalar.GoalType[] var2 = org.apache.commons.math3.optim.nonlinear.scalar.GoalType.values();
    org.apache.commons.math3.optim.PointVectorValuePair var3 = var0.optimize((org.apache.commons.math3.optim.OptimizationData[])var2);

  }

  public void test17() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test17"); }


    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var0 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer();
    org.apache.commons.math3.fitting.PolynomialFitter var1 = new org.apache.commons.math3.fitting.PolynomialFitter((org.apache.commons.math3.optim.nonlinear.vector.MultivariateVectorOptimizer)var0);
    double var2 = var0.getRMS();

  }
}
