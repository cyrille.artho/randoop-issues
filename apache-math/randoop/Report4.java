import junit.framework.*;

import org.apache.commons.math3.RealFieldElement;

public class Report4 extends TestCase {
  public void test6() throws Throwable {
    org.apache.commons.math3.dfp.DfpField var1 = new org.apache.commons.math3.dfp.DfpField(40);
    org.apache.commons.math3.dfp.Dfp var3 = var1.newDfp(0.0d);
    org.apache.commons.math3.geometry.euclidean.threed.FieldRotation var25 = new org.apache.commons.math3.geometry.euclidean.threed.FieldRotation((RealFieldElement)var3, (RealFieldElement)var3, (RealFieldElement)var3, (RealFieldElement)var3, true);
    org.apache.commons.math3.RealFieldElement var43 = var25.getQ0();
    
    // Checks the contract:  var43.equals(var43)
    assertTrue("Contract failed: var43.equals(var43)", var43.equals(var43));

  }
}
