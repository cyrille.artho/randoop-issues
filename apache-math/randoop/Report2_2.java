import junit.framework.*;

public class Report2_2 extends TestCase {
  // longer test than 2, 2_1
  public void test16() throws Throwable {
    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var0 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer();
    org.apache.commons.math3.optim.ConvergenceChecker var1 = var0.getConvergenceChecker();
    org.apache.commons.math3.optim.nonlinear.scalar.GoalType[] var2 = org.apache.commons.math3.optim.nonlinear.scalar.GoalType.values();
    org.apache.commons.math3.optim.PointVectorValuePair var3 = var0.optimize((org.apache.commons.math3.optim.OptimizationData[])var2);
  }

  public void test17() throws Throwable {
    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var0 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer();
    org.apache.commons.math3.fitting.PolynomialFitter var1 = new org.apache.commons.math3.fitting.PolynomialFitter((org.apache.commons.math3.optim.nonlinear.vector.MultivariateVectorOptimizer)var0);
    double var2 = var0.getRMS();

  }
}
