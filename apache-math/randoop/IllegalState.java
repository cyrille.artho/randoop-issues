import junit.framework.*;

// A collection of similar cases that result in a NullPointerException
// because some data has not been init'd yet.
// Such cases should probably be documented in the constructor.
// An example of such documentation:
// http://commons.apache.org/proper/commons-math/javadocs/api-3.2/org/apache/commons/math3/ode/sampling/NordsieckStepInterpolator.html#NordsieckStepInterpolator%28%29
// Some of these constructors should perhaps be deprecated or made
// non-public instead, though
public class IllegalState extends TestCase {
  public void test12() throws Throwable {
    new org.apache.commons.math3.random.ValueServer().resetReplayFile();
  }

  public void test18() throws Throwable {
    new org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression(). estimateErrorVariance();
  }

  public void test20() throws Throwable {
    new org.apache.commons.math3.random.EmpiricalDistribution().getGeneratorUpperBounds();
  }
}
