import junit.framework.*;

import org.apache.commons.math3.RealFieldElement;

public class FalsePositives extends TestCase {

  // NaN != NaN is OK
  public void test6() throws Throwable {
    org.apache.commons.math3.dfp.DfpField var1 = new org.apache.commons.math3.dfp.DfpField(40);
    org.apache.commons.math3.dfp.Dfp var3 = var1.newDfp(0.0d);
    org.apache.commons.math3.geometry.euclidean.threed.FieldRotation var25 = new org.apache.commons.math3.geometry.euclidean.threed.FieldRotation((RealFieldElement)var3, (RealFieldElement)var3, (RealFieldElement)var3, (RealFieldElement)var3, true);
    org.apache.commons.math3.RealFieldElement var43 = var25.getQ0();
    
    // Checks the contract:  var43.equals(var43)
    assertTrue("Contract failed: var43.equals(var43)", var43.equals(var43));
  }

  // parameter seems to be too large
  public void test11() throws Throwable {
    org.apache.commons.math3.analysis.integration.gauss.GaussIntegratorFactory var0 = new org.apache.commons.math3.analysis.integration.gauss.GaussIntegratorFactory();
    org.apache.commons.math3.analysis.integration.gauss.GaussIntegrator var2 = var0.legendre(2147483647);
  }

  // This constructor builds an instance that is not usable
  // yet, the AbstractStepInterpolator.reinitialize(double[],
  // boolean, org.apache.commons.math3.ode.EquationsMapper,
  // org.apache.commons.math3.ode.EquationsMapper[]) method should be called
  // before using the instance ...
  public void test19() throws Throwable {
    org.apache.commons.math3.ode.ContinuousOutputModel var0 = new org.apache.commons.math3.ode.ContinuousOutputModel();
    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var1 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
    var0.handleStep((org.apache.commons.math3.ode.sampling.StepInterpolator)var1, false);
    org.apache.commons.math3.ode.ContinuousOutputModel var4 = new org.apache.commons.math3.ode.ContinuousOutputModel();
    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var5 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
    var4.handleStep((org.apache.commons.math3.ode.sampling.StepInterpolator)var5, false);
    var0.append(var4);
  }
}
