import junit.framework.*;

public class IllegalStateA extends TestCase {
  public void test1() throws Throwable {
    new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression().calculateHat();
  }

  public void test2() throws Throwable {
    new org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression().estimateRegressionParametersVariance();
  }

  public void test12() throws Throwable {
    new org.apache.commons.math3.stat.correlation.PearsonsCorrelation().getCorrelationStandardErrors();
  }
}
