import junit.framework.*;

public class Report2a extends TestCase {

  public void test3() throws Throwable {
    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(1.0d, 1.0d, 1.0d, 1.0d, 1.0d);
    double[] var2 = new double[] { 1.0d, 1.0d};
    double[][] var3 = var1.computeCovariances(var2, 1.0d);
  }

  public void test7() throws Throwable {
    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var0 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer();
    org.apache.commons.math3.linear.RealMatrix var1 = var0.getWeightSquareRoot();
  }

  public void test8() throws Throwable {
    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(100.0d, 100.0d, 100.0d);
    org.apache.commons.math3.linear.RealMatrix var2 = var1.getWeight();
  }

  public void test13() throws Throwable {
    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(23.075991912170917d, 23.075991912170917d, 23.075991912170917d, 23.075991912170917d, 23.075991912170917d);
    org.apache.commons.math3.linear.RealMatrix var2 = var1.getWeightSquareRoot();
  }

  public void test14() throws Throwable {
    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(0.0d, 0.0d, 0.0d);
    double[][] var2 = var1.getCovariances(0.0d);

  }

  public void test15() throws Throwable {
    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(0.0d, 0.0d, 0.0d);
    double[][] var2 = var1.getCovariances();

  }

  public void test16() throws Throwable {
    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(23.075991912170917d, 23.075991912170917d, 23.075991912170917d, 23.075991912170917d, 23.075991912170917d);
    double[] var2 = new double[] { 23.075991912170917d, 23.075991912170917d};
    double[][] var3 = var1.computeCovariances(var2, 23.075991912170917d);

  }

  public void test18() throws Throwable {
    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var0 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer();
    org.apache.commons.math3.optim.univariate.SearchInterval var3 = new org.apache.commons.math3.optim.univariate.SearchInterval(0.5773502691895d, 1.0d);
    org.apache.commons.math3.optim.OptimizationData[] var4 = new org.apache.commons.math3.optim.OptimizationData[] { var3};
    org.apache.commons.math3.optim.PointVectorValuePair var5 = var0.optimize(var4);

  }
}
