import junit.framework.*;

public class Ignore extends TestCase {

  public void test9() throws Throwable {
    org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics var0 = new org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics();
    org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics var1 = var0.copy();
    org.apache.commons.math3.stat.descriptive.SummaryStatistics var2 = new org.apache.commons.math3.stat.descriptive.SummaryStatistics();
    org.apache.commons.math3.stat.descriptive.AggregateSummaryStatistics var3 = new org.apache.commons.math3.stat.descriptive.AggregateSummaryStatistics((org.apache.commons.math3.stat.descriptive.SummaryStatistics)var1, var2);
    org.apache.commons.math3.stat.descriptive.SummaryStatistics var4 = var3.createContributingStatistics();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
  }
}
