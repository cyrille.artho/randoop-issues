import junit.framework.*;

public class Report5a extends TestCase {
  public void test4() throws Throwable {
    org.apache.commons.math3.dfp.DfpField var1 = new org.apache.commons.math3.dfp.DfpField(1);
    org.apache.commons.math3.dfp.Dfp var6 = var1.newDfp(-0.0d);
    org.apache.commons.math3.dfp.Dfp var5 = var1.newDfp(0L);
    
    // Checks the contract:  equals-hashcode on var5 and var6
    assertTrue("Contract failed: equals-hashcode on var5 and var6", var5.equals(var6) ? var5.hashCode() == var6.hashCode() : true);
  }
}
