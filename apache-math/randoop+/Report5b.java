import junit.framework.*;

public class Report5b extends TestCase {
  public void test5() throws Throwable {
    org.apache.commons.math3.complex.Complex var1 = org.apache.commons.math3.complex.Complex.valueOf((-1.0d));
    org.apache.commons.math3.complex.ComplexField var2 = org.apache.commons.math3.complex.ComplexField.getInstance();
    org.apache.commons.math3.complex.Complex var3 = var2.getOne();
    org.apache.commons.math3.complex.Complex var4 = var1.negate();
    
    // Checks the contract:  equals-hashcode on var3 and var4
    assertTrue("Contract failed: equals-hashcode on var3 and var4", var3.equals(var4) ? var3.hashCode() == var4.hashCode() : true);
  }
}
