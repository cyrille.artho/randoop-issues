import junit.framework.*;

public class Deprecated extends TestCase {
    
    public void test3() throws Throwable {
        org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(1.0d, 1.0d, 1.0d, 1.0d, 1.0d);
        double[] var2 = new double[] { 1.0d, 1.0d};
        double[][] var3 = var1.computeCovariances(var2, 1.0d);

      }

      public void test6() throws Throwable {
        new org.apache.commons.math3.optimization.general.GaussNewtonOptimizer().getWeight();
      }

      public void test7() throws Throwable {

        if (debug) { System.out.println(); System.out.print("RandoopTest0.test7"); }


        org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var0 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer();
        org.apache.commons.math3.linear.RealMatrix var1 = var0.getWeightSquareRoot();

      }

      public void test8() throws Throwable {

        if (debug) { System.out.println(); System.out.print("RandoopTest0.test8"); }


        org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(100.0d, 100.0d, 100.0d);
        org.apache.commons.math3.linear.RealMatrix var2 = var1.getWeight();

      }

      public void test13() throws Throwable {

        if (debug) { System.out.println(); System.out.print("RandoopTest0.test13"); }


        org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(23.075991912170917d, 23.075991912170917d, 23.075991912170917d, 23.075991912170917d, 23.075991912170917d);
        org.apache.commons.math3.linear.RealMatrix var2 = var1.getWeightSquareRoot();

      }

      public void test14() throws Throwable {

        if (debug) { System.out.println(); System.out.print("RandoopTest0.test14"); }


        org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(0.0d, 0.0d, 0.0d);
        double[][] var2 = var1.getCovariances(0.0d);

      }

      public void test15() throws Throwable {

        if (debug) { System.out.println(); System.out.print("RandoopTest0.test15"); }


        org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(0.0d, 0.0d, 0.0d);
        double[][] var2 = var1.getCovariances();

      }

      public void test16() throws Throwable {

        if (debug) { System.out.println(); System.out.print("RandoopTest0.test16"); }


        org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(23.075991912170917d, 23.075991912170917d, 23.075991912170917d, 23.075991912170917d, 23.075991912170917d);
        double[] var2 = new double[] { 23.075991912170917d, 23.075991912170917d};
        double[][] var3 = var1.computeCovariances(var2, 23.075991912170917d);

      }


      public void test17() throws Throwable {
        org.apache.commons.math3.optimization.direct.NelderMeadSimplex var1 = new org.apache.commons.math3.optimization.direct.NelderMeadSimplex(45);
        org.apache.commons.math3.optimization.PointValuePair var2 = var1.getPoint(45);
      }

  public void test19() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test19"); }


    org.apache.commons.math3.optim.nonlinear.scalar.noderiv.SimplexOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.SimplexOptimizer(1.000000000001d, 1.000000000001d);
    org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex var3 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex(10, 1.000000000001d, 1.000000000001d, 1.000000000001d, 1.000000000001d);
    org.apache.commons.math3.optim.OptimizationData[] var4 = new org.apache.commons.math3.optim.OptimizationData[] { var3};
    org.apache.commons.math3.optim.PointValuePair var5 = var1.optimize(var4);

  }

  public void test20() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test20"); }


    double[] var1 = new double[] { 100.0d, 100.0d};
    org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex var2 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex(var1);
    org.apache.commons.math3.optim.PointValuePair var4 = var2.getPoint(1);

  }

  public void test21() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test21"); }


    org.apache.commons.math3.optim.linear.SimplexSolver var2 = new org.apache.commons.math3.optim.linear.SimplexSolver((-34.55974473970935d), (-1), (-34.55974473970935d));
    double[] var3 = new double[] { };
    org.apache.commons.math3.optim.linear.LinearObjectiveFunction var4 = new org.apache.commons.math3.optim.linear.LinearObjectiveFunction(var3, (-34.55974473970935d));
    org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction var5 = new org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction((org.apache.commons.math3.analysis.MultivariateFunction)var4);
    org.apache.commons.math3.optim.OptimizationData[] var6 = new org.apache.commons.math3.optim.OptimizationData[] { var5};
    org.apache.commons.math3.optim.PointValuePair var7 = var2.optimize(var6);

  }

  public void test22() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test22"); }


    org.apache.commons.math3.optim.linear.SimplexSolver var1 = new org.apache.commons.math3.optim.linear.SimplexSolver(100.0d);
    double[] var2 = new double[] { };
    double[][] var3 = new double[][] { var2};
    org.apache.commons.math3.optim.nonlinear.scalar.noderiv.MultiDirectionalSimplex var4 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.MultiDirectionalSimplex(var3, 100.0d, 100.0d);
    org.apache.commons.math3.optim.OptimizationData[] var5 = new org.apache.commons.math3.optim.OptimizationData[] { var4};
    org.apache.commons.math3.optim.PointValuePair var6 = var1.optimize(var5);

  }
     

        public void test23() throws Throwable {

        if (debug) { System.out.println(); System.out.print("RandoopTest0.test23"); }


        org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex var2 = new org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex(10, 0.5773502691895d);
        org.apache.commons.math3.optimization.PointValuePair[] var3 = var2.getPoints();

      }

        public void test28() throws Throwable {

        if (debug) { System.out.println(); System.out.print("RandoopTest0.test28"); }


        org.apache.commons.math3.analysis.solvers.LaguerreSolver var0 = new org.apache.commons.math3.analysis.solvers.LaguerreSolver();
        double var2 = var0.laguerre(2.0d, 2.0d, 2.0d, 2.0d);

      }

        public void test32() throws Throwable {

        if (debug) { System.out.println(); System.out.print("RandoopTest0.test32"); }


        double[] var2 = new double[] { 10.0d, 10.0d, 10.0d};
        org.apache.commons.math3.optimization.direct.CMAESOptimizer var3 = new org.apache.commons.math3.optimization.direct.CMAESOptimizer(80, var2);
        java.util.List var4 = var3.getStatisticsMeanHistory();
        org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet var5 = new org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet((java.util.Collection)var4);
        org.apache.commons.math3.geometry.partitioning.BSPTree var6 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
        boolean var7 = var5.isEmpty(var6);

      }

        public void test34() throws Throwable {

        if (debug) { System.out.println(); System.out.print("RandoopTest0.test34"); }


        org.apache.commons.math3.optimization.general.GaussNewtonOptimizer var1 = new org.apache.commons.math3.optimization.general.GaussNewtonOptimizer(false);
        double[] var2 = var1.getTarget();

      }

        public void test48() throws Throwable {

        if (debug) { System.out.println(); System.out.print("RandoopTest0.test48"); }


        org.apache.commons.math3.optimization.general.GaussNewtonOptimizer var1 = new org.apache.commons.math3.optimization.general.GaussNewtonOptimizer(false);
        double[] var2 = var1.getStartPoint();

      }

        public void test55() throws Throwable {

        if (debug) { System.out.println(); System.out.print("RandoopTest0.test55"); }


        org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex var2 = new org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex(10, 0.6470916444326115d, 0.6470916444326115d, 0.6470916444326115d);
        int var3 = var2.getSize();

      }



      public void test71() throws Throwable {

        if (debug) { System.out.println(); System.out.print("RandoopTest0.test71"); }


        org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(0.45121005791836927d, 0.45121005791836927d, 0.45121005791836927d, 0.45121005791836927d, 0.45121005791836927d);
        double[] var2 = new double[] { 0.45121005791836927d, 0.45121005791836927d, 0.45121005791836927d};
        double[] var3 = var1.computeSigma(var2, 0.45121005791836927d);

      }

      public void test78() throws Throwable {

        if (debug) { System.out.println(); System.out.print("RandoopTest0.test78"); }


        org.apache.commons.math3.optimization.general.GaussNewtonOptimizer var1 = new org.apache.commons.math3.optimization.general.GaussNewtonOptimizer(false);
        org.apache.commons.math3.optimization.PointVectorValuePair var2 = var1.doOptimize();

      }


}
