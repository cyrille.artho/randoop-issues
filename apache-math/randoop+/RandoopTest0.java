package enhancement;


import junit.framework.*;

public class RandoopTest0 extends TestCase {

  public void test25() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test25"); }


    org.apache.commons.math3.random.EmpiricalDistribution var0 = new org.apache.commons.math3.random.EmpiricalDistribution();
    double[] var1 = var0.getGeneratorUpperBounds();

  }

  public void test26() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test26"); }


    org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics var0 = new org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics();
    org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics var1 = var0.copy();
    org.apache.commons.math3.stat.descriptive.SummaryStatistics var2 = new org.apache.commons.math3.stat.descriptive.SummaryStatistics();
    org.apache.commons.math3.stat.descriptive.AggregateSummaryStatistics var3 = new org.apache.commons.math3.stat.descriptive.AggregateSummaryStatistics((org.apache.commons.math3.stat.descriptive.SummaryStatistics)var1, var2);
    org.apache.commons.math3.stat.descriptive.SummaryStatistics var4 = var3.createContributingStatistics();
//    java.lang.Object var5 = component.model.IdentityMethod.IdentityForward((java.lang.Object)var3);
    double var6 = var3.getGeometricMean();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
//    assertNotNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == Double.NaN);

  }

  public void test27() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test27"); }


    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
    double[] var1 = var0.getInterpolatedDerivatives();

  }

  public void test28() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test28"); }


    org.apache.commons.math3.analysis.solvers.LaguerreSolver var0 = new org.apache.commons.math3.analysis.solvers.LaguerreSolver();
    double var2 = var0.laguerre(2.0d, 2.0d, 2.0d, 2.0d);

  }

  public void test29() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test29"); }


    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(100.0d, 100.0d, 100.0d);
    int var2 = var1.getTargetSize();

  }

  public void test30() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test30"); }


    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
    double[] var2 = var0.getInterpolatedSecondaryDerivatives(3);

  }

  public void test31() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test31"); }


    org.apache.commons.math3.random.EmpiricalDistribution var0 = new org.apache.commons.math3.random.EmpiricalDistribution();
    double var1 = var0.getNumericalMean();

  }

  public void test32() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test32"); }


    double[] var2 = new double[] { 10.0d, 10.0d, 10.0d};
    org.apache.commons.math3.optimization.direct.CMAESOptimizer var3 = new org.apache.commons.math3.optimization.direct.CMAESOptimizer(80, var2);
    java.util.List var4 = var3.getStatisticsMeanHistory();
    org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet var5 = new org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet((java.util.Collection)var4);
    org.apache.commons.math3.geometry.partitioning.BSPTree var6 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
    boolean var7 = var5.isEmpty(var6);

  }

  public void test33() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test33"); }


    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
    var0.rescale(2.5d);

  }

  public void test34() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test34"); }


    org.apache.commons.math3.optimization.general.GaussNewtonOptimizer var1 = new org.apache.commons.math3.optimization.general.GaussNewtonOptimizer(false);
    double[] var2 = var1.getTarget();

  }

  public void test35() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test35"); }


    org.apache.commons.math3.random.EmpiricalDistribution var0 = new org.apache.commons.math3.random.EmpiricalDistribution();
    double var1 = var0.getNumericalVariance();

  }

  public void test36() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test36"); }


    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
    double var1 = var0.calculateTotalSumOfSquares();

  }

  public void test37() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test37"); }


    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(100.0d, 100.0d, 100.0d);
    double var2 = var1.getRMS();

  }

  public void test38() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test38"); }


    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
    double[] var1 = var0.estimateRegressionParameters();

  }

  public void test39() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test39"); }


    org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression();
    double[] var1 = var0.estimateRegressionParametersStandardErrors();

  }

  public void test40() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test40"); }


    org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex var1 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex(100);
    org.apache.commons.math3.optim.PointValuePair[] var2 = var1.getPoints();

  }

  public void test41() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test41"); }


    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(1.0d, 1.0d, 1.0d, 1.0d, 1.0d);
    double[] var2 = new double[] { 1.0d};
    double[] var3 = var1.computeSigma(var2, 1.0d);

  }

  public void test42() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test42"); }


    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
    double var1 = var0.calculateAdjustedRSquared();

  }

  public void test43() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test43"); }


    double[] var1 = new double[] { 1.0d};
    org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex var2 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex(var1, 1.0d, 1.0d, 1.0d, 1.0d);
    int var3 = var2.getSize();

  }

  public void test44() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test44"); }


    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
    double[] var1 = var0.getInterpolatedStateVariation();

  }

  public void test45() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test45"); }


    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(100.0d, 100.0d, 100.0d);
    double[] var2 = var1.getTarget();

  }

  public void test46() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test46"); }


    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
    double var1 = var0.estimateRegressandVariance();

  }

  public void test47() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test47"); }


    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
    double var1 = var0.estimateRegressionStandardError();

  }

  public void test48() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test48"); }


    org.apache.commons.math3.optimization.general.GaussNewtonOptimizer var1 = new org.apache.commons.math3.optimization.general.GaussNewtonOptimizer(false);
    double[] var2 = var1.getStartPoint();

  }

  public void test49() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test49"); }


    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(100.0d, 100.0d, 100.0d);
    org.apache.commons.math3.random.UnitSphereRandomVectorGenerator var3 = new org.apache.commons.math3.random.UnitSphereRandomVectorGenerator(18);
    org.apache.commons.math3.optim.nonlinear.vector.MultiStartMultivariateVectorOptimizer var4 = new org.apache.commons.math3.optim.nonlinear.vector.MultiStartMultivariateVectorOptimizer((org.apache.commons.math3.optim.nonlinear.vector.MultivariateVectorOptimizer)var1, 18, (org.apache.commons.math3.random.RandomVectorGenerator)var3);
    org.apache.commons.math3.optim.PointVectorValuePair[] var5 = var4.getOptima();

  }

  public void test50() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test50"); }


    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
    double[] var1 = var0.estimateResiduals();

  }

  public void test51() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test51"); }


    org.apache.commons.math3.geometry.partitioning.BSPTree var0 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
    org.apache.commons.math3.geometry.euclidean.oned.Vector1D var2 = new org.apache.commons.math3.geometry.euclidean.oned.Vector1D(100.0d);
    org.apache.commons.math3.geometry.euclidean.oned.OrientedPoint var4 = new org.apache.commons.math3.geometry.euclidean.oned.OrientedPoint(var2, true);
    boolean var5 = var0.insertCut((org.apache.commons.math3.geometry.partitioning.Hyperplane)var4);

  }

  public void test52() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test52"); }


    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
    double var1 = var0.calculateResidualSumOfSquares();

  }

  public void test53() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test53"); }


    org.apache.commons.math3.random.ValueServer var0 = new org.apache.commons.math3.random.ValueServer();
    var0.resetReplayFile();

  }

  public void test54() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test54"); }


    org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression();
    double var1 = var0.estimateErrorVariance();

  }

  public void test55() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test55"); }


    org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex var2 = new org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex(10, 0.6470916444326115d, 0.6470916444326115d, 0.6470916444326115d);
    int var3 = var2.getSize();

  }

  public void test56() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test56"); }


    org.apache.commons.math3.geometry.partitioning.BSPTree var0 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
    org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet var1 = new org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet(var0);
    double var2 = var1.getSup();

  }

  public void test57() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test57"); }


    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
    double var1 = var0.calculateRSquared();

  }

  public void test58() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test58"); }


    org.apache.commons.math3.random.EmpiricalDistribution var1 = new org.apache.commons.math3.random.EmpiricalDistribution(100);
    double var3 = var1.inverseCumulativeProbability(0.4472135954999579d);

  }

  public void test59() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test59"); }


    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
    double[] var1 = var0.getInterpolatedState();

  }

  public void test60() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test60"); }


    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
    double[] var2 = var0.getInterpolatedSecondaryState(2147483647);

  }

  public void test61() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test61"); }


    org.apache.commons.math3.dfp.DfpField var1 = new org.apache.commons.math3.dfp.DfpField(1);
    org.apache.commons.math3.dfp.Dfp var2 = var1.getTwo();
    org.apache.commons.math3.dfp.DfpDec var3 = new org.apache.commons.math3.dfp.DfpDec(var2);
    org.apache.commons.math3.dfp.Dfp var5 = var3.newInstance((byte)0);
    org.apache.commons.math3.dfp.Dfp var6 = var3.newInstance((byte)0, (byte)0);
    org.apache.commons.math3.dfp.DfpField var7 = var3.getField();
    
    // Checks the contract:  equals-hashcode on var5 and var6
    assertTrue("Contract failed: equals-hashcode on var5 and var6", var5.equals(var6) ? var5.hashCode() == var6.hashCode() : true);
    
    // Checks the contract:  equals-hashcode on var6 and var5
    assertTrue("Contract failed: equals-hashcode on var6 and var5", var6.equals(var5) ? var6.hashCode() == var5.hashCode() : true);

  }

  public void test62() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test62"); }


    org.apache.commons.math3.optim.univariate.SimpleUnivariateValueChecker var3 = new org.apache.commons.math3.optim.univariate.SimpleUnivariateValueChecker((-961.206742385875d), (-961.206742385875d), 64);
    org.apache.commons.math3.optim.nonlinear.vector.jacobian.GaussNewtonOptimizer var4 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.GaussNewtonOptimizer(false, (org.apache.commons.math3.optim.ConvergenceChecker)var3);
    org.apache.commons.math3.optim.PointVectorValuePair var5 = var4.doOptimize();

  }

  public void test63() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test63"); }


    org.apache.commons.math3.optim.linear.SimplexSolver var2 = new org.apache.commons.math3.optim.linear.SimplexSolver(0.0d, 15);
    org.apache.commons.math3.optim.PointValuePair var3 = var2.doOptimize();

  }

  public void test65() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test65"); }


    org.apache.commons.math3.optim.univariate.BrentOptimizer var1 = new org.apache.commons.math3.optim.univariate.BrentOptimizer(1.000000000001d, 1.000000000001d);
    org.apache.commons.math3.optim.MaxEval var2 = org.apache.commons.math3.optim.MaxEval.unlimited();
    org.apache.commons.math3.optim.OptimizationData[] var3 = new org.apache.commons.math3.optim.OptimizationData[] { var2};
    org.apache.commons.math3.optim.univariate.UnivariatePointValuePair var4 = var1.optimize(var3);

  }

  public void test66() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test66"); }


    org.apache.commons.math3.random.MersenneTwister var4 = new org.apache.commons.math3.random.MersenneTwister(1L);
    org.apache.commons.math3.optim.SimpleVectorValueChecker var5 = new org.apache.commons.math3.optim.SimpleVectorValueChecker(2.302736646959952d, 2.302736646959952d, 50);
    org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer var6 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer(50, 2.302736646959952d, true, 50, 50, (org.apache.commons.math3.random.RandomGenerator)var4, true, (org.apache.commons.math3.optim.ConvergenceChecker)var5);
    org.apache.commons.math3.optim.nonlinear.scalar.noderiv.MultiDirectionalSimplex var7 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.MultiDirectionalSimplex(50, 2.302736646959952d);
    org.apache.commons.math3.optim.OptimizationData[] var8 = new org.apache.commons.math3.optim.OptimizationData[] { var7};
    org.apache.commons.math3.optim.PointValuePair var9 = var6.optimize(var8);

  }

  public void test67() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test67"); }


    java.lang.Double[] var1 = new java.lang.Double[] { 100.0d};
    org.apache.commons.math3.linear.ArrayRealVector var2 = new org.apache.commons.math3.linear.ArrayRealVector(var1);
//    java.lang.Object var3 = component.model.IdentityMethod.IdentityForward((java.lang.Object)var2);
    org.apache.commons.math3.linear.OpenMapRealVector var4 = new org.apache.commons.math3.linear.OpenMapRealVector(var1);
//    java.lang.Object var5 = component.model.IdentityMethod.IdentityForward((java.lang.Object)var4);
    org.apache.commons.math3.linear.RealVector var6 = var2.combine(100.0d, 100.0d, (org.apache.commons.math3.linear.RealVector)var4);
    
    // Checks the contract:  equals-hashcode on var3 and var5
    assertTrue("Contract failed: equals-hashcode on var3 and var5", var2.equals(var4) ? var2.hashCode() == var4.hashCode() : true);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var3 and var5.", var2.equals(var4) == var4.equals(var2));

  }

  public void test68() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test68"); }


    java.lang.Double[] var1 = new java.lang.Double[] { 100.0d};
    org.apache.commons.math3.linear.OpenMapRealVector var2 = new org.apache.commons.math3.linear.OpenMapRealVector(var1);
    org.apache.commons.math3.linear.RealVector var3 = org.apache.commons.math3.linear.RealVector.unmodifiableRealVector((org.apache.commons.math3.linear.RealVector)var2);
    
    // Checks the contract:  var3.equals(var3)
    assertTrue("Contract failed: var3.equals(var3)", var3.equals(var3));
    
    // Checks the contract:  !var3.equals(null)
    assertTrue("Contract failed: !var3.equals(null)", !var3.equals(null));

  }

  public void test69() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test69"); }


    org.apache.commons.math3.geometry.partitioning.BSPTree var0 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
    org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet var1 = new org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet(var0);
    org.apache.commons.math3.geometry.Vector var2 = var1.getBarycenter();

  }

  public void test70() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test70"); }


    double[] var1 = new double[] { 1.0d, 1.0d};
    double[][] var2 = new double[][] { var1};
    org.apache.commons.math3.distribution.fitting.MultivariateNormalMixtureExpectationMaximization var3 = new org.apache.commons.math3.distribution.fitting.MultivariateNormalMixtureExpectationMaximization(var2);
    org.apache.commons.math3.distribution.MixtureMultivariateNormalDistribution var4 = var3.getFittedModel();

  }

  public void test71() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test71"); }


    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(0.45121005791836927d, 0.45121005791836927d, 0.45121005791836927d, 0.45121005791836927d, 0.45121005791836927d);
    double[] var2 = new double[] { 0.45121005791836927d, 0.45121005791836927d, 0.45121005791836927d};
    double[] var3 = var1.computeSigma(var2, 0.45121005791836927d);

  }

  public void test72() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test72"); }


    org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet var0 = new org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet();
    org.apache.commons.math3.geometry.partitioning.BSPTree var1 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
    org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet var2 = var0.buildNew(var1);
    boolean var3 = var2.isEmpty();

  }

  public void test73() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test73"); }


    org.apache.commons.math3.ode.ContinuousOutputModel var0 = new org.apache.commons.math3.ode.ContinuousOutputModel();
    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var1 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
    var0.handleStep((org.apache.commons.math3.ode.sampling.StepInterpolator)var1, false);
    var0.append(var0);

  }

//  public void test74() throws Throwable {
//
//    if (debug) { System.out.println(); System.out.print("RandoopTest0.test74"); }
//
//
//    org.apache.commons.math3.fraction.BigFractionFormat var0 = org.apache.commons.math3.fraction.BigFractionFormat.getProperInstance();
//    
//    var0.setWholeFormat((java.text.NumberFormat)var0);
//    java.lang.String var3 = var0.format((-75L));
//  }

  public void test75() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test75"); }


    org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet var0 = new org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet();
    org.apache.commons.math3.geometry.partitioning.BSPTree var1 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
    org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet var2 = var0.buildNew(var1);
    boolean var3 = var2.contains((org.apache.commons.math3.geometry.partitioning.Region)var0);

  }

//  public void test76() throws Throwable {
//
//    if (debug) { System.out.println(); System.out.print("RandoopTest0.test76"); }
//
//
//    org.apache.commons.math3.fraction.BigFractionFormat var1 = org.apache.commons.math3.fraction.BigFractionFormat.getProperInstance();
//    var1.setWholeFormat((java.text.NumberFormat)var1);
//    org.apache.commons.math3.linear.RealMatrixFormat var3 = new org.apache.commons.math3.linear.RealMatrixFormat("-36 / 1", "-36 / 1", "-36 / 1", "-36 / 1", "-36 / 1", "-36 / 1", (java.text.NumberFormat)var1);
//    double[] var5 = new double[] { (-1.0d), (-1.0d), (-1.0d)};
//    org.apache.commons.math3.linear.DiagonalMatrix var7 = new org.apache.commons.math3.linear.DiagonalMatrix(var5, true);
//    java.lang.String var8 = var3.format((org.apache.commons.math3.linear.RealMatrix)var7);
//
//  }

//  public void test77() throws Throwable {
//
//    if (debug) { System.out.println(); System.out.print("RandoopTest0.test77"); }
//
//
//    org.apache.commons.math3.fraction.BigFractionFormat var0 = org.apache.commons.math3.fraction.BigFractionFormat.getProperInstance();
//    var0.setWholeFormat((java.text.NumberFormat)var0);
//    org.apache.commons.math3.fraction.BigFraction var3 = new org.apache.commons.math3.fraction.BigFraction(10.0d);
//    java.lang.StringBuffer var4 = new java.lang.StringBuffer();
//    java.text.FieldPosition var6 = new java.text.FieldPosition((-2089898386));
//    java.lang.StringBuffer var7 = var0.format(var3, var4, var6);
//
//  }

  public void test78() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test78"); }


    org.apache.commons.math3.optimization.general.GaussNewtonOptimizer var1 = new org.apache.commons.math3.optimization.general.GaussNewtonOptimizer(false);
    org.apache.commons.math3.optimization.PointVectorValuePair var2 = var1.doOptimize();

  }

  public void test79() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test79"); }


    org.apache.commons.math3.analysis.integration.gauss.GaussIntegratorFactory var0 = new org.apache.commons.math3.analysis.integration.gauss.GaussIntegratorFactory();
    org.apache.commons.math3.analysis.integration.gauss.GaussIntegrator var2 = var0.legendre(1055287316);

  }

  public void test80() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test80"); }


    java.lang.Double[] var1 = new java.lang.Double[] { 100.0d};
    org.apache.commons.math3.linear.ArrayRealVector var2 = new org.apache.commons.math3.linear.ArrayRealVector(var1);
    org.apache.commons.math3.linear.RealVector var3 = org.apache.commons.math3.linear.RealVector.unmodifiableRealVector((org.apache.commons.math3.linear.RealVector)var2);
    
    // Checks the contract:  var3.equals(var3)
    assertTrue("Contract failed: var3.equals(var3)", var3.equals(var3));
    
    // Checks the contract:  !var3.equals(null)
    assertTrue("Contract failed: !var3.equals(null)", !var3.equals(null));

  }

}
