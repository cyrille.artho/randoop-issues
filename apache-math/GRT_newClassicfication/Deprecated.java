package enhancement;
import junit.framework.*;

public class Deprecated extends TestCase {

	public static boolean debug = false;	
	
  public void test3() throws Throwable {
    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(1.0d, 1.0d, 1.0d, 1.0d, 1.0d);
    double[] var2 = new double[] { 1.0d, 1.0d};
    double[][] var3 = var1.computeCovariances(var2, 1.0d);

  }

  public void test6() throws Throwable {
    new org.apache.commons.math3.optimization.general.GaussNewtonOptimizer().getWeight();
  }

  public void test7() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test7"); }


    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var0 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer();
    org.apache.commons.math3.linear.RealMatrix var1 = var0.getWeightSquareRoot();

  }

  public void test8() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test8"); }


    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(100.0d, 100.0d, 100.0d);
    org.apache.commons.math3.linear.RealMatrix var2 = var1.getWeight();

  }

  public void test13() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test13"); }


    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(23.075991912170917d, 23.075991912170917d, 23.075991912170917d, 23.075991912170917d, 23.075991912170917d);
    org.apache.commons.math3.linear.RealMatrix var2 = var1.getWeightSquareRoot();

  }

  public void test14() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test14"); }


    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(0.0d, 0.0d, 0.0d);
    double[][] var2 = var1.getCovariances(0.0d);

  }

  public void test15() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test15"); }


    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(0.0d, 0.0d, 0.0d);
    double[][] var2 = var1.getCovariances();

  }

  public void test16() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test16"); }


    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(23.075991912170917d, 23.075991912170917d, 23.075991912170917d, 23.075991912170917d, 23.075991912170917d);
    double[] var2 = new double[] { 23.075991912170917d, 23.075991912170917d};
    double[][] var3 = var1.computeCovariances(var2, 23.075991912170917d);

  }


  public void test17() throws Throwable {
    org.apache.commons.math3.optimization.direct.NelderMeadSimplex var1 = new org.apache.commons.math3.optimization.direct.NelderMeadSimplex(45);
    org.apache.commons.math3.optimization.PointValuePair var2 = var1.getPoint(45);
  }


    public void test23() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test23"); }


    org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex var2 = new org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex(10, 0.5773502691895d);
    org.apache.commons.math3.optimization.PointValuePair[] var3 = var2.getPoints();

  }

    public void test28() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test28"); }


    org.apache.commons.math3.analysis.solvers.LaguerreSolver var0 = new org.apache.commons.math3.analysis.solvers.LaguerreSolver();
    double var2 = var0.laguerre(2.0d, 2.0d, 2.0d, 2.0d);

  }

    public void test32() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test32"); }


    double[] var2 = new double[] { 10.0d, 10.0d, 10.0d};
    org.apache.commons.math3.optimization.direct.CMAESOptimizer var3 = new org.apache.commons.math3.optimization.direct.CMAESOptimizer(80, var2);
    java.util.List var4 = var3.getStatisticsMeanHistory();
    org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet var5 = new org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet((java.util.Collection)var4);
    org.apache.commons.math3.geometry.partitioning.BSPTree var6 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
    boolean var7 = var5.isEmpty(var6);

  }

    public void test34() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test34"); }


    org.apache.commons.math3.optimization.general.GaussNewtonOptimizer var1 = new org.apache.commons.math3.optimization.general.GaussNewtonOptimizer(false);
    double[] var2 = var1.getTarget();

  }

    public void test48() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test48"); }


    org.apache.commons.math3.optimization.general.GaussNewtonOptimizer var1 = new org.apache.commons.math3.optimization.general.GaussNewtonOptimizer(false);
    double[] var2 = var1.getStartPoint();

  }

    public void test55() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test55"); }


    org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex var2 = new org.apache.commons.math3.optimization.direct.MultiDirectionalSimplex(10, 0.6470916444326115d, 0.6470916444326115d, 0.6470916444326115d);
    int var3 = var2.getSize();

  }


  public void test71() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test71"); }


    org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer(0.45121005791836927d, 0.45121005791836927d, 0.45121005791836927d, 0.45121005791836927d, 0.45121005791836927d);
    double[] var2 = new double[] { 0.45121005791836927d, 0.45121005791836927d, 0.45121005791836927d};
    double[] var3 = var1.computeSigma(var2, 0.45121005791836927d);

  }

  public void test78() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test78"); }


    org.apache.commons.math3.optimization.general.GaussNewtonOptimizer var1 = new org.apache.commons.math3.optimization.general.GaussNewtonOptimizer(false);
    org.apache.commons.math3.optimization.PointVectorValuePair var2 = var1.doOptimize();

  }


  // More tests depracated from 3-3.2 to 3-3.5
  
  public void test29() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test29"); }


	    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(100.0d, 100.0d, 100.0d);
	    int var2 = var1.getTargetSize();

	  }  

  
  public void test37() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test37"); }


	    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(100.0d, 100.0d, 100.0d);
	    double var2 = var1.getRMS();

	  }
  
  public void test41() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test41"); }


	    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(1.0d, 1.0d, 1.0d, 1.0d, 1.0d);
	    double[] var2 = new double[] { 1.0d};
	    double[] var3 = var1.computeSigma(var2, 1.0d);

	  }
  
  public void test45() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test45"); }


	    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(100.0d, 100.0d, 100.0d);
	    double[] var2 = var1.getTarget();

	  }
  
  public void test49() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test49"); }


	    org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(100.0d, 100.0d, 100.0d);
	    org.apache.commons.math3.random.UnitSphereRandomVectorGenerator var3 = new org.apache.commons.math3.random.UnitSphereRandomVectorGenerator(18);
	    org.apache.commons.math3.optim.nonlinear.vector.MultiStartMultivariateVectorOptimizer var4 = new org.apache.commons.math3.optim.nonlinear.vector.MultiStartMultivariateVectorOptimizer((org.apache.commons.math3.optim.nonlinear.vector.MultivariateVectorOptimizer)var1, 18, (org.apache.commons.math3.random.RandomVectorGenerator)var3);
	    org.apache.commons.math3.optim.PointVectorValuePair[] var5 = var4.getOptima();

	  }

  public void test51() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test51"); }


	    org.apache.commons.math3.geometry.partitioning.BSPTree var0 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
	    org.apache.commons.math3.geometry.euclidean.oned.Vector1D var2 = new org.apache.commons.math3.geometry.euclidean.oned.Vector1D(100.0d);
	    org.apache.commons.math3.geometry.euclidean.oned.OrientedPoint var4 = new org.apache.commons.math3.geometry.euclidean.oned.OrientedPoint(var2, true);
	    boolean var5 = var0.insertCut((org.apache.commons.math3.geometry.partitioning.Hyperplane)var4);

	  }
  
  public void test56() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test56"); }


	    org.apache.commons.math3.geometry.partitioning.BSPTree var0 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
	    org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet var1 = new org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet(var0);
	    double var2 = var1.getSup();

	  }
  
  public void test62() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test62"); }


	    org.apache.commons.math3.optim.univariate.SimpleUnivariateValueChecker var3 = new org.apache.commons.math3.optim.univariate.SimpleUnivariateValueChecker((-961.206742385875d), (-961.206742385875d), 64);
	    org.apache.commons.math3.optim.nonlinear.vector.jacobian.GaussNewtonOptimizer var4 = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.GaussNewtonOptimizer(false, (org.apache.commons.math3.optim.ConvergenceChecker)var3);
	    org.apache.commons.math3.optim.PointVectorValuePair var5 = var4.doOptimize();

	  }

public void test69() throws Throwable {

if (debug) { System.out.println(); System.out.print("RandoopTest0.test69"); }


org.apache.commons.math3.geometry.partitioning.BSPTree var0 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet var1 = new org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet(var0);
//org.apache.commons.math3.geometry.Vector var2 = var1.getBarycenter();

}

public void test72() throws Throwable {

if (debug) { System.out.println(); System.out.print("RandoopTest0.test72"); }


org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet var0 = new org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet();
org.apache.commons.math3.geometry.partitioning.BSPTree var1 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet var2 = var0.buildNew(var1);
boolean var3 = var2.isEmpty();

}
public void test75() throws Throwable {

  if (debug) { System.out.println(); System.out.print("RandoopTest0.test75"); }


  org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet var0 = new org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet();
  org.apache.commons.math3.geometry.partitioning.BSPTree var1 = new org.apache.commons.math3.geometry.partitioning.BSPTree();
  org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet var2 = var0.buildNew(var1);
  boolean var3 = var2.contains((org.apache.commons.math3.geometry.partitioning.Region)var0);

}
  
}
