package enhancement;


import junit.framework.*;

public class ReportNPE3 extends TestCase {

  public static boolean debug = false;


//  NPE not advertised
  
  // do a calculation on an object with no data, API doc not specified
  public void test39() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test39"); }


	    org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression();
	    double[] var1 = var0.estimateRegressionParametersStandardErrors();

	  }  
  
  
  //should call build before getPoints, but not mentioned in Javadoc
  public void test40() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test40"); }


	    org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex var1 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex(100);
//Lei	    var1.build(new double[]{1,2,3});
	    org.apache.commons.math3.optim.PointValuePair[] var2 = var1.getPoints();

	  }
  
  //should call build before getPoints, but not mentioned in Javadoc  
  public void test43() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test43"); }


	    double[] var1 = new double[] { 1.0d};
	    org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex var2 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex(var1, 1.0d, 1.0d, 1.0d, 1.0d);
	    int var3 = var2.getSize();

	  }
  
  
}
