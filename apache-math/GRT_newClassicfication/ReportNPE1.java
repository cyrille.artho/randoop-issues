package enhancement;


import junit.framework.*;

public class ReportNPE1 extends TestCase {

  public static boolean debug = false;


//Uninitialized internal objects and no documentation on such issues in JavaDoc.
//NPE not advertised
  
  
  
  public void test31() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test31"); }


	    org.apache.commons.math3.random.EmpiricalDistribution var0 = new org.apache.commons.math3.random.EmpiricalDistribution();
	    double var1 = var0.getNumericalMean();

	  }
  
  
  public void test35() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test35"); }


	    org.apache.commons.math3.random.EmpiricalDistribution var0 = new org.apache.commons.math3.random.EmpiricalDistribution();
	    double var1 = var0.getNumericalVariance();

	  }
  
  
  public void test58() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test58"); }


	    org.apache.commons.math3.random.EmpiricalDistribution var1 = new org.apache.commons.math3.random.EmpiricalDistribution(100);
	    double var3 = var1.inverseCumulativeProbability(0.4472135954999579d);

	  }
  
}
