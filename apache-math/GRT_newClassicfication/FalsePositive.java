package enhancement;


import junit.framework.*;

public class FalsePositive extends TestCase {

  public static boolean debug = false;

  
  public void test12() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test12"); }


	    org.apache.commons.math3.stat.correlation.PearsonsCorrelation var0 = new org.apache.commons.math3.stat.correlation.PearsonsCorrelation();
	    org.apache.commons.math3.linear.RealMatrix var1 = var0.getCorrelationStandardErrors();

	  }
  
/////////////////////It is mandatory to set
//  The SimplexSolver supports the following OptimizationData data provided as arguments to optimize(OptimizationData):
//
//	  objective function: LinearObjectiveFunction - mandatory
//	  linear constraints LinearConstraintSet - mandatory
  ///////////////////////////////////////  
  public void test21() throws Throwable {
	    org.apache.commons.math3.optim.linear.SimplexSolver var2 = new org.apache.commons.math3.optim.linear.SimplexSolver((-34.55974473970935d), (-1), (-34.55974473970935d));
	    double[] var3 = new double[] { };
	    org.apache.commons.math3.optim.linear.LinearObjectiveFunction var4 = new org.apache.commons.math3.optim.linear.LinearObjectiveFunction(var3, (-34.55974473970935d));
	    org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction var5 = new org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction((org.apache.commons.math3.analysis.MultivariateFunction)var4);
	    org.apache.commons.math3.optim.OptimizationData[] var6 = new org.apache.commons.math3.optim.OptimizationData[] { var5};
	    //data format incorrect here!
	    org.apache.commons.math3.optim.PointValuePair var7 = var2.optimize(var6);
	  }
	  
	  public void test22() throws Throwable {
		    org.apache.commons.math3.optim.linear.SimplexSolver var1 = new org.apache.commons.math3.optim.linear.SimplexSolver(100.0d);
		    double[] var2 = new double[] { };
		    double[][] var3 = new double[][] { var2};
		    org.apache.commons.math3.optim.nonlinear.scalar.noderiv.MultiDirectionalSimplex var4 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.MultiDirectionalSimplex(var3, 100.0d, 100.0d);
		    org.apache.commons.math3.optim.OptimizationData[] var5 = new org.apache.commons.math3.optim.OptimizationData[] { var4};
		    org.apache.commons.math3.optim.PointValuePair var6 = var1.optimize(var5);
		  }
	  
	  public void test63() throws Throwable {

		    if (debug) { System.out.println(); System.out.print("RandoopTest0.test63"); }

		    org.apache.commons.math3.optim.linear.SimplexSolver var2 = new org.apache.commons.math3.optim.linear.SimplexSolver(0.0d, 15);
		    org.apache.commons.math3.optim.PointValuePair var3 = var2.doOptimize();

		  }
	  
////////////////////////////////////////////////////////////
  
  //false positive: discussed in issue 1116
  public void test27() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test27"); }


	    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
	    double[] var1 = var0.getInterpolatedDerivatives();

	  }


  //false positive: discussed in issue 1116
	  public void test33() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test33"); }


	    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
	    var0.rescale(2.5d);

	  }
  
	  //false positive: discussed in issue 1116
  public void test30() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test30"); }


	    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
	    double[] var2 = var0.getInterpolatedSecondaryDerivatives(3);

	  }
  
  //false positive: discussed in issue 1116
  public void test44() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test44"); }


	    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
	    double[] var1 = var0.getInterpolatedStateVariation();

	  }


  //false positive: discussed in issue 1116
  public void test59() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test59"); }


	    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
	    double[] var1 = var0.getInterpolatedState();

	  }

  //false positive: discussed in issue 1116
	  public void test60() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test60"); }


	    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var0 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
	    double[] var2 = var0.getInterpolatedSecondaryState(2147483647);

	  }
	  
	  //false positive: discussed in issue 1116
	  public void test73() throws Throwable {

		    if (debug) { System.out.println(); System.out.print("RandoopTest0.test73"); }


		    org.apache.commons.math3.ode.ContinuousOutputModel var0 = new org.apache.commons.math3.ode.ContinuousOutputModel();
		    org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator var1 = new org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator();
		    var0.handleStep((org.apache.commons.math3.ode.sampling.StepInterpolator)var1, false);
		    var0.append(var0);

		  }	  
	  
	  //NullPointerException - if the valuesFileURL has not been set.
	  public void test53() throws Throwable {

		    if (debug) { System.out.println(); System.out.print("RandoopTest0.test53"); }


		    org.apache.commons.math3.random.ValueServer var0 = new org.apache.commons.math3.random.ValueServer();
		    var0.resetReplayFile();

		  }
	  
	  
	  

//false positive: equals need to be overrided as described by javadoc.
  public void test80() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test80"); }


    java.lang.Double[] var1 = new java.lang.Double[] { 100.0d};
    org.apache.commons.math3.linear.ArrayRealVector var2 = new org.apache.commons.math3.linear.ArrayRealVector(var1);
    org.apache.commons.math3.linear.RealVector var3 = org.apache.commons.math3.linear.RealVector.unmodifiableRealVector((org.apache.commons.math3.linear.RealVector)var2);
    
    // Checks the contract:  var3.equals(var3)
    assertTrue("Contract failed: var3.equals(var3)", var3.equals(var3));
    
    // Checks the contract:  !var3.equals(null)
    assertTrue("Contract failed: !var3.equals(null)", !var3.equals(null));

  }
  
  
  //duplicated as described in issue 1116
  public void test25() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test25"); }

	    org.apache.commons.math3.random.EmpiricalDistribution var0 = new org.apache.commons.math3.random.EmpiricalDistribution();
	    double[] var1 = var0.getGeneratorUpperBounds();

	  }
  
  //  this should be false positive 
  // as java doc mentions that return NullPointerException - if the sample has not been set
  public void test36() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test36"); }

	    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
	    double var1 = var0.calculateTotalSumOfSquares();

	  }

//this should be false positive 
	  // as java doc mentions that return NullPointerException - if the sample has not been set

	  public void test42() throws Throwable {

		    if (debug) { System.out.println(); System.out.print("RandoopTest0.test42"); }


		    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
		    double var1 = var0.calculateAdjustedRSquared();

		  }
	  
	//  this should be false positive 
	// as java doc mentions that return NullPointerException - if the sample has not been set
	  public void test52() throws Throwable {

		    if (debug) { System.out.println(); System.out.print("RandoopTest0.test52"); }


		    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
		    double var1 = var0.calculateResidualSumOfSquares();

		  }
	  
		//  this should be false positive 
		// as java doc mentions that return NullPointerException - if the sample has not been set
	  
	  public void test57() throws Throwable {

		    if (debug) { System.out.println(); System.out.print("RandoopTest0.test57"); }


		    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
		    double var1 = var0.calculateRSquared();

		  }
	  
	  
	    public void test67() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test67"); }


	    java.lang.Double[] var1 = new java.lang.Double[] { 100.0d};
	    org.apache.commons.math3.linear.ArrayRealVector var2 = new org.apache.commons.math3.linear.ArrayRealVector(var1);
//	    java.lang.Object var3 = component.model.IdentityMethod.IdentityForward((java.lang.Object)var2);
	    org.apache.commons.math3.linear.OpenMapRealVector var4 = new org.apache.commons.math3.linear.OpenMapRealVector(var1);
//	    java.lang.Object var5 = component.model.IdentityMethod.IdentityForward((java.lang.Object)var4);
	    org.apache.commons.math3.linear.RealVector var6 = var2.combine(100.0d, 100.0d, (org.apache.commons.math3.linear.RealVector)var4);
	    
	    // Checks the contract:  equals-hashcode on var3 and var5
	    assertTrue("Contract failed: equals-hashcode on var3 and var5", var2.equals(var4) ? var2.hashCode() == var4.hashCode() : true);
	    
	    // This assertion (symmetry of equals) fails 
	    assertTrue("Contract failed: equals-symmetric on var3 and var5.", var2.equals(var4) == var4.equals(var2));

	  }
	    
	    public void test68() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test68"); }


	    java.lang.Double[] var1 = new java.lang.Double[] { 100.0d};
	    org.apache.commons.math3.linear.OpenMapRealVector var2 = new org.apache.commons.math3.linear.OpenMapRealVector(var1);
	    org.apache.commons.math3.linear.RealVector var3 = org.apache.commons.math3.linear.RealVector.unmodifiableRealVector((org.apache.commons.math3.linear.RealVector)var2);
	    
	    // Checks the contract:  var3.equals(var3)
	    assertTrue("Contract failed: var3.equals(var3)", var3.equals(var3));
	    
	    // Checks the contract:  !var3.equals(null)
	    assertTrue("Contract failed: !var3.equals(null)", !var3.equals(null));

	  }
	    
	    public void test70() throws Throwable {

	        if (debug) { System.out.println(); System.out.print("RandoopTest0.test70"); }


	        double[] var1 = new double[] { 1.0d, 1.0d};
	        double[][] var2 = new double[][] { var1};
	        org.apache.commons.math3.distribution.fitting.MultivariateNormalMixtureExpectationMaximization var3 = new org.apache.commons.math3.distribution.fitting.MultivariateNormalMixtureExpectationMaximization(var2);
	        org.apache.commons.math3.distribution.MixtureMultivariateNormalDistribution var4 = var3.getFittedModel();

	      }
  
}
