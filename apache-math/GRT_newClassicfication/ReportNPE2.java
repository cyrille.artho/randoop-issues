package enhancement;


import junit.framework.*;

public class ReportNPE2 extends TestCase {

  public static boolean debug = false;

  

//  NPE not advertised
  
  public void test38() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test38"); }


	    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
	    double[] var1 = var0.estimateRegressionParameters();

	  }
  
  
  
  
  public void test46() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test46"); }


	    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
	    double var1 = var0.estimateRegressandVariance();

	  }

	  public void test47() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test47"); }


	    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
	    double var1 = var0.estimateRegressionStandardError();

	  }

	  public void test50() throws Throwable {

		    if (debug) { System.out.println(); System.out.print("RandoopTest0.test50"); }


		    org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression();
		    double[] var1 = var0.estimateResiduals();

		  }
	  
	  
	  public void test54() throws Throwable {

		    if (debug) { System.out.println(); System.out.print("RandoopTest0.test54"); }


		    org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression var0 = new org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression();
		    double var1 = var0.estimateErrorVariance();

		  }
  
}
