package enhancement;


import junit.framework.*;

public class Report61 extends TestCase {

  public static boolean debug = false;

  public void test61() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test61"); }

	    org.apache.commons.math3.dfp.DfpField var1 = new org.apache.commons.math3.dfp.DfpField(1);
	    org.apache.commons.math3.dfp.Dfp var2 = var1.getTwo();
	    org.apache.commons.math3.dfp.DfpDec var3 = new org.apache.commons.math3.dfp.DfpDec(var2);
	    org.apache.commons.math3.dfp.Dfp var5 = var3.newInstance((byte)0);
	    org.apache.commons.math3.dfp.Dfp var6 = var3.newInstance((byte)0, (byte)0);
	    
	    // Checks the contract:  equals-hashcode on var5 and var6
	    assertTrue("Contract failed: equals-hashcode on var5 and var6", var5.equals(var6) ? var5.hashCode() == var6.hashCode() : true);
	    
	    // Checks the contract:  equals-hashcode on var6 and var5
	    assertTrue("Contract failed: equals-hashcode on var6 and var5", var6.equals(var5) ? var6.hashCode() == var5.hashCode() : true);

	  }
  
  
  
  public void test4() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test4"); }


	    org.apache.commons.math3.dfp.DfpField var1 = new org.apache.commons.math3.dfp.DfpField(1);
	    org.apache.commons.math3.dfp.Dfp var3 = var1.newDfp("-1");
	    org.apache.commons.math3.dfp.Dfp var5 = var1.newDfp(0L);
	    org.apache.commons.math3.dfp.Dfp var6 = var3.remainder(var3);
	    
	    // Checks the contract:  equals-hashcode on var5 and var6
	    assertTrue("Contract failed: equals-hashcode on var5 and var6", var5.equals(var6) ? var5.hashCode() == var6.hashCode() : true);
	    
	    // Checks the contract:  equals-hashcode on var6 and var5
	    assertTrue("Contract failed: equals-hashcode on var6 and var5", var6.equals(var5) ? var6.hashCode() == var5.hashCode() : true);

	  }
  
  
  public void test5() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test5"); }


	    org.apache.commons.math3.complex.Complex var1 = org.apache.commons.math3.complex.Complex.valueOf((-1.0d));
	    org.apache.commons.math3.complex.ComplexField var2 = org.apache.commons.math3.complex.ComplexField.getInstance();
	    org.apache.commons.math3.complex.Complex var3 = var2.getOne();
	    org.apache.commons.math3.complex.Complex var4 = var1.negate();
	    org.apache.commons.math3.complex.Complex var5 = var1.divide(var1);
	    
	    // Checks the contract:  equals-hashcode on var3 and var4
	    assertTrue("Contract failed: equals-hashcode on var3 and var4", var3.equals(var4) ? var3.hashCode() == var4.hashCode() : true);
	    
	    // Checks the contract:  equals-hashcode on var3 and var5
	    assertTrue("Contract failed: equals-hashcode on var3 and var5", var3.equals(var5) ? var3.hashCode() == var5.hashCode() : true);
	    
	    // Checks the contract:  equals-hashcode on var4 and var3
	    assertTrue("Contract failed: equals-hashcode on var4 and var3", var4.equals(var3) ? var4.hashCode() == var3.hashCode() : true);
	    
	    // Checks the contract:  equals-hashcode on var5 and var3
	    assertTrue("Contract failed: equals-hashcode on var5 and var3", var5.equals(var3) ? var5.hashCode() == var3.hashCode() : true);

	  }
}
