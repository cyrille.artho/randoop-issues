package enhancement;

import junit.framework.*;

public class IllegalState2 extends TestCase {

	public static boolean debug = false;

	
	//Illegal states.
	
	public void test19() throws Throwable {
		org.apache.commons.math3.optim.nonlinear.scalar.noderiv.SimplexOptimizer var1 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.SimplexOptimizer(
				1.000000000001d, 1.000000000001d);
		org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex var3 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex(
				10, 1.000000000001d, 1.000000000001d, 1.000000000001d,
				1.000000000001d);
		org.apache.commons.math3.optim.OptimizationData[] var4 = new org.apache.commons.math3.optim.OptimizationData[] { var3 };
		org.apache.commons.math3.optim.PointValuePair var5 = var1
				.optimize(var4);

	}

	public void test65() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test65");
		}

		org.apache.commons.math3.optim.univariate.BrentOptimizer var1 = new org.apache.commons.math3.optim.univariate.BrentOptimizer(
				1.000000000001d, 1.000000000001d);
		org.apache.commons.math3.optim.MaxEval var2 = org.apache.commons.math3.optim.MaxEval
				.unlimited();
		org.apache.commons.math3.optim.OptimizationData[] var3 = new org.apache.commons.math3.optim.OptimizationData[] { var2 };
		org.apache.commons.math3.optim.univariate.UnivariatePointValuePair var4 = var1
				.optimize(var3);

	}

	public void test66() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test66");
		}

		org.apache.commons.math3.random.MersenneTwister var4 = new org.apache.commons.math3.random.MersenneTwister(
				1L);
		org.apache.commons.math3.optim.SimpleVectorValueChecker var5 = new org.apache.commons.math3.optim.SimpleVectorValueChecker(
				2.302736646959952d, 2.302736646959952d, 50);
		org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer var6 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer(
				50, 2.302736646959952d, true, 50, 50,
				(org.apache.commons.math3.random.RandomGenerator) var4, true,
				(org.apache.commons.math3.optim.ConvergenceChecker) var5);
		org.apache.commons.math3.optim.nonlinear.scalar.noderiv.MultiDirectionalSimplex var7 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.MultiDirectionalSimplex(
				50, 2.302736646959952d);
		org.apache.commons.math3.optim.OptimizationData[] var8 = new org.apache.commons.math3.optim.OptimizationData[] { var7 };
		org.apache.commons.math3.optim.PointValuePair var9 = var6
				.optimize(var8);

	}

}
