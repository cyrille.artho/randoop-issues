package enhancement;


import junit.framework.*;

public class IllegalState1 extends TestCase {

  public static boolean debug = false;


//  NPE not advertised
  
  
  //need to set the differential equation before computing derivatives?
  public void test24() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test24"); }


	    org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator var1 = new org.apache.commons.math3.ode.nonstiff.HighamHall54Integrator(0.1700478193163087d, 0.1700478193163087d, 0.1700478193163087d, 0.1700478193163087d);
	    double[] var2 = new double[] { 0.1700478193163087d, 0.1700478193163087d, 0.1700478193163087d};
	    double[] var3 = new double[] { 0.1700478193163087d, 0.1700478193163087d, 0.1700478193163087d};
	    var1.computeDerivatives(0.1700478193163087d, var2, var3);

	  }
  
  
  // need to call build before using getPoint.
  
  public void test20() throws Throwable {
	    double[] var1 = new double[] { 100.0d, 100.0d};
	    new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex(var1).getPoint(1);
	  }

  
  //the constructor does not initialize rankCorrelation object, this is null when evaluating getCorrelationMatrix
  public void test64() throws Throwable {

	    if (debug) { System.out.println(); System.out.print("RandoopTest0.test64"); }

	    org.apache.commons.math3.stat.correlation.SpearmansCorrelation var0 = new org.apache.commons.math3.stat.correlation.SpearmansCorrelation();
	    org.apache.commons.math3.linear.RealMatrix var1 = var0.getCorrelationMatrix();

	  }
  
}
