package enhancement;


import junit.framework.*;

public class IgnoreRandoop extends TestCase {

	public static boolean debug = false;
	  public void test26() throws Throwable {

		    if (debug) { System.out.println(); System.out.print("RandoopTest0.test26"); }


		    org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics var0 = new org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics();
		    org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics var1 = var0.copy();
		    org.apache.commons.math3.stat.descriptive.SummaryStatistics var2 = new org.apache.commons.math3.stat.descriptive.SummaryStatistics();
		    org.apache.commons.math3.stat.descriptive.AggregateSummaryStatistics var3 = new org.apache.commons.math3.stat.descriptive.AggregateSummaryStatistics((org.apache.commons.math3.stat.descriptive.SummaryStatistics)var1, var2);
		    org.apache.commons.math3.stat.descriptive.SummaryStatistics var4 = var3.createContributingStatistics();
//		    java.lang.Object var5 = component.model.IdentityMethod.IdentityForward((java.lang.Object)var3);
		    double var6 = var3.getGeometricMean();
		    
		    // Regression assertion (captures the current behavior of the code)
		    assertNotNull(var1);
		    
		    // Regression assertion (captures the current behavior of the code)
		    assertNotNull(var4);
		    
		    // Regression assertion (captures the current behavior of the code)
//		    assertNotNull(var5);
		    
		    // Regression assertion (captures the current behavior of the code)
//		    assertTrue(var6 == Double.NaN);
		    assertTrue(((Double)var6).equals( Double.NaN));

		  }
	  
	  
/***	  The following three method do not compile when moveing to version 3.5***/
	  
	//  public void test74() throws Throwable {
	  //
//	      if (debug) { System.out.println(); System.out.print("RandoopTest0.test74"); }
	  //
	  //
//	      org.apache.commons.math3.fraction.BigFractionFormat var0 = org.apache.commons.math3.fraction.BigFractionFormat.getProperInstance();
	  //    
//	      var0.setWholeFormat((java.text.NumberFormat)var0);
//	      java.lang.String var3 = var0.format((-75L));
	  //  }

	    

	  //  public void test76() throws Throwable {
	  //
//	      if (debug) { System.out.println(); System.out.print("RandoopTest0.test76"); }
	  //
	  //
//	      org.apache.commons.math3.fraction.BigFractionFormat var1 = org.apache.commons.math3.fraction.BigFractionFormat.getProperInstance();
//	      var1.setWholeFormat((java.text.NumberFormat)var1);
//	      org.apache.commons.math3.linear.RealMatrixFormat var3 = new org.apache.commons.math3.linear.RealMatrixFormat("-36 / 1", "-36 / 1", "-36 / 1", "-36 / 1", "-36 / 1", "-36 / 1", (java.text.NumberFormat)var1);
//	      double[] var5 = new double[] { (-1.0d), (-1.0d), (-1.0d)};
//	      org.apache.commons.math3.linear.DiagonalMatrix var7 = new org.apache.commons.math3.linear.DiagonalMatrix(var5, true);
//	      java.lang.String var8 = var3.format((org.apache.commons.math3.linear.RealMatrix)var7);
	  //
	  //  }

	  //  public void test77() throws Throwable {
	  //
//	      if (debug) { System.out.println(); System.out.print("RandoopTest0.test77"); }
	  //
	  //
//	      org.apache.commons.math3.fraction.BigFractionFormat var0 = org.apache.commons.math3.fraction.BigFractionFormat.getProperInstance();
//	      var0.setWholeFormat((java.text.NumberFormat)var0);
//	      org.apache.commons.math3.fraction.BigFraction var3 = new org.apache.commons.math3.fraction.BigFraction(10.0d);
//	      java.lang.StringBuffer var4 = new java.lang.StringBuffer();
//	      java.text.FieldPosition var6 = new java.text.FieldPosition((-2089898386));
//	      java.lang.StringBuffer var7 = var0.format(var3, var4, var6);
	  //
	  //  }

}
