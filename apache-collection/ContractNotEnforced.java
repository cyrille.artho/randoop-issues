import junit.framework.*;

public class ContractNotEnforced extends TestCase {

  // case 1: TransformIterator
  public void test4() throws Throwable {
    new org.apache.commons.collections4.iterators.TransformIterator().next();
  }


  // case 2: EnumerationIterator
  public void test14() throws Throwable {
    new org.apache.commons.collections4.iterators.EnumerationIterator().next();
  }

  // case 3: IteratorEnumeration
  public void test16() throws Throwable {
    new org.apache.commons.collections4.iterators.IteratorEnumeration().hasMoreElements();
  }

  // case 4: FilterListIterator
  public void test23() throws Throwable {
    new org.apache.commons.collections4.iterators.FilterListIterator().next();
  }

  // case 5: FilterIterator
  public void test7() throws Throwable {
    new org.apache.commons.collections4.iterators.FilterIterator().next();
  }
}
