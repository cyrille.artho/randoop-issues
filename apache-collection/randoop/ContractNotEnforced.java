
import junit.framework.*;

public class ContractNotEnforced extends TestCase {

  // case 1: TransformIterator
  public void test4() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test4"); }


    org.apache.commons.collections4.iterators.TransformIterator var0 = new org.apache.commons.collections4.iterators.TransformIterator();
    java.lang.Object var2 = org.apache.commons.collections4.CollectionUtils.get((java.util.Iterator)var0, 0);

  }

  public void test7() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test7"); }


    org.apache.commons.collections4.iterators.TransformIterator var0 = new org.apache.commons.collections4.iterators.TransformIterator();
    org.apache.commons.collections4.iterators.PeekingIterator var1 = org.apache.commons.collections4.iterators.PeekingIterator.peekingIterator((java.util.Iterator)var0);
    var1.remove();

  }

  public void test9() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test9"); }


    org.apache.commons.collections4.iterators.TransformIterator var0 = new org.apache.commons.collections4.iterators.TransformIterator();
    var0.remove();

  }

  public void test10() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test10"); }


    org.apache.commons.collections4.iterators.TransformIterator var0 = new org.apache.commons.collections4.iterators.TransformIterator();
    boolean var1 = var0.hasNext();

  }

  public void test13() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test13"); }


    org.apache.commons.collections4.iterators.TransformIterator var0 = new org.apache.commons.collections4.iterators.TransformIterator();
    org.apache.commons.collections4.iterators.PeekingIterator var1 = org.apache.commons.collections4.iterators.PeekingIterator.peekingIterator((java.util.Iterator)var0);
    java.lang.Object var2 = var1.peek();

  }

  public void test18() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test18"); }


    org.apache.commons.collections4.iterators.TransformIterator var0 = new org.apache.commons.collections4.iterators.TransformIterator();
    org.apache.commons.collections4.iterators.PeekingIterator var1 = org.apache.commons.collections4.iterators.PeekingIterator.peekingIterator((java.util.Iterator)var0);
    java.lang.Object var2 = var1.next();

  }

  public void test20() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test20"); }


    org.apache.commons.collections4.iterators.TransformIterator var0 = new org.apache.commons.collections4.iterators.TransformIterator();
    org.apache.commons.collections4.iterators.PeekingIterator var1 = org.apache.commons.collections4.iterators.PeekingIterator.peekingIterator((java.util.Iterator)var0);
    java.util.List var2 = org.apache.commons.collections4.IteratorUtils.toList((java.util.Iterator)var1);

  }

  public void test27() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test27"); }


    org.apache.commons.collections4.iterators.TransformIterator var0 = new org.apache.commons.collections4.iterators.TransformIterator();
    java.lang.Object var1 = var0.next();

  }

  // case 2: EnumerationIterator
  public void test14() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test14"); }


    org.apache.commons.collections4.iterators.EnumerationIterator var0 = new org.apache.commons.collections4.iterators.EnumerationIterator();
    java.lang.Object var1 = var0.next();

  }

  // case 3: IteratorEnumeration
  public void test16() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test16"); }


    org.apache.commons.collections4.iterators.IteratorEnumeration var0 = new org.apache.commons.collections4.iterators.IteratorEnumeration();
    boolean var1 = var0.hasMoreElements();

  }

  // case 4: FilterListIterator
  public void test23() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test23"); }


    org.apache.commons.collections4.functors.UniquePredicate var0 = new org.apache.commons.collections4.functors.UniquePredicate();
    org.apache.commons.collections4.iterators.SingletonListIterator var1 = new org.apache.commons.collections4.iterators.SingletonListIterator((java.lang.Object)var0);
    java.lang.Object var2 = var1.next();
    java.lang.Object var3 = var1.previous();
    org.apache.commons.collections4.iterators.FilterListIterator var4 = new org.apache.commons.collections4.iterators.FilterListIterator((java.util.ListIterator)var1);
    java.lang.Object var5 = var4.next();

  }

}
