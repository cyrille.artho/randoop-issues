import junit.framework.*;

public class FalsePositives extends TestCase {

  // nullFactory results in null entries for multimap that leads to NPE

  public void test12() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test12"); }


    org.apache.commons.collections4.map.PassiveExpiringMap var0 = new org.apache.commons.collections4.map.PassiveExpiringMap();
    org.apache.commons.collections4.Factory var1 = org.apache.commons.collections4.FactoryUtils.nullFactory();
    org.apache.commons.collections4.map.MultiValueMap var2 = org.apache.commons.collections4.map.MultiValueMap.<java.lang.Object,java.lang.Object,java.util.Collection>multiValueMap((java.util.Map)var0, var1);
    org.apache.commons.collections4.Transformer var3 = org.apache.commons.collections4.TransformerUtils.stringValueTransformer();
    org.apache.commons.collections4.comparators.TransformingComparator var4 = new org.apache.commons.collections4.comparators.TransformingComparator(var3);
    org.apache.commons.collections4.bidimap.DualTreeBidiMap var5 = new org.apache.commons.collections4.bidimap.DualTreeBidiMap((java.util.Comparator)var4, (java.util.Comparator)var4);
    java.util.Set var6 = var5.values();
    org.apache.commons.collections4.collection.SynchronizedCollection var7 = org.apache.commons.collections4.collection.SynchronizedCollection.synchronizedCollection((java.util.Collection)var6);
    java.util.Set[] var8 = new java.util.Set[] { };
    org.apache.commons.collections4.set.CompositeSet var9 = new org.apache.commons.collections4.set.CompositeSet(var8);
    org.apache.commons.collections4.map.EntrySetToMapIteratorAdapter var10 = new org.apache.commons.collections4.map.EntrySetToMapIteratorAdapter((java.util.Set)var9);
    org.apache.commons.collections4.iterators.ListIteratorWrapper var11 = new org.apache.commons.collections4.iterators.ListIteratorWrapper((java.util.Iterator)var10);
    java.lang.Object var12 = var2.put((java.lang.Object)var7, (java.lang.Object)var11);

  }

  // contract violated: "Attempted to add null object to queue"
  public void test17() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test17"); }


    org.apache.commons.collections4.queue.CircularFifoQueue var0 = new org.apache.commons.collections4.queue.CircularFifoQueue();
    org.apache.commons.collections4.functors.UniquePredicate var1 = new org.apache.commons.collections4.functors.UniquePredicate();
    boolean var2 = var0.add((java.lang.Object)var1);
    org.apache.commons.collections4.map.PassiveExpiringMap var3 = new org.apache.commons.collections4.map.PassiveExpiringMap();
    org.apache.commons.collections4.Transformer var4 = org.apache.commons.collections4.TransformerUtils.mapTransformer((java.util.Map)var3);
    org.apache.commons.collections4.queue.TransformedQueue var5 = org.apache.commons.collections4.queue.TransformedQueue.transformedQueue((java.util.Queue)var0, var4);

  }


}
