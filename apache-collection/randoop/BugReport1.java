import junit.framework.*;

public class BugReport1 extends TestCase {

	public void test1() throws Throwable {

		java.util.Set[] var0 = new java.util.Set[] {};
		org.apache.commons.collections4.set.CompositeSet var1 = new org.apache.commons.collections4.set.CompositeSet(
				var0);
		java.util.Collection var2 = org.apache.commons.collections4.CollectionUtils
				.emptyCollection();
		java.util.Collection var3 = org.apache.commons.collections4.CollectionUtils
				.subtract((java.lang.Iterable) var1, (java.lang.Iterable) var1);

		// This assertion (symmetry of equals) fails
		assertTrue("Contract failed: equals-symmetric on var2 and var3.",
				var2.equals(var3) == var3.equals(var2));

	}

	public void test8() throws Throwable {

		org.apache.commons.collections4.Transformer var0 = org.apache.commons.collections4.TransformerUtils
				.nopTransformer();
		org.apache.commons.collections4.functors.ConstantTransformer var1 = new org.apache.commons.collections4.functors.ConstantTransformer(
				(java.lang.Object) var0);
		org.apache.commons.collections4.comparators.TransformingComparator var2 = new org.apache.commons.collections4.comparators.TransformingComparator(
				(org.apache.commons.collections4.Transformer) var1);
		org.apache.commons.collections4.comparators.TransformingComparator var3 = new org.apache.commons.collections4.comparators.TransformingComparator(
				(org.apache.commons.collections4.Transformer) var1,
				(java.util.Comparator) var2);

		// Checks the contract: equals-hashcode on var2 and var3
		assertTrue("Contract failed: equals-hashcode on var2 and var3",
				var2.equals(var3) ? var2.hashCode() == var3.hashCode() : true);

		// Checks the contract: equals-hashcode on var3 and var2
		assertTrue("Contract failed: equals-hashcode on var3 and var2",
				var3.equals(var2) ? var3.hashCode() == var2.hashCode() : true);

	}

}
