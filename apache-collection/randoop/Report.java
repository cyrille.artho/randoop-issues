import junit.framework.*;

public class Report extends TestCase {

  // many cases of equals vs. hashCode
  public void test1() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test1"); }


    java.util.Set[] var0 = new java.util.Set[] { };
    org.apache.commons.collections4.set.CompositeSet var1 = new org.apache.commons.collections4.set.CompositeSet(var0);
    java.util.Collection var2 = org.apache.commons.collections4.CollectionUtils.emptyCollection();
    java.util.Collection var3 = org.apache.commons.collections4.CollectionUtils.subtract((java.lang.Iterable)var1, (java.lang.Iterable)var1);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var2 and var3.", var2.equals(var3) == var3.equals(var2));

  }

  public void test2() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test2"); }


    org.apache.commons.collections4.Transformer var0 = org.apache.commons.collections4.TransformerUtils.stringValueTransformer();
    org.apache.commons.collections4.comparators.TransformingComparator var1 = new org.apache.commons.collections4.comparators.TransformingComparator(var0);
    org.apache.commons.collections4.bidimap.DualTreeBidiMap var2 = new org.apache.commons.collections4.bidimap.DualTreeBidiMap((java.util.Comparator)var1, (java.util.Comparator)var1);
    java.util.Set var3 = var2.values();
    org.apache.commons.collections4.map.LazySortedMap var4 = org.apache.commons.collections4.map.LazySortedMap.lazySortedMap((java.util.SortedMap)var2, var0);
    java.util.Set var5 = var4.entrySet();
    
    // Checks the contract:  equals-hashcode on var5 and var3
    assertTrue("Contract failed: equals-hashcode on var5 and var3", var5.equals(var3) ? var5.hashCode() == var3.hashCode() : true);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var5 and var3.", var5.equals(var3) == var3.equals(var5));

  }

  public void test3() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test3"); }


    java.util.Set[] var0 = new java.util.Set[] { };
    org.apache.commons.collections4.set.CompositeSet var1 = new org.apache.commons.collections4.set.CompositeSet(var0);
    org.apache.commons.collections4.Transformer var2 = org.apache.commons.collections4.TransformerUtils.stringValueTransformer();
    org.apache.commons.collections4.comparators.TransformingComparator var3 = new org.apache.commons.collections4.comparators.TransformingComparator(var2);
    org.apache.commons.collections4.bidimap.DualTreeBidiMap var4 = new org.apache.commons.collections4.bidimap.DualTreeBidiMap((java.util.Comparator)var3, (java.util.Comparator)var3);
    java.util.Set var5 = var4.values();
    java.util.Set var6 = var1.toSet();
    
    // Checks the contract:  equals-hashcode on var6 and var5
    assertTrue("Contract failed: equals-hashcode on var6 and var5", var6.equals(var5) ? var6.hashCode() == var5.hashCode() : true);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var6 and var5.", var6.equals(var5) == var5.equals(var6));

  }


  public void test5() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test5"); }


    org.apache.commons.collections4.map.LRUMap var3 = new org.apache.commons.collections4.map.LRUMap(1, 1.0f, true);
    java.util.Set var4 = var3.keySet();
    org.apache.commons.collections4.Transformer var5 = org.apache.commons.collections4.TransformerUtils.stringValueTransformer();
    org.apache.commons.collections4.comparators.TransformingComparator var6 = new org.apache.commons.collections4.comparators.TransformingComparator(var5);
    org.apache.commons.collections4.bidimap.DualTreeBidiMap var7 = new org.apache.commons.collections4.bidimap.DualTreeBidiMap((java.util.Comparator)var6, (java.util.Comparator)var6);
    java.util.Set var8 = var7.values();
    org.apache.commons.collections4.splitmap.TransformedSplitMap var9 = org.apache.commons.collections4.splitmap.TransformedSplitMap.transformingMap((java.util.Map)var3, var5, var5);
    
    // Checks the contract:  equals-hashcode on var4 and var8
    assertTrue("Contract failed: equals-hashcode on var4 and var8", var4.equals(var8) ? var4.hashCode() == var8.hashCode() : true);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var4 and var8.", var4.equals(var8) == var8.equals(var4));

  }

  public void test6() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test6"); }


    org.apache.commons.collections4.map.PassiveExpiringMap var0 = new org.apache.commons.collections4.map.PassiveExpiringMap();
    org.apache.commons.collections4.Transformer var1 = org.apache.commons.collections4.TransformerUtils.stringValueTransformer();
    org.apache.commons.collections4.comparators.TransformingComparator var2 = new org.apache.commons.collections4.comparators.TransformingComparator(var1);
    org.apache.commons.collections4.bidimap.DualTreeBidiMap var3 = new org.apache.commons.collections4.bidimap.DualTreeBidiMap((java.util.Comparator)var2, (java.util.Comparator)var2);
    java.util.Set var4 = var3.values();
    org.apache.commons.collections4.map.CompositeMap var5 = new org.apache.commons.collections4.map.CompositeMap((java.util.Map)var0, (java.util.Map)var3);
    java.util.Set var6 = var5.keySet();
    
    // Checks the contract:  equals-hashcode on var6 and var4
    assertTrue("Contract failed: equals-hashcode on var6 and var4", var6.equals(var4) ? var6.hashCode() == var4.hashCode() : true);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var6 and var4.", var6.equals(var4) == var4.equals(var6));

  }

  public void test8() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test8"); }


    org.apache.commons.collections4.Transformer var0 = org.apache.commons.collections4.TransformerUtils.nopTransformer();
    org.apache.commons.collections4.functors.ConstantTransformer var1 = new org.apache.commons.collections4.functors.ConstantTransformer((java.lang.Object)var0);
    org.apache.commons.collections4.comparators.TransformingComparator var2 = new org.apache.commons.collections4.comparators.TransformingComparator((org.apache.commons.collections4.Transformer)var1);
    org.apache.commons.collections4.comparators.TransformingComparator var3 = new org.apache.commons.collections4.comparators.TransformingComparator((org.apache.commons.collections4.Transformer)var1, (java.util.Comparator)var2);
    
    // Checks the contract:  equals-hashcode on var2 and var3
    assertTrue("Contract failed: equals-hashcode on var2 and var3", var2.equals(var3) ? var2.hashCode() == var3.hashCode() : true);
    
    // Checks the contract:  equals-hashcode on var3 and var2
    assertTrue("Contract failed: equals-hashcode on var3 and var2", var3.equals(var2) ? var3.hashCode() == var2.hashCode() : true);

  }


  public void test11() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test11"); }


    org.apache.commons.collections4.map.PassiveExpiringMap var0 = new org.apache.commons.collections4.map.PassiveExpiringMap();
    org.apache.commons.collections4.Transformer var1 = org.apache.commons.collections4.TransformerUtils.stringValueTransformer();
    org.apache.commons.collections4.comparators.TransformingComparator var2 = new org.apache.commons.collections4.comparators.TransformingComparator(var1);
    org.apache.commons.collections4.bidimap.DualTreeBidiMap var3 = new org.apache.commons.collections4.bidimap.DualTreeBidiMap((java.util.Comparator)var2, (java.util.Comparator)var2);
    java.util.Set var4 = var3.values();
    org.apache.commons.collections4.map.CompositeMap var5 = new org.apache.commons.collections4.map.CompositeMap((java.util.Map)var0, (java.util.Map)var3);
    java.util.Set var6 = var3.values();
    java.util.Set var7 = var5.keySet();
    
    // Checks the contract:  equals-hashcode on var7 and var4
    assertTrue("Contract failed: equals-hashcode on var7 and var4", var7.equals(var4) ? var7.hashCode() == var4.hashCode() : true);
    
    // Checks the contract:  equals-hashcode on var7 and var6
    assertTrue("Contract failed: equals-hashcode on var7 and var6", var7.equals(var6) ? var7.hashCode() == var6.hashCode() : true);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var7 and var4.", var7.equals(var4) == var4.equals(var7));
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var7 and var6.", var7.equals(var6) == var6.equals(var7));

  }

  public void test15() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test15"); }


    org.apache.commons.collections4.map.PassiveExpiringMap var0 = new org.apache.commons.collections4.map.PassiveExpiringMap();
    org.apache.commons.collections4.Transformer var1 = org.apache.commons.collections4.TransformerUtils.stringValueTransformer();
    org.apache.commons.collections4.comparators.TransformingComparator var2 = new org.apache.commons.collections4.comparators.TransformingComparator(var1);
    org.apache.commons.collections4.bidimap.DualTreeBidiMap var3 = new org.apache.commons.collections4.bidimap.DualTreeBidiMap((java.util.Comparator)var2, (java.util.Comparator)var2);
    java.util.Set var4 = var3.values();
    org.apache.commons.collections4.map.LazySortedMap var5 = org.apache.commons.collections4.map.LazySortedMap.lazySortedMap((java.util.SortedMap)var3, var1);
    org.apache.commons.collections4.set.MapBackedSet var6 = org.apache.commons.collections4.set.MapBackedSet.mapBackedSet((java.util.Map)var0, (java.lang.Object)var5);
    java.util.Set var7 = org.apache.commons.collections4.SetUtils.synchronizedSet((java.util.Set)var6);
    
    // Checks the contract:  equals-hashcode on var7 and var4
    assertTrue("Contract failed: equals-hashcode on var7 and var4", var7.equals(var4) ? var7.hashCode() == var4.hashCode() : true);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var7 and var4.", var7.equals(var4) == var4.equals(var7));

  }

  public void test19() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test19"); }


    org.apache.commons.collections4.map.ListOrderedMap var0 = new org.apache.commons.collections4.map.ListOrderedMap();
    org.apache.commons.collections4.Transformer var1 = org.apache.commons.collections4.TransformerUtils.stringValueTransformer();
    org.apache.commons.collections4.comparators.TransformingComparator var2 = new org.apache.commons.collections4.comparators.TransformingComparator(var1);
    org.apache.commons.collections4.bidimap.DualTreeBidiMap var3 = new org.apache.commons.collections4.bidimap.DualTreeBidiMap((java.util.Comparator)var2, (java.util.Comparator)var2);
    java.util.Set var4 = var3.values();
    java.util.Set var5 = var0.entrySet();
    
    // Checks the contract:  equals-hashcode on var5 and var4
    assertTrue("Contract failed: equals-hashcode on var5 and var4", var5.equals(var4) ? var5.hashCode() == var4.hashCode() : true);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var5 and var4.", var5.equals(var4) == var4.equals(var5));

  }

  public void test22() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test22"); }


    org.apache.commons.collections4.map.PassiveExpiringMap var0 = new org.apache.commons.collections4.map.PassiveExpiringMap();
    org.apache.commons.collections4.Transformer var1 = org.apache.commons.collections4.TransformerUtils.stringValueTransformer();
    org.apache.commons.collections4.comparators.TransformingComparator var2 = new org.apache.commons.collections4.comparators.TransformingComparator(var1);
    org.apache.commons.collections4.bidimap.DualTreeBidiMap var3 = new org.apache.commons.collections4.bidimap.DualTreeBidiMap((java.util.Comparator)var2, (java.util.Comparator)var2);
    java.util.Set var4 = var3.values();
    org.apache.commons.collections4.map.LazySortedMap var5 = org.apache.commons.collections4.map.LazySortedMap.lazySortedMap((java.util.SortedMap)var3, var1);
    org.apache.commons.collections4.set.MapBackedSet var6 = org.apache.commons.collections4.set.MapBackedSet.mapBackedSet((java.util.Map)var0, (java.lang.Object)var5);
    java.util.Set var7 = org.apache.commons.collections4.SetUtils.emptyIfNull((java.util.Set)var6);
    
    // Checks the contract:  equals-hashcode on var7 and var4
    assertTrue("Contract failed: equals-hashcode on var7 and var4", var7.equals(var4) ? var7.hashCode() == var4.hashCode() : true);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var7 and var4.", var7.equals(var4) == var4.equals(var7));

  }

  public void test24() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test24"); }


    org.apache.commons.collections4.map.PassiveExpiringMap.ConstantTimeToLiveExpirationPolicy var0 = new org.apache.commons.collections4.map.PassiveExpiringMap.ConstantTimeToLiveExpirationPolicy();
    org.apache.commons.collections4.Transformer var1 = org.apache.commons.collections4.TransformerUtils.stringValueTransformer();
    org.apache.commons.collections4.comparators.TransformingComparator var2 = new org.apache.commons.collections4.comparators.TransformingComparator(var1);
    org.apache.commons.collections4.Predicate var3 = org.apache.commons.collections4.functors.ComparatorPredicate.comparatorPredicate((java.lang.Object)var0, (java.util.Comparator)var2);
    org.apache.commons.collections4.bidimap.DualTreeBidiMap var4 = new org.apache.commons.collections4.bidimap.DualTreeBidiMap((java.util.Comparator)var2, (java.util.Comparator)var2);
    java.util.Set var5 = var4.values();
    java.util.Set var6 = var4.values();
    org.apache.commons.collections4.functors.UniquePredicate var7 = new org.apache.commons.collections4.functors.UniquePredicate();
    org.apache.commons.collections4.map.ListOrderedMap var8 = new org.apache.commons.collections4.map.ListOrderedMap();
    java.util.Set var9 = var8.entrySet();
    org.apache.commons.collections4.Predicate var10 = org.apache.commons.collections4.functors.AndPredicate.andPredicate(var3, (org.apache.commons.collections4.Predicate)var7);
    
    // Checks the contract:  equals-hashcode on var9 and var5
    assertTrue("Contract failed: equals-hashcode on var9 and var5", var9.equals(var5) ? var9.hashCode() == var5.hashCode() : true);
    
    // Checks the contract:  equals-hashcode on var9 and var6
    assertTrue("Contract failed: equals-hashcode on var9 and var6", var9.equals(var6) ? var9.hashCode() == var6.hashCode() : true);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var9 and var5.", var9.equals(var5) == var5.equals(var9));
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var9 and var6.", var9.equals(var6) == var6.equals(var9));

  }

  public void test25() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test25"); }


    org.apache.commons.collections4.map.PassiveExpiringMap var0 = new org.apache.commons.collections4.map.PassiveExpiringMap();
    org.apache.commons.collections4.Transformer var1 = org.apache.commons.collections4.TransformerUtils.stringValueTransformer();
    org.apache.commons.collections4.comparators.TransformingComparator var2 = new org.apache.commons.collections4.comparators.TransformingComparator(var1);
    org.apache.commons.collections4.bidimap.DualTreeBidiMap var3 = new org.apache.commons.collections4.bidimap.DualTreeBidiMap((java.util.Comparator)var2, (java.util.Comparator)var2);
    java.util.Set var4 = var3.values();
    org.apache.commons.collections4.map.LazySortedMap var5 = org.apache.commons.collections4.map.LazySortedMap.lazySortedMap((java.util.SortedMap)var3, var1);
    org.apache.commons.collections4.set.MapBackedSet var6 = org.apache.commons.collections4.set.MapBackedSet.mapBackedSet((java.util.Map)var0, (java.lang.Object)var5);
    java.util.Set var7 = org.apache.commons.collections4.SetUtils.emptyIfNull((java.util.Set)var6);
    
    // Checks the contract:  equals-hashcode on var7 and var4
    assertTrue("Contract failed: equals-hashcode on var7 and var4", var7.equals(var4) ? var7.hashCode() == var4.hashCode() : true);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var7 and var4.", var7.equals(var4) == var4.equals(var7));

  }

  public void test26() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test26"); }


    org.apache.commons.collections4.map.PassiveExpiringMap var0 = new org.apache.commons.collections4.map.PassiveExpiringMap();
    org.apache.commons.collections4.Transformer var1 = org.apache.commons.collections4.TransformerUtils.stringValueTransformer();
    org.apache.commons.collections4.comparators.TransformingComparator var2 = new org.apache.commons.collections4.comparators.TransformingComparator(var1);
    org.apache.commons.collections4.bidimap.DualTreeBidiMap var3 = new org.apache.commons.collections4.bidimap.DualTreeBidiMap((java.util.Comparator)var2, (java.util.Comparator)var2);
    java.util.Set var4 = var3.values();
    java.util.Set var5 = var0.entrySet();
    int var6 = org.apache.commons.collections4.MapUtils.getIntValue((java.util.Map)var0, (java.lang.Object)var0);
    
    // Checks the contract:  equals-hashcode on var5 and var4
    assertTrue("Contract failed: equals-hashcode on var5 and var4", var5.equals(var4) ? var5.hashCode() == var4.hashCode() : true);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var5 and var4.", var5.equals(var4) == var4.equals(var5));

  }


  // NullPointerException (not sure why)
  public void test21() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test21"); }


    java.util.Set[] var0 = new java.util.Set[] { };
    org.apache.commons.collections4.set.CompositeSet var1 = new org.apache.commons.collections4.set.CompositeSet(var0);
    org.apache.commons.collections4.functors.UniquePredicate var2 = new org.apache.commons.collections4.functors.UniquePredicate();
    org.apache.commons.collections4.Predicate[] var3 = new org.apache.commons.collections4.Predicate[] { var2};
    java.lang.Object[] var4 = var1.toArray((java.lang.Object[])var3);
    org.apache.commons.collections4.Transformer var5 = org.apache.commons.collections4.TransformerUtils.stringValueTransformer();
    org.apache.commons.collections4.comparators.TransformingComparator var6 = new org.apache.commons.collections4.comparators.TransformingComparator(var5);
    org.apache.commons.collections4.bidimap.DualTreeBidiMap var7 = new org.apache.commons.collections4.bidimap.DualTreeBidiMap((java.util.Comparator)var6, (java.util.Comparator)var6);
    org.apache.commons.collections4.Transformer[] var8 = new org.apache.commons.collections4.Transformer[] { var5};
    org.apache.commons.collections4.functors.SwitchTransformer var9 = new org.apache.commons.collections4.functors.SwitchTransformer(var3, var8, var5);
    java.lang.Object var10 = var9.transform((java.lang.Object)var7);

  }
}
