import junit.framework.*;

public class Report5 extends TestCase {
  // Three more cases with a NullPointerException, perhaps related to
  // bug 513: https://issues.apache.org/jira/browse/COLLECTIONS-513 
  // should probably throw IllegalArgumentException as the map is empty?

  public void test42() throws Throwable {

    org.apache.commons.collections4.bidimap.DualTreeBidiMap var0 = new org.apache.commons.collections4.bidimap.DualTreeBidiMap();
    java.util.SortedMap var1 = org.apache.commons.collections4.MapUtils.synchronizedSortedMap((java.util.SortedMap)var0);
    org.apache.commons.collections4.Predicate var3 = org.apache.commons.collections4.PredicateUtils.equalPredicate((java.lang.Object)(byte)100);
    org.apache.commons.collections4.Predicate var4 = org.apache.commons.collections4.functors.NullPredicate.nullPredicate();
    java.util.SortedMap var5 = org.apache.commons.collections4.MapUtils.predicatedSortedMap((java.util.SortedMap)var1, (org.apache.commons.collections4.Predicate)var3, (org.apache.commons.collections4.Predicate)var4);
    org.apache.commons.collections4.Closure var6 = org.apache.commons.collections4.ClosureUtils.switchMapClosure((java.util.Map)var5);

  }

  // does not compile w/ Java 6
  public void test53() throws Throwable {

    org.apache.commons.collections4.bidimap.TreeBidiMap var0 = new org.apache.commons.collections4.bidimap.TreeBidiMap();
    java.lang.Comparable var3 = var0.put((java.lang.Comparable)512, (java.lang.Comparable)(-28.046383f));
    org.apache.commons.collections4.Factory var5 = org.apache.commons.collections4.FactoryUtils.constantFactory((java.lang.Object)(short)1);
    org.apache.commons.collections4.map.DefaultedMap var6 = org.apache.commons.collections4.map.DefaultedMap.defaultedMap((java.util.Map)var0, (org.apache.commons.collections4.Factory)var5);
    org.apache.commons.collections4.Closure var7 = org.apache.commons.collections4.ClosureUtils.switchClosure((java.util.Map)var6);

  }

  // another case that should probably throw IllegalArgumentException
  public void test61() throws Throwable {

    org.apache.commons.collections4.bidimap.TreeBidiMap var0 = new org.apache.commons.collections4.bidimap.TreeBidiMap();
    java.lang.Comparable var2 = var0.put((java.lang.Comparable)"]={\n", (java.lang.Comparable)"]={\n");
    org.apache.commons.collections4.Transformer var3 = org.apache.commons.collections4.functors.SwitchTransformer.switchTransformer((java.util.Map)var0);

  }

}
