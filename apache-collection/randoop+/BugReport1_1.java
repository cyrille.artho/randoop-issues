import junit.framework.*;

public class BugReport1_1 extends TestCase {

  // new case of hashCode vs equals?
  public void test14() throws Throwable {
    org.apache.commons.collections4.bidimap.DualHashBidiMap var1 = new org.apache.commons.collections4.bidimap.DualHashBidiMap();
    org.apache.commons.collections4.splitmap.AbstractIterableGetMapDecorator var2 = new org.apache.commons.collections4.splitmap.AbstractIterableGetMapDecorator((java.util.Map)var1);
    org.apache.commons.collections4.map.PassiveExpiringMap var4 = new org.apache.commons.collections4.map.PassiveExpiringMap();
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var3 and var5.", var2.equals(var4) == var4.equals(var2));

  }

}
