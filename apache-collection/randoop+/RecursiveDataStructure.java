import junit.framework.*;

public class Ignore extends TestCase {

  public void test9() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test9"); }

    java.util.Comparator var0 = org.apache.commons.collections4.ComparatorUtils.<java.lang.Comparable>naturalComparator();
    org.apache.commons.collections4.iterators.CollatingIterator var1 = new org.apache.commons.collections4.iterators.CollatingIterator((java.util.Comparator)var0);
    var1.addIterator((java.util.Iterator)var1);
    java.lang.Object var3 = var1.next();

  }

  public void test12() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test12"); }


    org.apache.commons.collections4.map.Flat3Map var0 = new org.apache.commons.collections4.map.Flat3Map();
    org.apache.commons.collections4.map.PassiveExpiringMap var1 = new org.apache.commons.collections4.map.PassiveExpiringMap();
    java.lang.Object var2 = var1.put((java.lang.Object)var1, (java.lang.Object)var0);
    var0.putAll((java.util.Map)var1);

  }

  public void test20() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test20"); }


    org.apache.commons.collections4.set.ListOrderedSet var0 = new org.apache.commons.collections4.set.ListOrderedSet();
    boolean var1 = var0.add((java.lang.Object)var0);

    //org.apache.commons.collections4.ArrayStack
    //@deprecated
    org.apache.commons.collections4.ArrayStack var2 = new org.apache.commons.collections4.ArrayStack();
    java.util.Collection var3 = org.apache.commons.collections4.CollectionUtils.intersection((java.lang.Iterable)var0, (java.lang.Iterable)var2);

  }

  public void test22() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test22"); }


    org.apache.commons.collections4.map.SingletonMap var1 = new org.apache.commons.collections4.map.SingletonMap((java.lang.Object)"The current criterion '", (java.lang.Object)"The current criterion '");
    org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap var2 = new org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap((java.util.Map)var1);
    org.apache.commons.collections4.map.StaticBucketMap var4 = new org.apache.commons.collections4.map.StaticBucketMap(3);
    java.lang.Object var5 = var4.put((java.lang.Object)var4, (java.lang.Object)var4);
    org.apache.commons.collections4.map.SingletonMap var6 = new org.apache.commons.collections4.map.SingletonMap((java.util.Map)var4);
    java.lang.Object var7 = var2.remove((java.lang.Object)var6);

  }

  public void test24() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test24"); }


    org.apache.commons.collections4.map.CompositeMap var0 = new org.apache.commons.collections4.map.CompositeMap();
    org.apache.commons.collections4.Transformer var1 = org.apache.commons.collections4.TransformerUtils.instantiateTransformer();
    org.apache.commons.collections4.map.LazyMap var2 = org.apache.commons.collections4.map.LazyMap.lazyMap((java.util.Map)var0, (org.apache.commons.collections4.Transformer)var1);
    var0.addComposited((java.util.Map)var2);
    boolean var5 = var0.containsKey((java.lang.Object)"Closures must not be null");

  }

  public void test27() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test27"); }


    org.apache.commons.collections4.map.LinkedMap var1 = new org.apache.commons.collections4.map.LinkedMap(128);
    org.apache.commons.collections4.set.MapBackedSet var2 = org.apache.commons.collections4.set.MapBackedSet.mapBackedSet((java.util.Map)var1);
    java.util.Collection var3 = org.apache.commons.collections4.CollectionUtils.synchronizedCollection((java.util.Collection)var2);
    boolean var4 = var3.add((java.lang.Object)var3);
    boolean var6 = var3.equals((java.lang.Object)"RootEntry(");
    
    // Checks the contract:  var2.equals(var2)
    assertTrue("Contract failed: var2.equals(var2)", var2.equals(var2));

  }

  public void test29() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test29"); }


    org.apache.commons.collections4.iterators.IteratorChain var0 = new org.apache.commons.collections4.iterators.IteratorChain();
    var0.addIterator((java.util.Iterator)var0);
    java.lang.Object var3 = org.apache.commons.collections4.CollectionUtils.get((java.util.Iterator)var0, 61);

  }

  public void test30() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test30"); }


    org.apache.commons.collections4.collection.CompositeCollection var0 = new org.apache.commons.collections4.collection.CompositeCollection();
    var0.addComposited((java.util.Collection)var0);
    //org.apache.commons.collections4.ArrayStack
    //@deprecated
    org.apache.commons.collections4.ArrayStack var3 = new org.apache.commons.collections4.ArrayStack(91);
    java.util.List var4 = org.apache.commons.collections4.ListUtils.synchronizedList((java.util.List)var3);
    org.apache.commons.collections4.list.FixedSizeList var5 = org.apache.commons.collections4.list.FixedSizeList.fixedSizeList((java.util.List)var4);
    java.util.ListIterator var6 = var5.listIterator();
    java.lang.Object[] var7 = new java.lang.Object[] { var6};
    java.lang.Object[] var8 = var0.toArray(var7);

  }

  public void test32() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test32"); }


    org.apache.commons.collections4.set.ListOrderedSet var0 = new org.apache.commons.collections4.set.ListOrderedSet();
    boolean var1 = var0.add((java.lang.Object)var0);
    int var2 = org.apache.commons.collections4.SetUtils.hashCodeForSet((java.util.Collection)var0);

  }

  public void test34() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test34"); }


    org.apache.commons.collections4.iterators.FilterListIterator var0 = new org.apache.commons.collections4.iterators.FilterListIterator();
    var0.setListIterator((java.util.ListIterator)var0);
    java.lang.Object var2 = var0.next();

  }

  public void test36() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test36"); }


    org.apache.commons.collections4.iterators.IteratorChain var0 = new org.apache.commons.collections4.iterators.IteratorChain();
    java.util.Iterator var1 = org.apache.commons.collections4.iterators.EmptyIterator.emptyIterator();
    org.apache.commons.collections4.iterators.IteratorChain var2 = new org.apache.commons.collections4.iterators.IteratorChain((java.util.Iterator)var0, (java.util.Iterator)var1);
    var0.addIterator((java.util.Iterator)var0);
    boolean var4 = var2.hasNext();

  }

  public void test37() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test37"); }


    org.apache.commons.collections4.collection.CompositeCollection var0 = new org.apache.commons.collections4.collection.CompositeCollection();
    var0.addComposited((java.util.Collection)var0);
    java.lang.Object[] var2 = var0.toArray();

  }

  public void test38() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test38"); }


    org.apache.commons.collections4.iterators.IteratorChain var0 = new org.apache.commons.collections4.iterators.IteratorChain();
    var0.addIterator((java.util.Iterator)var0);
    java.lang.Object var2 = var0.next();

  }

  public void test39() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test39"); }


    org.apache.commons.collections4.collection.CompositeCollection var0 = new org.apache.commons.collections4.collection.CompositeCollection();
    var0.addComposited((java.util.Collection)var0, (java.util.Collection)var0);
    org.apache.commons.collections4.set.ListOrderedSet var2 = new org.apache.commons.collections4.set.ListOrderedSet();
    java.util.Set[] var3 = new java.util.Set[] { var2};
    org.apache.commons.collections4.set.CompositeSet var4 = new org.apache.commons.collections4.set.CompositeSet(var3);
    boolean var5 = var0.retainAll((java.util.Collection)var4);

  }

  public void test41() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test41"); }


    org.apache.commons.collections4.comparators.ComparatorChain var0 = new org.apache.commons.collections4.comparators.ComparatorChain();
    var0.addComparator((java.util.Comparator)var0, false);
    java.util.StringTokenizer var5 = new java.util.StringTokenizer("");
    int var6 = var0.compare((java.lang.Object)1396882132138L, (java.lang.Object)var5);

  }

  public void test45() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test45"); }


    org.apache.commons.collections4.set.CompositeSet var0 = new org.apache.commons.collections4.set.CompositeSet();
    var0.addComposited((java.util.Set)var0);
    boolean var3 = var0.contains((java.lang.Object)"No Collection associated with this Iterator=null");

  }

  public void test46() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test46"); }


    org.apache.commons.collections4.comparators.ComparatorChain var0 = new org.apache.commons.collections4.comparators.ComparatorChain();
    var0.addComparator((java.util.Comparator)var0, false);
    java.util.Comparator var3 = org.apache.commons.collections4.ComparatorUtils.nullHighComparator((java.util.Comparator)var0);
    int var6 = var3.compare((java.lang.Object)1.0d, (java.lang.Object)"Entry.after=null, header.after");

  }

  public void test49() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test49"); }


    org.apache.commons.collections4.list.NodeCachingLinkedList var1 = new org.apache.commons.collections4.list.NodeCachingLinkedList(2);
    org.apache.commons.collections4.list.SetUniqueList var2 = org.apache.commons.collections4.list.SetUniqueList.setUniqueList((java.util.List)var1);
    java.util.Collection[] var3 = new java.util.Collection[] { var2};
    org.apache.commons.collections4.collection.CompositeCollection var4 = new org.apache.commons.collections4.collection.CompositeCollection(var3);
    var4.addComposited((java.util.Collection)var4);
    boolean var6 = var4.isEmpty();

  }

  public void test51() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test51"); }


    org.apache.commons.collections4.set.CompositeSet var0 = new org.apache.commons.collections4.set.CompositeSet();
    var0.addComposited((java.util.Set)var0);
    org.apache.commons.collections4.map.ReferenceMap var2 = new org.apache.commons.collections4.map.ReferenceMap();
    java.util.Set var3 = var2.entrySet();
    java.util.Set[] var4 = new java.util.Set[] { var3};
    var0.addComposited(var4);

  }

  public void test52() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test52"); }


    org.apache.commons.collections4.bag.HashBag var0 = new org.apache.commons.collections4.bag.HashBag();
    org.apache.commons.collections4.bag.SynchronizedBag var1 = org.apache.commons.collections4.bag.SynchronizedBag.synchronizedBag((org.apache.commons.collections4.Bag)var0);
    boolean var3 = var1.add((java.lang.Object)var1, 4);
    org.apache.commons.collections4.Predicate var4 = org.apache.commons.collections4.functors.UniquePredicate.uniquePredicate();
    org.apache.commons.collections4.Predicate var5 = org.apache.commons.collections4.PredicateUtils.nullIsTruePredicate((org.apache.commons.collections4.Predicate)var4);
    org.apache.commons.collections4.Predicate var6 = org.apache.commons.collections4.functors.NullIsExceptionPredicate.nullIsExceptionPredicate((org.apache.commons.collections4.Predicate)var5);
    org.apache.commons.collections4.Predicate[] var7 = new org.apache.commons.collections4.Predicate[] { var6};
    java.lang.Object[] var8 = var1.toArray((java.lang.Object[])var7);

  }

  public void test54() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test54"); }


    org.apache.commons.collections4.map.ReferenceMap var0 = new org.apache.commons.collections4.map.ReferenceMap();
    java.util.Map[] var1 = new java.util.Map[] { var0};
    org.apache.commons.collections4.map.CompositeMap var2 = new org.apache.commons.collections4.map.CompositeMap(var1);
    var2.addComposited((java.util.Map)var2);
    java.lang.Object var5 = var2.get((java.lang.Object)"MultiKey[\uFFFF, true, InstantiateTransformer:XInstantiationException]");

  }

  public void test56() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test56"); }


    org.apache.commons.collections4.iterators.FilterListIterator var0 = new org.apache.commons.collections4.iterators.FilterListIterator();
    org.apache.commons.collections4.iterators.FilterListIterator var1 = new org.apache.commons.collections4.iterators.FilterListIterator((java.util.ListIterator)var0);
    var0.setListIterator((java.util.ListIterator)var0);
    boolean var3 = var1.hasNext();

  }

  public void test57() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test57"); }


    org.apache.commons.collections4.set.ListOrderedSet var0 = new org.apache.commons.collections4.set.ListOrderedSet();
    org.apache.commons.collections4.Predicate var1 = org.apache.commons.collections4.functors.NotNullPredicate.notNullPredicate();
    org.apache.commons.collections4.Transformer var2 = org.apache.commons.collections4.functors.CloneTransformer.cloneTransformer();
    org.apache.commons.collections4.Transformer var3 = org.apache.commons.collections4.TransformerUtils.switchTransformer((org.apache.commons.collections4.Predicate)var1, (org.apache.commons.collections4.Transformer)var2, (org.apache.commons.collections4.Transformer)var2);
    java.util.Set var4 = org.apache.commons.collections4.set.TransformedSet.transformedSet((java.util.Set)var0, (org.apache.commons.collections4.Transformer)var3);
    java.util.Set[] var5 = new java.util.Set[] { var4};
    org.apache.commons.collections4.set.CompositeSet var6 = new org.apache.commons.collections4.set.CompositeSet(var5);
    var6.addComposited((java.util.Set)var6);
    java.util.List var9 = var6.getSets();
    
    // Checks the contract:  var6.equals(var6)
    assertTrue("Contract failed: var6.equals(var6)", var6.equals(var6));

  }

  public void test59() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test59"); }


    org.apache.commons.collections4.map.PassiveExpiringMap var0 = new org.apache.commons.collections4.map.PassiveExpiringMap();
    java.util.Map[] var1 = new java.util.Map[] { var0};
    org.apache.commons.collections4.map.CompositeMap var2 = new org.apache.commons.collections4.map.CompositeMap(var1);
    var2.addComposited((java.util.Map)var2);
    boolean var6 = var2.equals((java.lang.Object)2793764253664L);
    
    // Checks the contract:  var2.equals(var2)
    assertTrue("Contract failed: var2.equals(var2)", var2.equals(var2));
    
    // Checks the contract:  var4.equals(var4)

  }

}
