import junit.framework.*;

public class Ignore extends TestCase {

  public void test1() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test1"); }


    org.apache.commons.collections4.queue.CircularFifoQueue var0 = new org.apache.commons.collections4.queue.CircularFifoQueue();
    java.util.Queue var1 = org.apache.commons.collections4.QueueUtils.unmodifiableQueue((java.util.Queue)var0);
    boolean var2 = var0.add((java.lang.Object)var1);
    java.util.Iterator var4 = var0.iterator();
    java.lang.Object var5 = var0.remove();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var2 == true);
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var5);

  }

  public void test19() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test19"); }


    org.apache.commons.collections4.bidimap.DualTreeBidiMap var0 = new org.apache.commons.collections4.bidimap.DualTreeBidiMap();
    org.apache.commons.collections4.SortedBidiMap var1 = org.apache.commons.collections4.bidimap.UnmodifiableSortedBidiMap.unmodifiableSortedBidiMap((org.apache.commons.collections4.SortedBidiMap)var0);
    java.util.Set var2 = var1.entrySet();
    org.apache.commons.collections4.OrderedMapIterator var4 = var1.mapIterator();
    java.util.Comparator var5 = var1.valueComparator();
    java.util.Set var6 = var1.values();
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var2);
    
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNotNull(var6);

  }

  public void test28() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test28"); }


    java.util.Collection var0 = org.apache.commons.collections4.CollectionUtils.emptyCollection();
    org.apache.commons.collections4.iterators.PermutationIterator var1 = new org.apache.commons.collections4.iterators.PermutationIterator((java.util.Collection)var0);
    java.util.List var2 = var1.next();
    boolean var3 = var2.add((java.lang.Object)var2);
    org.apache.commons.collections4.list.NodeCachingLinkedList var4 = new org.apache.commons.collections4.list.NodeCachingLinkedList();
    java.util.List var5 = org.apache.commons.collections4.ListUtils.intersection((java.util.List)var2, (java.util.List)var4);

  }

  public void test60() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test60"); }


    org.apache.commons.collections4.list.TreeList var0 = new org.apache.commons.collections4.list.TreeList();
    org.apache.commons.collections4.list.SetUniqueList var1 = org.apache.commons.collections4.list.SetUniqueList.setUniqueList((java.util.List)var0);
    org.apache.commons.collections4.list.GrowthList var2 = new org.apache.commons.collections4.list.GrowthList();
    org.apache.commons.collections4.set.ListOrderedSet var3 = org.apache.commons.collections4.set.ListOrderedSet.listOrderedSet((java.util.List)var2);
    var2.add(32768, (java.lang.Object)var3);
    boolean var6 = var3.addAll(32768, (java.util.Collection)var3);
    boolean var7 = var1.removeAll((java.util.Collection)var3);

  }

}
