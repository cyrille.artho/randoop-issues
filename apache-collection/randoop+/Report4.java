import junit.framework.*;

public class Report4 extends TestCase {
  // NullPointerException because Map has only entry <null, null>.
  // However, javadoc states "A null input will return an empty
  // properties object." so (1) this should be clarified as it would
  // only apply to the map reference itself, not its contents, or (2)
  // an empty property object should be generated for null entries in
  // the map as well.
  public void test35() throws Throwable {
    org.apache.commons.collections4.map.SingletonMap var0 = new org.apache.commons.collections4.map.SingletonMap();
    java.util.Properties var1 = org.apache.commons.collections4.MapUtils.toProperties((java.util.Map)var0);

  }


}
