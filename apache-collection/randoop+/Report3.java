import junit.framework.*;

public class Report3 extends TestCase {

  // java.lang.ClassCastException: org.apache.commons.collections4.map.LazySortedMap cannot be cast to java.lang.String
  public void test33() throws Throwable {

    org.apache.commons.collections4.trie.PatriciaTrie var0 = new org.apache.commons.collections4.trie.PatriciaTrie();
    org.apache.commons.collections4.Transformer var1 = org.apache.commons.collections4.functors.ExceptionTransformer.exceptionTransformer();
    java.util.SortedMap var2 = org.apache.commons.collections4.MapUtils.lazySortedMap((java.util.SortedMap)var0, (org.apache.commons.collections4.Transformer)var1);
    java.util.SortedMap var3 = var2.tailMap((java.lang.Object)var2);
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var2 and var3.", var2.equals(var3) == var3.equals(var2));

  }

}
