import junit.framework.*;

public class Report1 extends TestCase {

  // new case of hashCode vs equals?
  public void test14() throws Throwable {
    org.apache.commons.collections4.bidimap.DualHashBidiMap var1 = new org.apache.commons.collections4.bidimap.DualHashBidiMap();
    org.apache.commons.collections4.splitmap.AbstractIterableGetMapDecorator var2 = new org.apache.commons.collections4.splitmap.AbstractIterableGetMapDecorator((java.util.Map)var1);
    org.apache.commons.collections4.map.PassiveExpiringMap var4 = new org.apache.commons.collections4.map.PassiveExpiringMap();
    
    // This assertion (symmetry of equals) fails 
    assertTrue("Contract failed: equals-symmetric on var3 and var5.", var2.equals(var4) == var4.equals(var2));

  }

  // previously found bug? hashCode vs equals
  // similar to test8 from plain Randoop
  public void test50() throws Throwable {

    org.apache.commons.collections4.Transformer var0 = org.apache.commons.collections4.TransformerUtils.nullTransformer();
    org.apache.commons.collections4.comparators.TransformingComparator var1 = new org.apache.commons.collections4.comparators.TransformingComparator((org.apache.commons.collections4.Transformer)var0);
    org.apache.commons.collections4.bidimap.TreeBidiMap var2 = new org.apache.commons.collections4.bidimap.TreeBidiMap();
    org.apache.commons.collections4.OrderedBidiMap var3 = org.apache.commons.collections4.bidimap.UnmodifiableOrderedBidiMap.unmodifiableOrderedBidiMap((org.apache.commons.collections4.OrderedBidiMap)var2);
    java.lang.Object[] var4 = new java.lang.Object[] { var3};
    org.apache.commons.collections4.comparators.FixedOrderComparator var5 = new org.apache.commons.collections4.comparators.FixedOrderComparator(var4);
    org.apache.commons.collections4.comparators.TransformingComparator var6 = new org.apache.commons.collections4.comparators.TransformingComparator((org.apache.commons.collections4.Transformer)var0, (java.util.Comparator)var5);
    
    // Checks the contract:  equals-hashcode on var1 and var6
    assertTrue("Contract failed: equals-hashcode on var1 and var6", var1.equals(var6) ? var1.hashCode() == var6.hashCode() : true);
    
    // Checks the contract:  equals-hashcode on var6 and var1
    assertTrue("Contract failed: equals-hashcode on var6 and var1", var6.equals(var1) ? var6.hashCode() == var1.hashCode() : true);

  }

}
