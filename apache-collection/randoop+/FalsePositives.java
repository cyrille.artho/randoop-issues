import junit.framework.*;

public class FalsePositives extends TestCase {

  // java.lang.NullPointerException: Key cannot be null
  public void test2() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test2"); }


    org.apache.commons.collections4.trie.PatriciaTrie var0 = new org.apache.commons.collections4.trie.PatriciaTrie();
    org.apache.commons.collections4.map.StaticBucketMap var2 = new org.apache.commons.collections4.map.StaticBucketMap(3);
    org.apache.commons.collections4.Transformer var3 = org.apache.commons.collections4.TransformerUtils.switchTransformer((java.util.Map)var2);
    org.apache.commons.collections4.Transformer var4 = org.apache.commons.collections4.functors.NOPTransformer.nopTransformer();
    java.util.SortedMap var5 = org.apache.commons.collections4.MapUtils.transformedSortedMap((java.util.SortedMap)var0, (org.apache.commons.collections4.Transformer)var3, (org.apache.commons.collections4.Transformer)var4);
    java.lang.Object var7 = var5.put((java.lang.Object)" entryIsHeader=_^", (java.lang.Object)" entryIsHeader=_^");

  }

  // java.lang.NullPointerException: Key must not be null
  public void test23() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test23"); }


    org.apache.commons.collections4.map.MultiKeyMap var0 = new org.apache.commons.collections4.map.MultiKeyMap();
    org.apache.commons.collections4.map.SingletonMap var1 = new org.apache.commons.collections4.map.SingletonMap();
    org.apache.commons.collections4.map.ListOrderedMap var2 = org.apache.commons.collections4.map.ListOrderedMap.listOrderedMap((java.util.Map)var1);
    var0.putAll((java.util.Map)var2);

  }

  // java.lang.NullPointerException: Attempted to add null object to queue
  public void test43() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test43"); }


    org.apache.commons.collections4.map.SingletonMap var0 = new org.apache.commons.collections4.map.SingletonMap();
    org.apache.commons.collections4.map.ListOrderedMap var1 = org.apache.commons.collections4.map.ListOrderedMap.listOrderedMap((java.util.Map)var0);
    java.util.Collection var2 = var1.values();
    org.apache.commons.collections4.queue.CircularFifoQueue var3 = new org.apache.commons.collections4.queue.CircularFifoQueue((java.util.Collection)var2);

  }

  // java.lang.NullPointerException: Attempted to add null object to queue
  public void test44() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test44"); }


    org.apache.commons.collections4.queue.CircularFifoQueue var1 = new org.apache.commons.collections4.queue.CircularFifoQueue(3);
    org.apache.commons.collections4.list.GrowthList var2 = new org.apache.commons.collections4.list.GrowthList(3);
    boolean var3 = var2.addAll(3, (java.util.Collection)var2);
    boolean var4 = var1.addAll((java.util.Collection)var2);

  }

  // java.lang.NullPointerException: Iterator must not be null
  public void test48() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test48"); }


    org.apache.commons.collections4.comparators.ReverseComparator var0 = new org.apache.commons.collections4.comparators.ReverseComparator();
    java.util.Comparator var1 = org.apache.commons.collections4.ComparatorUtils.nullHighComparator((java.util.Comparator)var0);
    org.apache.commons.collections4.map.SingletonMap var2 = new org.apache.commons.collections4.map.SingletonMap();
    org.apache.commons.collections4.map.ListOrderedMap var3 = org.apache.commons.collections4.map.ListOrderedMap.listOrderedMap((java.util.Map)var2);
    java.util.List var4 = var3.keyList();
    java.util.Iterator var5 = org.apache.commons.collections4.IteratorUtils.collatedIterator((java.util.Comparator)var1, (java.util.Collection)var4);

  }

  // probably fails because ReferenceMap does not allow null;
  // however, min is called on two valid strings.
  // I assume the transformer returns null because the strings are
  // not in the map, which is why the comparison fails.
  public void test55() throws Throwable {

    org.apache.commons.collections4.map.ReferenceMap var1 = new org.apache.commons.collections4.map.ReferenceMap();
    org.apache.commons.collections4.Transformer var2 = org.apache.commons.collections4.TransformerUtils.mapTransformer((java.util.Map)var1);
    java.util.Comparator var3 = org.apache.commons.collections4.ComparatorUtils.<java.lang.Comparable>naturalComparator();
    org.apache.commons.collections4.comparators.TransformingComparator var4 = new org.apache.commons.collections4.comparators.TransformingComparator((org.apache.commons.collections4.Transformer)var2, (java.util.Comparator)var3);
    java.lang.Object var5 = org.apache.commons.collections4.ComparatorUtils.min((java.lang.Object)"a", (java.lang.Object)"a", (java.util.Comparator)var4);

  }
  // java.lang.NullPointerException
  // due to nullTransformer returning null, which is then used by compare
  public void test58() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test58"); }


    org.apache.commons.collections4.Transformer var0 = org.apache.commons.collections4.TransformerUtils.nullTransformer();
    org.apache.commons.collections4.comparators.TransformingComparator var1 = new org.apache.commons.collections4.comparators.TransformingComparator((org.apache.commons.collections4.Transformer)var0);
    int var4 = var1.compare((java.lang.Object)2793764253669L, (java.lang.Object)"End index must not be less than start index.");

  }

}
