import junit.framework.*;

public class ContractNotEnforced extends TestCase {

  // case 1: TransformIterator
  public void test4() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test4"); }


    org.apache.commons.collections4.iterators.TransformIterator var0 = new org.apache.commons.collections4.iterators.TransformIterator();
    java.lang.Object var1 = var0.next();

  }

  public void test5() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test5"); }


    org.apache.commons.collections4.iterators.TransformIterator var0 = new org.apache.commons.collections4.iterators.TransformIterator();
    java.util.ListIterator var1 = org.apache.commons.collections4.IteratorUtils.toListIterator((java.util.Iterator)var0);
    java.util.Iterator var2 = org.apache.commons.collections4.IteratorUtils.pushbackIterator((java.util.Iterator)var1);
    java.lang.Object var3 = var2.next();

  }

  public void test6() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test6"); }


    org.apache.commons.collections4.iterators.TransformIterator var0 = new org.apache.commons.collections4.iterators.TransformIterator();
    java.util.ListIterator var1 = org.apache.commons.collections4.IteratorUtils.toListIterator((java.util.Iterator)var0);
    java.lang.Object var2 = var1.next();

  }

  public void test15() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test15"); }


    org.apache.commons.collections4.iterators.TransformIterator var0 = new org.apache.commons.collections4.iterators.TransformIterator();
    boolean var1 = var0.hasNext();

  }

  public void test17() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test17"); }


    org.apache.commons.collections4.iterators.TransformIterator var0 = new org.apache.commons.collections4.iterators.TransformIterator();
    java.util.ListIterator var1 = org.apache.commons.collections4.IteratorUtils.toListIterator((java.util.Iterator)var0);
    boolean var2 = var1.hasNext();

  }

  public void test25() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test25"); }


    org.apache.commons.collections4.iterators.TransformIterator var0 = new org.apache.commons.collections4.iterators.TransformIterator();
    var0.remove();

  }

  public void test40() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test40"); }


    org.apache.commons.collections4.iterators.TransformIterator var0 = new org.apache.commons.collections4.iterators.TransformIterator();
    java.util.List var2 = org.apache.commons.collections4.IteratorUtils.toList((java.util.Iterator)var0, 58);

  }

  // case 2: FilterIterator
  public void test7() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test7"); }


    org.apache.commons.collections4.iterators.FilterIterator var0 = new org.apache.commons.collections4.iterators.FilterIterator();
    java.lang.Object var1 = var0.next();

  }

  public void test16() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test16"); }


    java.util.ListIterator var1 = org.apache.commons.collections4.IteratorUtils.singletonListIterator((java.lang.Object)"Key cannot be null");
    org.apache.commons.collections4.iterators.FilterIterator var2 = new org.apache.commons.collections4.iterators.FilterIterator((java.util.Iterator)var1);
    boolean var3 = var2.hasNext();

  }

  public void test21() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test21"); }


    java.util.ListIterator var1 = org.apache.commons.collections4.IteratorUtils.singletonListIterator((java.lang.Object)"Key cannot be null");
    org.apache.commons.collections4.iterators.FilterIterator var2 = new org.apache.commons.collections4.iterators.FilterIterator((java.util.Iterator)var1);
    org.apache.commons.collections4.Transformer var3 = org.apache.commons.collections4.functors.ConstantTransformer.constantTransformer((java.lang.Object)"Key cannot be null");
    java.util.Queue var4 = org.apache.commons.collections4.QueueUtils.emptyQueue();
    java.util.Collection var5 = org.apache.commons.collections4.CollectionUtils.<java.lang.Object,java.lang.Object,java.util.Collection>collect((java.util.Iterator)var2, (org.apache.commons.collections4.Transformer)var3, (java.util.Collection)var4);

  }

  public void test47() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test47"); }


    org.apache.commons.collections4.iterators.FilterIterator var0 = new org.apache.commons.collections4.iterators.FilterIterator();
    var0.remove();

  }

  // case 3: EnumerationIterator
  public void test8() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test8"); }


    org.apache.commons.collections4.iterators.EnumerationIterator var0 = new org.apache.commons.collections4.iterators.EnumerationIterator();
    java.lang.Object var1 = var0.next();

  }

  public void test11() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test11"); }


    org.apache.commons.collections4.iterators.EnumerationIterator var0 = new org.apache.commons.collections4.iterators.EnumerationIterator();
    boolean var1 = var0.hasNext();

  }

  // case 4: IteratorEnumeration
  public void test10() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test10"); }


    org.apache.commons.collections4.iterators.IteratorEnumeration var0 = new org.apache.commons.collections4.iterators.IteratorEnumeration();
    java.util.List var1 = org.apache.commons.collections4.EnumerationUtils.toList((java.util.Enumeration)var0);

  }

  public void test13() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test13"); }


    org.apache.commons.collections4.list.NodeCachingLinkedList var0 = new org.apache.commons.collections4.list.NodeCachingLinkedList();
    org.apache.commons.collections4.iterators.IteratorEnumeration var1 = new org.apache.commons.collections4.iterators.IteratorEnumeration();
    boolean var2 = org.apache.commons.collections4.CollectionUtils.addAll((java.util.Collection)var0, (java.util.Enumeration)var1);

  }

  public void test26() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test26"); }


    org.apache.commons.collections4.iterators.IteratorEnumeration var0 = new org.apache.commons.collections4.iterators.IteratorEnumeration();
    boolean var1 = var0.hasMoreElements();

  }

  public void test31() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test31"); }


    org.apache.commons.collections4.iterators.IteratorEnumeration var0 = new org.apache.commons.collections4.iterators.IteratorEnumeration();
    java.lang.Object var1 = var0.nextElement();

  }


  // case 5: FilterListIterator
  public void test34() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test34"); }


    org.apache.commons.collections4.iterators.FilterListIterator var0 = new org.apache.commons.collections4.iterators.FilterListIterator();
    var0.setListIterator((java.util.ListIterator)var0);
    java.lang.Object var2 = var0.next();

  }

  public void test56() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test56"); }


    org.apache.commons.collections4.iterators.FilterListIterator var0 = new org.apache.commons.collections4.iterators.FilterListIterator();
    org.apache.commons.collections4.iterators.FilterListIterator var1 = new org.apache.commons.collections4.iterators.FilterListIterator((java.util.ListIterator)var0);
    var0.setListIterator((java.util.ListIterator)var0);
    boolean var3 = var1.hasNext();

  }
}
