import junit.framework.*;

public class RandoopTest0 extends TestCase {
  // same as issue report from plain Randoop (no "?" in PreparedStatement)
  public void test1() throws Throwable {
    com.sqlmagic.tinysql.dbfFileDriver var0 = new com.sqlmagic.tinysql.dbfFileDriver();
    com.sqlmagic.tinysql.tinySQLConnection var2 = var0.getConnection("Return ", "Return ", (java.sql.Driver)var0);
    com.sqlmagic.tinysql.tinySQLPreparedStatement var3 = new com.sqlmagic.tinysql.tinySQLPreparedStatement((com.sqlmagic.tinysql.tinySQLConnection)var2, "Return ");

  }

  // same as issue found by plain Randoop (incomplete init.)
  public void test2() throws Throwable {
    com.sqlmagic.tinysql.dbfFile var0 = new com.sqlmagic.tinysql.dbfFile();
    com.sqlmagic.tinysql.tsResultSet var1 = var0.sqlexec();
  }

  // same as above (no "?" in PreparedStatement)
  public void test3() throws Throwable {
    com.sqlmagic.tinysql.textFileDriver var0 = new com.sqlmagic.tinysql.textFileDriver();
    com.sqlmagic.tinysql.tinySQLConnection var2 = var0.getConnection("MODIFY", "MODIFY", (java.sql.Driver)var0);
    java.sql.PreparedStatement var3 = var2.prepareStatement("MODIFY");
  }

  // same as issue found by plain Randoop (invalid XML is not rejected)
  public void test4() throws Throwable {
    com.sqlmagic.tinysql.tsResultSet var2 = new com.sqlmagic.tinysql.tsResultSet();
    java.util.Vector var3 = var2.getTables();
    com.sqlmagic.tinysql.SimpleXMLTag var4 = new com.sqlmagic.tinysql.SimpleXMLTag("tinySQL does not support updateTimestamp.", 1001, var3);
    java.lang.String var5 = var4.toString();
  }

  // same as issue found by plain Randoop (invalid XML is not rejected)
  public void test5() throws Throwable {
    com.sqlmagic.tinysql.tsResultSet var2 = new com.sqlmagic.tinysql.tsResultSet();
    java.util.Vector var3 = var2.getTables();
    com.sqlmagic.tinysql.SimpleXMLTag var4 = new com.sqlmagic.tinysql.SimpleXMLTag("tinySQL does not support updateTimestamp.", 1001, var3);
    java.lang.String var5 = com.sqlmagic.tinysql.UtilString.actionToString((java.util.Hashtable)var4);
  }

  // same as issue found by plain Randoop (invalid column yields NPE)
  public void test6() throws Throwable {
    com.sqlmagic.tinysql.textFileDriver var2 = new com.sqlmagic.tinysql.textFileDriver();
    com.sqlmagic.tinysql.dbfFileConnection var3 = new com.sqlmagic.tinysql.dbfFileConnection("0000", "tinySQLXdoesXnotXsupportXgetTypeMap.", (java.sql.Driver)var2);
    com.sqlmagic.tinysql.dbfFileDatabaseMetaData var4 = new com.sqlmagic.tinysql.dbfFileDatabaseMetaData((java.sql.Connection)var3);
    java.sql.ResultSet var5 = var4.getColumns("0000", "0000", "0000", "0000");
    byte var7 = var5.getByte(13);

  }

  // new issue (Report 5)
  public void test7() throws Throwable {

    com.sqlmagic.tinysql.SimpleXMLTag var1 = new com.sqlmagic.tinysql.SimpleXMLTag(" in this directory.");
    com.sqlmagic.tinysql.tinySQLWhere var2 = new com.sqlmagic.tinysql.tinySQLWhere(" in this directory.", (java.util.Hashtable)var1);

  }

  // same issue as found by plain Randoop (getMoreResults)
  public void test8() throws Throwable {
    com.sqlmagic.tinysql.SimpleXMLTag var1 = new com.sqlmagic.tinysql.SimpleXMLTag("\\does\\not");
    com.sqlmagic.tinysql.tinySQLWhere var2 = new com.sqlmagic.tinysql.tinySQLWhere("\\does\\not", (java.util.Hashtable)var1);
    com.sqlmagic.tinysql.dbfFileDriver var3 = new com.sqlmagic.tinysql.dbfFileDriver();
    com.sqlmagic.tinysql.tinySQLConnection var4 = var3.getConnection("\\does\\not", "\\does\\not", (java.sql.Driver)var3);
    com.sqlmagic.tinysql.tinySQL var5 = var4.get_tinySQL();
    com.sqlmagic.tinysql.tsResultSet var6 = new com.sqlmagic.tinysql.tsResultSet(var2, (com.sqlmagic.tinysql.tinySQL)var5);
    boolean var8 = var6.getMoreResults(19, 19);

  }

}
