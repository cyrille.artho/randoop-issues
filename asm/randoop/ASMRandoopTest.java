import junit.framework.*;

public class ASMRandoopTest extends TestCase {
  // NullPointerException: incompletely initialized data
  public void test1() throws Throwable {
    org.objectweb.asm.tree.analysis.SimpleVerifier var0 = new org.objectweb.asm.tree.analysis.SimpleVerifier();
    org.objectweb.asm.tree.analysis.Analyzer var1 = new org.objectweb.asm.tree.analysis.Analyzer((org.objectweb.asm.tree.analysis.Interpreter)var0);
    java.util.List var3 = var1.getHandlers(0);
  }

  // NullPointerException: incompletely initialized data
  public void test2() throws Throwable {
    org.objectweb.asm.Label var2 = new org.objectweb.asm.Label();
    new org.objectweb.asm.util.ASMifier().visitLineNumber(0, var2);
  }

  // NullPointerException: incompletely initialized data
  public void test3() throws Throwable {
    org.objectweb.asm.ClassWriter var2 = new org.objectweb.asm.ClassWriter(0);
    new org.objectweb.asm.tree.ClassNode().accept((org.objectweb.asm.ClassVisitor)var2);
  }

  // NullPointerException: complex sequence of insert/remove/add
  public void test4() throws Throwable {
    org.objectweb.asm.tree.InsnList var0 = new org.objectweb.asm.tree.InsnList();
    org.objectweb.asm.tree.IntInsnNode var2 = new org.objectweb.asm.tree.IntInsnNode(0, 0);
    var0.insert((org.objectweb.asm.tree.AbstractInsnNode)var2);
    org.objectweb.asm.tree.InsnList var4 = new org.objectweb.asm.tree.InsnList();
    var4.remove((org.objectweb.asm.tree.AbstractInsnNode)var2);
    var0.add(var4);
  }

  // public void test5() // bad variant of test6 (generics problem)

  // NullPointerException: incompletely initialized data
  public void test6() throws Throwable {
    org.objectweb.asm.tree.analysis.Frame var1 = new org.objectweb.asm.tree.analysis.Frame(1, 1);
    boolean[] var3 = new boolean[] { false};
    // 0-length array produces ArrayIndexOutOfBoundsException
    var1.merge(var1, var3);
  }

  // NullPointerException: incompletely initialized data
  public void test8() throws Throwable {
    org.objectweb.asm.ClassWriter var1 = new org.objectweb.asm.ClassWriter(3);
    byte[] var5 = var1.toByteArray();
    org.objectweb.asm.ClassReader var6 = new org.objectweb.asm.ClassReader(var5);
    org.objectweb.asm.commons.SerialVersionUIDAdder var7 = new org.objectweb.asm.commons.SerialVersionUIDAdder((org.objectweb.asm.ClassVisitor)var1);
    var6.accept((org.objectweb.asm.ClassVisitor)var7, 3);
  }

  // NullPointerException: incompletely initialized data
  public void test10() throws Throwable {
    org.objectweb.asm.tree.analysis.SourceInterpreter var0 = new org.objectweb.asm.tree.analysis.SourceInterpreter();
    org.objectweb.asm.tree.analysis.Analyzer var1 = new org.objectweb.asm.tree.analysis.Analyzer((org.objectweb.asm.tree.analysis.Interpreter)var0);
    org.objectweb.asm.tree.MethodNode var3 = new org.objectweb.asm.tree.MethodNode();
    org.objectweb.asm.tree.analysis.Frame[] var4 = var1.analyze("", var3);
  }
}
