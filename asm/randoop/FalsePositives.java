import junit.framework.*;

public class FalsePositives extends TestCase {
  // MethodNode: default c'tor is documented as creating an uninit'd instance

  //public void test7() // same as test11

  // NullPointerException: incompletely initialized data, similar to test11
  public void test9() throws Throwable {
    org.objectweb.asm.TypePath var3 = org.objectweb.asm.TypePath.fromString("");
    new org.objectweb.asm.tree.MethodNode().visitTryCatchAnnotation(0, var3, "", false);
  }

  // NullPointerException: incompletely initialized data
  public void test11() throws Throwable {
    new org.objectweb.asm.tree.MethodNode().visitParameterAnnotation(0, "", true);
  }

  // NullPointerException: incompletely initialized data, similar to test11
  public void test12() throws Throwable {
    org.objectweb.asm.TypePath var4 = org.objectweb.asm.TypePath.fromString("");
    new org.objectweb.asm.tree.MethodNode().visitInsnAnnotation(1, var4, "", false);
  }

  //public void test13() // same as test11
}
