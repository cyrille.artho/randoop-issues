import junit.framework.*;

public class Report extends TestCase {
	public void test1() throws Throwable {

		org.apache.commons.collections.primitives.FloatList var3 = org.apache.commons.collections.primitives.FloatCollections
				.singletonFloatList(100.0f);
		org.apache.commons.collections.primitives.LongList var5 = org.apache.commons.collections.primitives.LongCollections
				.singletonLongList(100L);
		java.util.List var6 = org.apache.commons.collections.primitives.adapters.LongListList
				.wrap((org.apache.commons.collections.primitives.LongList) var5);
		org.apache.commons.collections.primitives.adapters.ListFloatList var7 = new org.apache.commons.collections.primitives.adapters.ListFloatList(
				(java.util.List) var6);
		org.apache.commons.collections.primitives.FloatList var8 = org.apache.commons.collections.primitives.FloatCollections
				.unmodifiableFloatList((org.apache.commons.collections.primitives.FloatList) var7);

		// Checks the contract: equals-hashcode on var8 and var3
		assertTrue("Contract failed: equals-hashcode on var8 and var3",
				var8.equals(var3) ? var8.hashCode() == var3.hashCode() : true);


	}

	public void test2() throws Throwable {

		org.apache.commons.collections.primitives.LongList var1 = org.apache.commons.collections.primitives.LongCollections
				.singletonLongList(100L);
		java.util.List var2 = org.apache.commons.collections.primitives.adapters.LongListList
				.wrap((org.apache.commons.collections.primitives.LongList) var1);
		org.apache.commons.collections.primitives.adapters.ListDoubleList var3 = new org.apache.commons.collections.primitives.adapters.ListDoubleList(
				(java.util.List) var2);
		org.apache.commons.collections.primitives.ArrayDoubleList var5 = new org.apache.commons.collections.primitives.ArrayDoubleList(
				(org.apache.commons.collections.primitives.DoubleCollection) var3);

		// Checks the contract: equals-hashcode on var6 and var4
		assertTrue("Contract failed: equals-hashcode on var6 and var4",
				var5.equals(var3) ? var5.hashCode() == var3.hashCode() : true);

	}
}
