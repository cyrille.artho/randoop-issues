import junit.framework.*;

public class FalsePositives extends TestCase {

	public void test3() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test1");
		}

		org.apache.commons.collections.primitives.LongList var1 = org.apache.commons.collections.primitives.LongCollections
				.singletonLongList(100L);
		java.util.List var2 = org.apache.commons.collections.primitives.adapters.LongListList
				.wrap((org.apache.commons.collections.primitives.LongList) var1);
		org.apache.commons.collections.primitives.adapters.ListCharList var3 = new org.apache.commons.collections.primitives.adapters.ListCharList(
				(java.util.List) var2);
		org.apache.commons.collections.primitives.CharList var4 = org.apache.commons.collections.primitives.CharCollections
				.unmodifiableCharList((org.apache.commons.collections.primitives.CharList) var3);

		// Checks the contract: var4.equals(var4)
		assertTrue("Contract failed: var4.equals(var4)", var4.equals(var4));

	}

	public void test4() throws Throwable {

		if (debug) {
			System.out.println();
			System.out.print("RandoopTest0.test2");
		}

		org.apache.commons.collections.primitives.CharList var1 = org.apache.commons.collections.primitives.CharCollections
				.singletonCharList('#');
		java.util.List var2 = org.apache.commons.collections.primitives.adapters.CharListList
				.wrap((org.apache.commons.collections.primitives.CharList) var1);
		org.apache.commons.collections.primitives.ShortList var3 = org.apache.commons.collections.primitives.adapters.ListShortList
				.wrap((java.util.List) var2);
		org.apache.commons.collections.primitives.ShortList var4 = org.apache.commons.collections.primitives.decorators.UnmodifiableShortList
				.wrap((org.apache.commons.collections.primitives.ShortList) var3);

		// Checks the contract: var4.equals(var4)
		assertTrue("Contract failed: var4.equals(var4)", var4.equals(var4));

	}


}
