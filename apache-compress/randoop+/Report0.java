import junit.framework.*;

public class Report0 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test1"); }


    org.apache.commons.compress.archivers.zip.UnrecognizedExtraField var0 = new org.apache.commons.compress.archivers.zip.UnrecognizedExtraField();
    org.apache.commons.compress.archivers.zip.ZipShort var1 = var0.getLocalFileDataLength();

  }

  public void test2() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test2"); }


    org.apache.commons.compress.archivers.zip.UnrecognizedExtraField var0 = new org.apache.commons.compress.archivers.zip.UnrecognizedExtraField();
    org.apache.commons.compress.archivers.zip.ZipShort var1 = var0.getCentralDirectoryLength();

  }

  public void test3() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test3"); }


    org.apache.commons.compress.archivers.zip.UnicodeCommentExtraField var0 = new org.apache.commons.compress.archivers.zip.UnicodeCommentExtraField();
    org.apache.commons.compress.archivers.zip.ZipShort var1 = var0.getCentralDirectoryLength();

  }

  public void test4() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test4"); }


    org.apache.commons.compress.archivers.zip.UnicodeCommentExtraField var0 = new org.apache.commons.compress.archivers.zip.UnicodeCommentExtraField();
    org.apache.commons.compress.archivers.zip.ZipShort var1 = var0.getLocalFileDataLength();

  }

  public void test5() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test5"); }


    org.apache.commons.compress.archivers.jar.JarArchiveEntry var1 = new org.apache.commons.compress.archivers.jar.JarArchiveEntry("hi!");
    org.apache.commons.compress.archivers.zip.X7875_NewUnix var2 = new org.apache.commons.compress.archivers.zip.X7875_NewUnix();
    org.apache.commons.compress.archivers.zip.ZipShort var3 = var2.getLocalFileDataLength();
    org.apache.commons.compress.archivers.zip.ZipExtraField var4 = org.apache.commons.compress.archivers.zip.ExtraFieldUtils.createExtraField(var3);
    org.apache.commons.compress.archivers.zip.ZipExtraField[] var5 = new org.apache.commons.compress.archivers.zip.ZipExtraField[] { var4};
    var1.setExtraFields(var5);

  }

  public void test6() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test6"); }


    org.apache.commons.compress.archivers.jar.JarArchiveEntry var1 = new org.apache.commons.compress.archivers.jar.JarArchiveEntry("hi!");
    org.apache.commons.compress.archivers.zip.UnicodePathExtraField var2 = new org.apache.commons.compress.archivers.zip.UnicodePathExtraField();
    var1.addAsFirstExtraField((org.apache.commons.compress.archivers.zip.ZipExtraField)var2);

  }

  public void test7() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test7"); }


    org.apache.commons.compress.archivers.jar.JarArchiveEntry var1 = new org.apache.commons.compress.archivers.jar.JarArchiveEntry("hi!");
    org.apache.commons.compress.archivers.zip.UnicodeCommentExtraField var2 = new org.apache.commons.compress.archivers.zip.UnicodeCommentExtraField();
    var1.addExtraField((org.apache.commons.compress.archivers.zip.ZipExtraField)var2);

  }

  public void test8() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test8"); }


    org.apache.commons.compress.archivers.zip.X5455_ExtendedTimestamp var0 = new org.apache.commons.compress.archivers.zip.X5455_ExtendedTimestamp();
    var0.setFlags((byte)1);
    byte[] var3 = var0.getLocalFileDataData();

  }

  public void test9() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test9"); }


    org.apache.commons.compress.archivers.zip.X5455_ExtendedTimestamp var0 = new org.apache.commons.compress.archivers.zip.X5455_ExtendedTimestamp();
    var0.setFlags((byte)63);
    byte[] var3 = var0.getCentralDirectoryData();

  }

  public void test10() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test10"); }


    org.apache.commons.compress.archivers.zip.X5455_ExtendedTimestamp var0 = new org.apache.commons.compress.archivers.zip.X5455_ExtendedTimestamp();
    var0.setFlags((byte)63);
    org.apache.commons.compress.archivers.zip.ZipExtraField[] var3 = new org.apache.commons.compress.archivers.zip.ZipExtraField[] { var0};
    byte[] var4 = org.apache.commons.compress.archivers.zip.ExtraFieldUtils.mergeCentralDirectoryData(var3);

  }

  public void test11() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test11"); }


    org.apache.commons.compress.archivers.zip.UnrecognizedExtraField var0 = new org.apache.commons.compress.archivers.zip.UnrecognizedExtraField();
    org.apache.commons.compress.archivers.zip.ZipExtraField[] var1 = new org.apache.commons.compress.archivers.zip.ZipExtraField[] { var0};
    byte[] var2 = org.apache.commons.compress.archivers.zip.ExtraFieldUtils.mergeLocalFileDataData(var1);

  }

}
