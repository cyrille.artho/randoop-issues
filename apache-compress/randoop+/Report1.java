import junit.framework.*;

public class Report1 extends TestCase {
/*
All these tests show the same problem, top of the stack trace is always same:
java.lang.NullPointerException
        at org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream$Curr
entEntry.access$100(ZipArchiveOutputStream.java:1464)
        at org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream.writ
e(ZipArchiveOutputStream.java:756)
        at org.apache.commons.compress.archivers.ArchiveOutputStream.write(Archi
veOutputStream.java:109)
*/
  public void test12() throws Throwable {

    java.io.ByteArrayOutputStream var0 = new java.io.ByteArrayOutputStream();
    org.apache.commons.compress.archivers.jar.JarArchiveOutputStream var1 = new org.apache.commons.compress.archivers.jar.JarArchiveOutputStream((java.io.OutputStream)var0);
    var1.write(25843);

  }

  public void test13() throws Throwable {

    java.io.ByteArrayOutputStream var1 = new java.io.ByteArrayOutputStream(2561);
    org.apache.commons.compress.archivers.jar.JarArchiveOutputStream var2 = new org.apache.commons.compress.archivers.jar.JarArchiveOutputStream((java.io.OutputStream)var1);
    org.apache.commons.compress.compressors.gzip.GzipParameters var3 = new org.apache.commons.compress.compressors.gzip.GzipParameters();
    org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream var4 = new org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream((java.io.OutputStream)var2, var3);

  }

  public void test14() throws Throwable {

    java.io.ByteArrayOutputStream var0 = new java.io.ByteArrayOutputStream();
    org.apache.commons.compress.archivers.jar.JarArchiveOutputStream var1 = new org.apache.commons.compress.archivers.jar.JarArchiveOutputStream((java.io.OutputStream)var0);
    byte[] var3 = new byte[] { (byte)10};
    var1.write(var3, 21840, 21840);

  }

  public void test19() throws Throwable {

    java.io.ByteArrayOutputStream var0 = new java.io.ByteArrayOutputStream();
    org.apache.commons.compress.archivers.jar.JarArchiveOutputStream var1 = new org.apache.commons.compress.archivers.jar.JarArchiveOutputStream((java.io.OutputStream)var0);
    byte[] var3 = new byte[] { (byte)0, (byte)0, (byte)0};
    var1.write(var3);

  }

  public void test20() throws Throwable {

    java.io.ByteArrayOutputStream var0 = new java.io.ByteArrayOutputStream();
    org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream var1 = new org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream((java.io.OutputStream)var0);
    org.apache.commons.compress.archivers.jar.JarArchiveOutputStream var2 = new org.apache.commons.compress.archivers.jar.JarArchiveOutputStream((java.io.OutputStream)var1);
    org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream var4 = new org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream((java.io.OutputStream)var2, (short)1);
    var4.finish();

  }

  public void test21() throws Throwable {

    java.io.ByteArrayOutputStream var1 = new java.io.ByteArrayOutputStream(2561);
    org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream var2 = new org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream((java.io.OutputStream)var1);
    org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream var3 = new org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream((java.io.OutputStream)var2);

  }

  public void test22() throws Throwable {

    byte[] var1 = new byte[] { (byte)10, (byte)10};
    java.io.ByteArrayInputStream var2 = new java.io.ByteArrayInputStream(var1);
    java.io.ByteArrayOutputStream var3 = new java.io.ByteArrayOutputStream();
    org.apache.commons.compress.archivers.jar.JarArchiveOutputStream var4 = new org.apache.commons.compress.archivers.jar.JarArchiveOutputStream((java.io.OutputStream)var3);
    long var6 = org.apache.commons.compress.utils.IOUtils.copy((java.io.InputStream)var2, (java.io.OutputStream)var4, 26742);

  }

  public void test23() throws Throwable {

    java.io.ByteArrayOutputStream var0 = new java.io.ByteArrayOutputStream();
    org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream var1 = new org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream((java.io.OutputStream)var0);
    org.apache.commons.compress.archivers.jar.JarArchiveOutputStream var2 = new org.apache.commons.compress.archivers.jar.JarArchiveOutputStream((java.io.OutputStream)var1);
    org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream var4 = new org.apache.commons.compress.archivers.cpio.CpioArchiveOutputStream((java.io.OutputStream)var2, (short)1);
    var4.close();

  }

  public void test25() throws Throwable {

    java.io.ByteArrayOutputStream var0 = new java.io.ByteArrayOutputStream();
    org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream var1 = new org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream((java.io.OutputStream)var0);
    org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream var2 = new org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream((java.io.OutputStream)var1);

  }

}
