package randoop;


import junit.framework.*;

public class RandoopTest extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test1"); }


    org.apache.commons.compress.archivers.zip.UnicodeCommentExtraField var0 = new org.apache.commons.compress.archivers.zip.UnicodeCommentExtraField();
    org.apache.commons.compress.archivers.zip.ZipShort var1 = var0.getLocalFileDataLength();

  }

  public void test2() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test2"); }


    org.apache.commons.compress.archivers.zip.UnrecognizedExtraField var0 = new org.apache.commons.compress.archivers.zip.UnrecognizedExtraField();
    org.apache.commons.compress.archivers.zip.ZipShort var1 = var0.getCentralDirectoryLength();

  }

  public void test3() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test3"); }


    org.apache.commons.compress.archivers.cpio.CpioArchiveEntry var1 = new org.apache.commons.compress.archivers.cpio.CpioArchiveEntry((short)1);
    int var2 = var1.getHeaderPadCount();

  }

  public void test4() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test4"); }


    org.apache.commons.compress.archivers.zip.ZipArchiveEntry var1 = new org.apache.commons.compress.archivers.zip.ZipArchiveEntry("hi!");
    org.apache.commons.compress.archivers.zip.UnrecognizedExtraField var2 = new org.apache.commons.compress.archivers.zip.UnrecognizedExtraField();
    var1.addAsFirstExtraField((org.apache.commons.compress.archivers.zip.ZipExtraField)var2);

  }

  public void test5() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test5"); }


    org.apache.commons.compress.archivers.zip.UnicodeCommentExtraField var0 = new org.apache.commons.compress.archivers.zip.UnicodeCommentExtraField();
    org.apache.commons.compress.archivers.zip.ZipShort var1 = var0.getCentralDirectoryLength();

  }

  public void test6() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test6"); }


    org.apache.commons.compress.archivers.zip.ZipArchiveEntry var1 = new org.apache.commons.compress.archivers.zip.ZipArchiveEntry("hi!");
    org.apache.commons.compress.archivers.zip.UnrecognizedExtraField var2 = new org.apache.commons.compress.archivers.zip.UnrecognizedExtraField();
    var1.addExtraField((org.apache.commons.compress.archivers.zip.ZipExtraField)var2);

  }

  public void test7() throws Throwable {

    if (debug) { System.out.println(); System.out.print("RandoopTest0.test7"); }


    org.apache.commons.compress.archivers.zip.UnrecognizedExtraField var0 = new org.apache.commons.compress.archivers.zip.UnrecognizedExtraField();
    org.apache.commons.compress.archivers.zip.ZipShort var1 = var0.getLocalFileDataLength();

  }

}
